
# drawtrans - functions for transforming Acorn Drawfile objects
#
# Copyright (C) 2002-2010 David Boddie <david@boddie.org.uk>
# 
# This file is part of the Impression package.
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301, USA.

import copy, drawfile, math
import numpy as Numeric

def transform(object, transformation, parameters, origin = None, copy_object = 0):

    # Make a copy of this object if required.
    if copy_object != 0:

        object = copy.deepcopy(object)

    if isinstance(object, drawfile.font_table) or isinstance(object, drawfile.options):

        # Neither of these objects can be transformed in any meaningful way.

        # Return the object without any change, if a copy is required.
        if copy_object != 0:

            return object

        else:

            return

    # Transform the object about its origin.
    if origin is None:

        origin = (object.x1, object.y1)

    if isinstance(object, drawfile.path):

        xmin = []
        ymin = []
        xmax = []
        ymax = []

        for i in xrange(0, len(object.path)):
    
            # Look for elements with parameters.
    
            if len(object.path[i]) > 1:
    
                # Read the command and parameters.
                command = object.path[i][0]
    
                params = object.path[i][1:]
                new_params = []
    
                for j in range(0, len(params)):
    
                    # Read the coordinates of the point given.
                    x0 = params[j][0]
                    y0 = params[j][1]
    
                    # Transform the coordinates.
                    x1, y1 = transformation(x0 - origin[0], y0 - origin[1], parameters)

                    new_params.append( (origin[0] + x1, origin[1] + y1) )

                    # Maintain the minimum and maximum coordinates only for the first coordinates pair.
                    if j == 0:

                        xmin.append(origin[0] + x1)
                        ymin.append(origin[1] + y1)
                        xmax.append(origin[0] + x1)
                        ymax.append(origin[1] + y1)
    
                # Store the coordinates back in the object.
                object.path[i] = tuple([command] + new_params)

        # If the object is being stretch then adjust the line thickness according to the horizontal
        # scale factor.
        if transformation == _stretch:

            object.width = object.width * parameters[0]  

        # Determine the bounding box coordinates.
        xmin, ymin, xmax, ymax = min(xmin), min(ymin), max(xmax), max(ymax)

    elif isinstance(object, drawfile.group) and len(object.objects) > 0:

        # Apply the transformation to all the relevant objects in the group.

        # Don't copy the objects in the group as any previous deep copy will
        # have already done this.
        transform(object.objects[0], transformation, parameters, origin, 0)

        xmin = object.objects[0].x1
        ymin = object.objects[0].y1
        xmax = object.objects[0].x2
        ymax = object.objects[0].y2

        for obj in object.objects[1:]:

            transform(obj, transformation, parameters, origin, 0)

            # Maintain the minimum and maximum coordinates.
            xmin = min(xmin, obj.x1)
            ymin = min(ymin, obj.y1)
            xmax = max(xmax, obj.x2)
            ymax = max(ymax, obj.y2)

    elif isinstance(object, drawfile.sprite) or isinstance(object, drawfile.text):

        # Transform the coordinates of the bounding box corners.

        x1, y1 = transformation(
            object.x1 - origin[0], object.y1 - origin[1], parameters
            )

        x1 = x1 + origin[0]
        y1 = y1 + origin[1]

        x2, y2 = transformation(
            object.x2 - origin[0], object.y2 - origin[1], parameters
            )

        x2 = x2 + origin[0]
        y2 = y2 + origin[1]

        xa, ya = transformation(
            object.x1 - origin[0], object.y2 - origin[1], parameters
            )

        xa = xa + origin[0]
        ya = ya + origin[1]

        xb, yb = transformation(
            object.x2 - origin[0], object.y1 - origin[1], parameters
            )

        xb = xb + origin[0]
        yb = yb + origin[1]

        # Transformed text objects require their baseline origin to be transformed.
        if isinstance(object, drawfile.text):

            # Transform the baseline coordinates.

            xbl, ybl = transformation(
                object.baseline[0] - origin[0], object.baseline[1] - origin[1], parameters
                )

            object.baseline = [xbl + origin[0], ybl + origin[1]]

        # Determine the new bounding box of the object from the transformed bounding
        # box coordinates.
        xmin = min(x1, x2, xa, xb)
        ymin = min(y1, y2, ya, yb)
        xmax = max(x1, x2, xa, xb)
        ymax = max(y1, y2, ya, yb)

        # Provide a transformation matrix if the sprite does not have one.
        if not hasattr(object, 'transform'):

            trans = Numeric.identity(3) + Numeric.zeros((3,3), 'f')

            # Transformed text objects need font flags to be defined.
            if isinstance(object, drawfile.text):

                font_flags = {'kerning': 'on'}

        else:

            trans = Numeric.zeros((3,3), 'f')

            trans[0] = [object.transform[0], object.transform[1], 0]
            trans[1] = [object.transform[2], object.transform[3], 0]
            trans[2] = [object.transform[4], object.transform[5], 1]

            # Transformed text objects also have font flags to read.
            if isinstance(object, drawfile.text):

                font_flags = object.font_flags

        # Modify the matrix to take the new transformation into account.
        if transformation == _rotate:

            angle = parameters
            matrix = Numeric.zeros((3,3), 'f')
            matrix[0] = [math.cos(angle), math.sin(angle), 0]
            matrix[1] = [-math.sin(angle), math.cos(angle), 0]
            matrix[2] = [0, 0, 1]

        elif transformation == _stretch:

            factors = parameters
            matrix = Numeric.zeros((3,3), 'f')
            matrix[0] = [factors[0], 0, 0]
            matrix[1] = [0, factors[1], 0]
            matrix[2] = [0, 0, 1]

        else:

            matrix = Numeric.identity(3) + Numeric.zeros((3,3), 'f')

        # Multiply the matrix with the transformation matrix.
        new_matrix = Numeric.matrixmultiply(matrix, trans)

        #if transformation == _translate:
        #
        #    new_matrix[2] = [xmin, ymin, 1]

        # Store the result in the object.
        object.transform = \
        [
            new_matrix[0][0], new_matrix[0][1],
            new_matrix[1][0], new_matrix[1][1],
            new_matrix[2][0], new_matrix[2][1] 
        ]

        if isinstance(object, drawfile.sprite):

            # Fill in the displacement entries in the transformation matrix only
            # for sprites.
            object.transform[4:6] = [x1, y1]

        # For text objects, set the font flags for this object.
        if isinstance(object, drawfile.text):

            object.font_flags = font_flags

    else:

        # This object is not understood or supported. Return it as it is, if specified.
        if copy_object != 0:

            return object

        else:

            return

    # Store the new bounding box of the object.
    object.x1, object.y1 = xmin, ymin
    object.x2, object.y2 = xmax, ymax

    # If we have been working with a copy then return it.
    if copy_object != 0:

        return object


def _rotate(x0, y0, angle):

    x1 = int( (x0 * math.cos(angle)) - (y0 * math.sin(angle)) )
    y1 = int( (y0 * math.cos(angle)) + (x0 * math.sin(angle)) )

    return x1, y1


def _stretch(x0, y0, factors):

    x1 = int(x0 * factors[0])
    y1 = int(y0 * factors[1])

    return x1, y1


def _translate(x0, y0, translations):

    x1 = x0 + translations[0]
    y1 = y0 + translations[1]

    return x1, y1


def rotate(object, angle, origin = None, copy_object = 0, units = 'degrees'):

    if units == 'degrees':

        angle = angle * math.pi / 180.0

    elif units == 'radians':

        pass

    else:

        raise ValueError, 'Unknown units: %s' % units

    object = transform(object, _rotate, angle, origin, copy_object)

    # If we have been working with a copy then return it.
    if copy_object != 0:

        return object


def stretch(object, factors, origin = None, copy_object = 0):

    object = transform(object, _stretch, factors, origin, copy_object)

    # If we have been working with a copy then return it.
    if copy_object != 0:

        return object


def translate(object, translations, origin = None, copy_object = 0):

    object = transform(object, _translate, translations, origin, copy_object)

    # If we have been working with a copy then return it.
    if copy_object != 0:

        return object


def recolour(object, colour, copy_object = 0):

    # Make a copy of this object if required.
    if copy_object != 0:

        object = copy.deepcopy(object)

    if isinstance(object, drawfile.group):

        # Apply the transformation to all the relevant objects in the group.
        for obj in object.objects:

            # Don't copy the objects in the group as any previous deep copy will
            # have already done this.
            recolour(obj, colour, 0)

    else:

        # Only recolour object colours if they are not transparent.
        if hasattr(object, 'outline'):

            if tuple(object.outline) != (255, 255, 255, 255):

                object.outline = \
                (
                    object.outline[0],
                    min(object.outline[1] + colour[1], 255),
                    min(object.outline[2] + colour[2], 255),
                    min(object.outline[3] + colour[3], 255)
                )

        if hasattr(object, 'fill'):

            if tuple(object.fill) != (255, 255, 255, 255):

                object.fill = \
                (
                    object.fill[0],
                    min(object.fill[1] + colour[1], 255),
                    min(object.fill[2] + colour[2], 255),
                    min(object.fill[3] + colour[3], 255)
                )


    # If we have been working with a copy then return it.
    if copy_object != 0:

        return object


def find_bbox(objects):

    if len(objects) == 0:

        return None, None

    xmin = []
    ymin = []
    xmax = []
    ymax = []

    for object in objects:

        if isinstance(object, drawfile.font_table) or \
            isinstance(object, drawfile.options):

            continue

        elif isinstance(object, drawfile.group):

            x1, y1, x2, y2 = find_bbox(object.objects)

        else:

            x1, y1, x2, y2 = object.x1, object.y1, object.x2, object.y2

        xmin.append(x1)
        ymin.append(y1)
        xmax.append(x2)
        ymax.append(y2)

    return min(xmin), min(ymin), max(xmax), max(ymax)

def update_fonts(object, font_mappings):

    if isinstance(object, drawfile.group):

        for obj in object.objects:

            update_fonts(obj, font_mappings)

    elif isinstance(object, drawfile.text):

        # Modify the text style.
        if object.style not in font_mappings.keys():

            object.style = 0

        else:

            object.style = font_mappings[object.style]
