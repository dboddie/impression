#! /usr/bin/env python
#
# writer.py - a writer interface for the Impression package
# 
# Copyright (C) 2002-2010 David Boddie <david@boddie.org.uk>
# 
# This file is part of the Impression package.
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301, USA.

"""
writer.py

General writer classes for Impression documents.
"""

import drawfile, fonts, impression, string


class WriterError(Exception):

    pass


def clean_string(s):

    new = ''

    for i in s:

        if ord(i) >= 32:

            new = new + i

    return new


# A style to provide the default style attributes which the base style in many documents fails to provide.

class UnderBaseStyle:

    background_colour = impression.RGBTColour( 255, 255, 255, 255 )


class Text:

    def __init__(self, text, y1, y2, style):

        self.text = text
        self.y1 = y1
        self.y2 = y2
        self.style = style

        font_name, font_h = style.font_name, style.font_size
        font_w = font_h * (style.aspect_ratio / 100.0)

        self.font = font_name
        self.font_width = font_w
        self.font_height = font_h

        # Create a font instance.
        try:
            self.font = fonts.Font(font_name, font_w, font_h)

        except fonts.FontError:

            # It would be useful to introduce a fallback option to cover this situation.
            raise
        #print self.font

    def width(self):

        return self.font.string_width(self.text)


class Space(Text):

    def __init__(self, style):

        Text.__init__(self, ' ', 0.0, 0.0, style)

class HSpace:

    def __init__(self, value, style):

        self.value = value
        self.style = style

    def width(self):

        return self.value

class Kern:

    def __init__(self, hkern, vkern, style):

        self.hkern = hkern
        self.vkern = vkern
        self.style = style

    def width(self):

        return self.hkern


class Line:

    def __init__(self, coords, inset_h):

        # Record the bounding box coordinates.
        self.x1, self.y1, self.x2, self.y2 = coords

        # Record the parent frame's horizontal inset margin.
        self.inset_h = inset_h

        # Store text objects in a list.
        self.text_list = []

        # Margins are initially undefined and can be specified by commands in the text.
        self.left_margin = None
        self.right_margin = None

        # Line is initially empty.
        self.empty = 1

    def add_word(self, word):

        """add_word(word)
        \r
        \rAdd an object, such as a Text object describing a word or a Space object, to
        \rthe list of elements in this line.
        """
        self.text_list.append(word)
        self.empty = 0

    def find_text_placement(self, first_line, trailing):

        """left_margin, right_margin, align_style = find_text_placement(first_line, trailing)
        \r
        \rDetermine the left and right margins which will be values obtained from
        \rmodifications to the minimum and maximum horizontal extent of the line.
        \rThe alignment style returned is the style used to determine the margins
        \rand contains information on the justification scheme to be applied to
        \rthe text.
        \r
        \rThe first_line and trailing parameters determine whether the line is to be
        \rconsidered to be the first or last line in a paragraph.  
        """

        # Store the first line and trailing flags.
        self.first_line = first_line
        self.trailing = trailing

        # Read the text styles, establishing the alignment of the text based on the
        # rules that:
        # 
        # - Paragraph styles override non-paragraph styles.
        # - Earlier non-paragraph styles override later ones.
        # - Earlier paragraph styles override later ones.

        para_align = []
        non_para_align = []

        at = 0

        for text in self.text_list:

            if text.style.paragraph_apply:

                para_align.append(text.style)

            else:

                non_para_align.append(text.style)

            at = at + 1

        if para_align == []:

            if non_para_align != []:

                # If no paragraph styles were used then use the non-paragraph style alignment.
                align_style = non_para_align[0]

            else:

                print 'Information: expected text list but found', self.text_list
                return

        else:

            # Otherwise, use the paragraph alignment.
            align_style = para_align[0] # [-1]
        
        # Check whether the margins are explicitly given.
        if self.left_margin is not None:

            left_margin = self.left_margin

        else:

            # Indent the first line correctly.
            if self.first_line == 1: # and self.trailing == 0:

                # This is the first line in a paragraph.
                left_margin = align_style.first_line_left_margin

            else:

                # This line is part of a paragraph.
                left_margin = align_style.left_margin

        # Left and right margin positions
        left_margin = min(
            max(self.x1 + left_margin, self.x1 + self.inset_h),
            self.x2 - self.inset_h
            )

        if self.right_margin is not None:

            right_margin = self.right_margin

        else:

            right_margin = align_style.right_margin

        if right_margin > 0.0:

            # Measure the right margin from the left hand side of the frame.
            right_margin = min(
                max(self.x1 + right_margin, self.x1 + self.inset_h),
                self.x2 - self.inset_h
                )

        else:
        
            # Normally, measure the margin from the right hand side of the frame.
            right_margin = min(
                self.x2 - self.inset_h + right_margin, self.x2 - self.inset_h
                )

        return left_margin, right_margin, align_style


    def position_text(self, left_margin, right_margin, baseline, align_style):

        """position_text(left_margin, right_margin, baseline, align_style)
        \r
        \rHorizontally position the text on the line according to the margins and the
        \rjustification scheme given in the style used to describe the text alignment.
        \r
        \rThe text is vertically displaced by the amount given by the baseline
        \rparameter.
        """

        # Record the alignment of the text.
        alignment = align_style.justification

        # Read the text.

        # Distribute the text horizontally on the line.
        x = left_margin

        # Vertically displace the items by the distance between the top of the line and the
        # baseline.
        y = self.y2 - baseline

        # Calculate the space used by non-whitespace in the line.
        space_used = 0.0
        number_of_spaces = 0

        for item in self.text_list:

            # Store the cursor position within the object itself.
            item.x = x

            if hasattr(item, 'y1'):

                height = item.y2 - item.y1
                item.y1 = y
                item.y2 = y + height

            if isinstance(item, Space):

                number_of_spaces = number_of_spaces + 1

            else:

                # Add the width of the item to the space used unless it is soft
                # whitespace.
                space_used = space_used + item.width()

            if isinstance(item, Kern):

                # Horizontal kerning is taken care of with the width method, but vertical
                # kerning is accounted for here.
                y = y + item.vkern

            # Move the cursor.
            x = x + item.width()


        if (alignment == 'full' and self.trailing == 0) or (x > right_margin):
            #(isinstance(self.text_list[-1], Space) or ):

            # Redistribute the text according to the amount of soft whitespace available
            # on the line, not including trailing soft whitespace.

            i = len(self.text_list) - 1

            while i >= 0:

                if isinstance(self.text_list[i], Space):
                    number_of_spaces = number_of_spaces - 1
                    i = i - 1
                else:
                    break

            if number_of_spaces != 0:

                each_space_width = float(
                    (right_margin - left_margin) - space_used
                    ) / number_of_spaces

                x = left_margin

                for item in self.text_list[:i+1]:

                    # Put items where they are supposed to be in this alignment scheme.
                    if x != item.x:

                        item.x = x

                    # Update the cursor, being flexible where soft whitespace is found.
                    if isinstance(item, Space):

                        x = x + each_space_width

                    else:

                        x = x + item.width()

        elif alignment == 'centre':

            # Move the text so that it is centred within the margins.
            displace_by = (right_margin - x) / 2.0

            for item in self.text_list:

                # Move all items to the right by the specified distance.
                item.x = item.x + displace_by

        elif alignment == 'right':

            # Move the text so that it is aligned with the right margin.
            displace_by = (right_margin - x)

            for item in self.text_list:

                # Move all items to the right by the specified distance.
                item.x = item.x + displace_by


    def write_line(self, container, writer, page_height, baseline, first_line,
                   trailing):

        """write_line(container, writer, page_height, baseline, first_line,
        \r            trailing)
        \r
        \rThis method is called when a Writer object needs to produce a line of text
        \rin the relevant format. The implementation in the base Line class performs
        \rno actions on the line content and should be replaced in subclasses.
        \r
        \rThe parameters may be defined to suit the needs of each Writer subclass
        \rbut the ones given above have the following meanings:
        \r
        \r container:   An object which will contain all the objects corresponding to
        \r              words in the line.
        \r
        \r writer:      The instance of the Writer class which created this object.
        \r              This reference is often needed because the Writer object will
        \r              maintain lists of required fonts and other information.
        \r
        \r page_height: The height of the page in millipoints. Since the vertical
        \r              locations of text on the page are expressed as negative
        \r              displacements from the top left corner of the page, this
        \r              value allows the Line object to translate them to
        \r              appropriate values for output formats which specify the
        \r              page origin as the bottom left corner of the page.
        \r
        \r baseline:    The vertical displacement of the text from the vertical
        \r              coordinates when the line was created. Note that this
        \r              value could have been added to the vertical coordinates
        \r              by the impression module but is currently given
        \r              separately so that subclasses of this class can ignore
        \r              it if they wish.
        \r
        \r first_line:  A flag which indicates whether this line is the first
        \r              in a paragraph.
        \r
        \r trailing:    A flag which indicates whether this line is the last
        \r              in a paragraph.
        \r
        \r
        \rSubclasses could:
        \r
        \r - Add the items in the text list (self.text_list) to containers so that
        \r   lines of text are neatly packaged in the output format.
        \r
        \r - Add underline and strikeout where relevant.
        \r
        \r - Add horizontal and vertical rule-offs where relevant.
        \r
        \r - Add the line container to the container object passed as a parameter
        \r   to this method.
        """
        
        # Convert the coordinates of this object to native coordinates for
        # later use.
        
        # Find the alignment of the text line using the paragraph information
        # stored in the writer object.
        left_margin, right_margin, align_style = \
            self.find_text_placement(first_line, trailing)
        
        # Horizontally distribute the text on the baseline.
        # The baseline is a displacement from the text's vertical position.
        self.position_text(
            left_margin, right_margin, baseline, align_style
            )
        
        # Add the items in the text list to the line group.
        
        for item in self.text_list:
        
            if isinstance(item, writer.Text):
            
                # Create some text.
                pass



class Writer:

    # Fonts with an oblique variant available
    oblique_fonts = [
        'Corpus.Bold', 'Corpus.Medium',
        'Frutiger.Black', 'Frutiger.Bold', 'Frutiger.Light', 'Frutiger.Medium',
        'Honerton.Black', 'Homerton.Bold', 'Homerton.Light', 'Homerton.Medium', 
        ]

    # Fonts with an italic variant available
    italic_fonts = [
        'Goudy.OldStyle', 'MathGreek', 'MathPhys',
        'NewHall.Bold', 'NewHall.Medium', 'Pembroke.Bold', 'Pembroke.Medium',
        'Puritan', 'Trinity.Bold', 'Trinity.Medium' 
        ]

    # Fonts with a bold weight available
    bold_fonts = ['Anwen', 'BitFont', 'Corpus', 'Frutiger', 'Goudy.OldStyle', 'Homerton',
                    'NewHall', 'Pembroke', 'Sassoon.Primary', 'Trinity']

    weights = ['Black', 'Bold', 'Light', 'Medium']
    variants = ['Italic', 'Oblique']
    weights_and_variants = weights + variants

    def reset_styles(self):

        # Record the application numbers used as styles are applied.
        self.applied = []

        # The application numbers are used as keys in a dictionary of style instances.
        self.styles = {}

        # The current style class being used.
        self.current_style = UnderBaseStyle

        self.apply_style(0, 0)

    def apply_style(self, counter, style_no):

        style = self.document.styles[style_no]

        self.applied.append(counter)
        self.styles[counter] = style

        # Derive a new class based on the current style.
        if self.current_style is not None:

            # The new style attributes should override the old ones (left to right inheritance lookup).

            class TextStyle(style, self.current_style):

                pass
            
            self.current_style = TextStyle

        else:

            self.current_style = style

        # Determine the font name that the new style should use based on the properties
        # of the font.
        self.current_style.font_name = self.read_style_font()


    def remove_style(self, counter, style_no):

        if style_no >= len(self.document.styles):

            # Index exceeds length of document styles list so find the last style to
            # appear on the style menu.
            i = len(self.document.styles) - 1

            while i >= 0:

                if self.document.styles[i].on_menu == 1:

                    break

                i = i - 1

            style_no = max(0, i)

        style = self.document.styles[style_no]

        # Search the list of applied styles for the given style and remove it.

        i = len(self.applied) - 1

        while i >= 0:

            if self.applied[i] == counter:

                break

            i = i - 1

        self.applied = self.applied[:i] + self.applied[i+1:]

        if counter not in self.applied:

            del self.styles[counter]

    def read_style_attribute(self, name):

        # Return the style attribute by examining the applied styles in
        # reverse order until the relevant information has been found.

        i = len(self.applied) - 1

        while i >= 0:

            counter = self.applied[i]

            if hasattr(self.styles[counter], name):

                return getattr(self.styles[counter], name)

            i = i - 1

        return None

    def read_style_font(self):

        # Determine the font to use on the basis of the underlying font and any effects
        # subsequently applied.

        font_name = self.current_style.font_name
        bold = self.current_style.bold_on
        italic = self.current_style.italic_on

        # Convert the font to a list representation.
        font_list = string.split(font_name, '.')

        # Ensure that the font family and font name remain together.
        i = 0
        while i < len(font_list):

            if font_list[i] in self.weights_and_variants:

                break

            i = i + 1

        font_list = [string.join(font_list[:i], '.')] + font_list[i:]

        # Bold and italic effects can only override fonts without these effects already
        # set.
        if bold == 1:

            if (font_list[0] in self.bold_fonts) and ('Bold' not in font_list):

                if 'Medium' in font_list:

                    weight_index = font_list.index('Medium')
                    font_list[ weight_index ] = 'Bold'
                    font_list[0:weight_index] = [string.join(font_list[0:weight_index], '.')]

                else:

                    font_list.insert(1, 'Bold')
                    font_list[0:2] = [string.join(font_list[0:2], '.')]

        else:

            # Join all the elements of the font description up to and including the font
            # weight.
            i = 0

            for j in font_list:

                if j in self.weights:

                    break

                i = i + 1

            font_list[0:i+1] = [string.join(font_list[0:i+1], '.')]

        if italic == 1:

            if (font_list[0] in self.italic_fonts) and ('Italic' not in font_list):

                font_list.append('Italic')

            elif (font_list[0] in self.oblique_fonts) and ('Oblique' not in font_list):

                font_list.append('Oblique')

        # Reconstruct the font name to use.
        return string.join(font_list, '.')

    def write_line(self, line_object):

        # The way the writer deals with the line object depends on the output
        # format. By default, just pass the writer object to the write_line
        # method of the line_object and let it discover the information it
        # needs.
        
        # Write the line using the line object specified.
        line_object.write_line(
            None, self, self.page_height, self.baseline, self.first_line,
            self.trailing
            )
    
    def write_number(self, number, style):
    
        if style == 'arabic':
            return '%i' % number
        
        else:
            # Roman numerals.
            s = ''
            
            # Find the number of thousands first.
            thousands = int(number / 1000)
            s = s + ('M' * thousands)
            
            number = number - (thousands * 1000)
            
            # If the result is greater than five hundred then add the relevant symbols.
            if number > 500:

                if number < 800:
                
                    s = s + 'D'
                    number = number - 500
                
                # Determine how many hundreds can be taken away from a thousand to
                # produce a number smaller than the one we have.
                elif number < 900:
                
                    s = s + 'CCM'
                    number = number - 800
                
                else:
                
                    s = s + 'CM'
                    number = number - 900
            
            # Find the number of hundreds remaining.
            hundreds = int(number / 100)
            s = s + ('C' * hundreds)
            
            number = number - (hundreds * 100)
            
            # If the result is greater than fifty then add the relevant symbol.
            if number > 50:

                if number < 80:

                    s = s + 'L'
                    number = number - 50
                
                # Determine how many tens can be taken away from a hundred to
                # produce a number smaller than the one we have.
                elif number < 90:
                
                    s = s + 'XXC'
                    number = number - 80
                
                else:
                
                    s = s + 'XC'
                    number = number - 90

            # Find the number of tens remaining.
            tens = int(number / 10)
            s = s + ('M' * tens)
            
            # If the result is greater than five then add the relevant symbols.
            if number > 5:

                if number < 8:
                
                    s = s + 'V'
                    number = number - 5
                
                # Determine how many ones can be taken away from a ten to
                # produce a number smaller than the one we have.
                elif number < 9:
                
                    s = s + 'IIX'
                    number = number - 8
                
                else:
                
                    s = s + 'IX'
                    number = number - 9
            
            # Find the number of ones remaining.
            s = s + ('I' * number)
        
        if style == 'roman':
        
            return string.lower(s)
        
        else:
            return s
    
    # Define the types of Draw object which can be rendered in a border.
    drawfile_render = \
    (
        drawfile.text, drawfile.path, drawfile.sprite, drawfile.group
    )
