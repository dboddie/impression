#! /usr/bin/env python

# Font support for outline fonts.
# 
# Copyright (C) 2002-2010 David Boddie <david@boddie.org.uk>
#
# This file is part of the Impression package.
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301, USA.

"""
fonts.py

Font support for outline fonts.
"""

import types, sys


# An exception class for font related errors
class FontError(Exception):

    pass



if sys.platform == 'RISCOS':

    # Import SWI module

    import swi

    # Borrowed from my app library for RISC OS and modified for use with the
    # Impression document reading class.

    def points_to_os1(x):
        """x2 = points_to_os1(x1)
        Convert a length x1 measured in points to the corresponding length x2
        measured in screen units.
        """
        return swi.swi('Font_ConverttoOS', '.i;.i', x*1000)

    def points_to_os2(x, y):
        """x2, y2 = points_to_os2(x1, y1)
        Convert lengths x1, y1 measured in points to the corresponding lengths
        x2, y2 measured in screen units.
        """
        return swi.swi('Font_ConverttoOS', '.ii;.ii', x*1000, y*1000)

    def os_to_points1(x):
        """x2 = os_to_points1(x1)
        Convert a length x1 measured in screen units to the corresponding length
        x2 measured in points.
        """
        a = swi.swi('Font_Converttopoints', '.i;.i', x)
        return a/1000.0, b/1000.0

    def os_to_points2(x, y):
        """x2, y2 = os_to_points2(x1, y1)
        Convert lengths x1, y1 measured in screen units to the corresponding
        lengths x2, y2 measured in points.
        """
        a, b = swi.swi('Font_Converttopoints', '.ii;.ii', x, y)
        return a/1000.0, b/1000.0

    def millipoints_to_os1(x):
        """x2 = millipoints_to_os1(x1)
        Convert a length x1 measured in millipoints to the corresponding length
        x2 measured in screen units.
        """
        return swi.swi('Font_ConverttoOS', '.i;.i', x)

    def millipoints_to_os2(x, y):
        """x2, y2 = millipoints_to_os2(x1, y1)
        Convert lengths x1, y1 measured in millipoints to the corresponding
        lengths x2, y2 measured in screen units.
        """
        return swi.swi('Font_ConverttoOS', '.ii;.ii', x, y)

    def os_to_millipoints1(x):
        """x2 = os_to_millipoints1(x1)
        Convert a length x1 measured in screen units to the corresponding length
        x2 measured in millipoints.
        """
        return swi.swi('Font_Converttopoints', '.i;.i', x)

    def os_to_millipoints2(x, y):
        """x2, y2 = os_to_points2(x1, y1)
        Convert lengths x1, y1 measured in screen units to the corresponding
        lengths x2, y2 measured in millipoints.
        """
        return swi.swi('Font_Converttopoints', '.ii;.ii', x, y)

    class Font:
        """instance = Font(name, x_size, y_size)
        Create an instance of a font having the specified name and size, measured
        in points.
        """

        def __init__(self, name, x_size, y_size):

            # Find the relevant font and obtain a font handle
            # Sizes are in millipoints
            self.x_size = x_size
            self.y_size = y_size

            try:

                self.h, self.x_dpi, self.y_dpi = swi.swi(
                    'Font_FindFont', '.siiii;i...ii', name,
                    int((x_size/1000.0)*16), int((y_size/1000.0)*16), 0, 0
                    )

            except swi.error:

                raise FontError, 'Font %s could not be found.' % name

            # Find the size of a space in this font (in points)
            temp = swi.block(9, [0])
            max_x, max_y = os_to_millipoints2(65536, 65536)
            swi.swi('Font_ScanString', 'isiiib.i', self.h, 'a a', 0x403a0, max_x, max_y, temp, 3)
            size1 = (temp[7]-temp[5])/1000.0
            swi.swi('Font_ScanString', 'isiiib.i', self.h, 'a', 0x403a0, max_x, max_y, temp, 1)
            size2 = (temp[7]-temp[5])/1000.0
            self.space_size = size1-size2

        def __del__(self):

            # Release font
            swi.swi('Font_LoseFont', 'i', self.h)

        def string_width(self, s):
            """width = string_width(string)
            Return the width in millipoints this string would have if it
            were displayed using this font object.
            """
            # Measure the width of a string in OS units
            temp = swi.block(9, [0])
            # No split character
            temp[4] = -1
            max_x, max_y = os_to_millipoints2(65536, 65536)
            max_x = swi.swi('Font_ScanString', 'isiiib.i;...i', self.h, s, 0x403a0, max_x, max_y, temp, len(s))
            return max_x

        def width(self, s):
            """w = width(string)
            Return the width in points this string would have if it were
            displayed using this font object.
            """
            # Measure the width of a string's bounding box in points
            temp = swi.block(9, [0])
            max_x, max_y = os_to_millipoints2(65536, 65536)
            swi.swi('Font_ScanString', 'isiiib.i', self.h, s, 0x403a0, max_x, max_y, temp, len(s))

            return (temp[7] - temp[5])/1000.0

        def bbox_width(self, s):
            """width = bbox_width(string)
            Return the width in millipoints of the bounding box this string
            would have if it were displayed using this font object.
            """
            # Measure the width of a string's bounding box in points
            temp = swi.block(9, [0])
            max_x, max_y = os_to_millipoints2(65536, 65536)
            swi.swi('Font_ScanString', 'isiiib.i', self.h, s, 0x403a0, max_x, max_y, temp, len(s))

            return temp[7] - temp[5]

        def bbox_height(self, s):
            """height = bbox_width(string)
            Return the height in millipoints of the bounding box this string
            would have if it were displayed using this font object.
            """
            # Measure the width of a string's bounding box in points
            temp = swi.block(9, [0])
            max_x, max_y = os_to_millipoints2(65536, 65536)
            swi.swi('Font_ScanString', 'isiiib.i', self.h, s, 0x403a0, max_x, max_y, temp, len(s))

            return temp[8] - temp[6]

        def bbox(self, s):
            """width, height = bbox(string)
            Return the width and height in millipoints of the bounding box
            this string would have if it were displayed using this font
            object.
            """
            # Measure the width of a string's bounding box in points
            temp = swi.block(9, [0])
            max_x, max_y = os_to_millipoints2(65536, 65536)
            swi.swi('Font_ScanString', 'isiiib.i', self.h, s, 0x403a0, max_x, max_y, temp, len(s))

            x1, y1 = temp[5], temp[6]
            x2, y2 = temp[7], temp[8]
    #        x1, y1 = millipoints_to_os2(temp[5], temp[6])
    #        w, h   = millipoints_to_os2(temp[7] - temp[5], temp[8] - temp[6])

            return x1, y1, x2, y2
    #        return x1, y1, w, h

        def string_split(self, text, split, max):
            """x, offset = string_split(string, split, maximum)
            Given the maximum horizontal space available in millipoints
            and a type of character at which the string may be wrapped,
            return the horizontal coordinate and the index into the string
            where the string is wrapped.
            If the string is to be wrapped at the nearest character to the
            right margin then split must be an empty string.
            """

            temp = swi.block(9, [0])
            l = len(text) + 1
    #        t = swi.block((l/4) + ((l%4)!=0), text+"\000")
            t = swi.block((l >> 2) + ((l%4)!=0), text+"\000")
            if split != '':
                temp[4] = ord(split[0])
            else:
                temp[4] = -1
            max_x, max_y = max, os_to_millipoints1(65536)
            ptr, max_x = swi.swi('Font_ScanString', 'ibiiib.i;.i.i', self.h, t, 0x3a0, max_x, max_y, temp, len(text))

            return max_x, (ptr-t.start)

        def click(self, text, x, y):
            """offset, x, y = click(string, x, y)
            Given the position (x, y) of a point relative to the lower left
            corner of the string specified in millipoints, determine the
            offset into the string of the closest character to this point and
            its position.
            """
            l = len(text) + 1
    #        t = swi.block((l/4) + ((l%4)!=0), text+"\000")
            t = swi.block((l >> 2) + ((l%4)!=0), text+"\000")
            ptr, x, y = swi.swi('Font_ScanString', 'ibiii..i;.i.ii', self.h, t, 0x20380, x, y, len(text))

            return ptr-t.start, x, y

        def render(self, text, fg, bg, x, y, transform = None):
            """render(string, foreground, background, x, y, transform = None)
            Render the string using the given colours at a screen position (x, y).
            """
            # Set colours
            if type(fg) != type(bg):
                return

            if type(fg) == types.IntType:

                # WIMP colours
                swi.swi('Wimp_SetFontColours', 'iiii', 0, bg, fg, 14)

            else:
                # Palette entries (lists or tuples)
                fg = (fg[0] << 8) | (fg[1] << 16) | (fg[2] << 24)
                bg = (bg[0] << 8) | (bg[1] << 16) | (bg[2] << 24)
                swi.swi('ColourTrans_SetFontColours', 'iiii', 0, bg, fg, 14)

            if transform != None:
                trans = swi.block(6,
                    [ int(transform[0]*65536), int(transform[1]*65536),
                      int(transform[2]*65536), int(transform[3]*65536),
                      int(transform[4]*256), int(transform[5]*256) ] )

                swi.swi('Font_Paint', 'isiii.b.', self.h, text,
                    0x350, x, y, trans)
            else:
                swi.swi('Font_Paint', 'isiii', self.h, text,
                    0x310, x, y)

else:

    class Font:
    
        def __init__(self, name, x_size, y_size):

            # Find the relevant font and obtain a font handle
            # Sizes are in millipoints
            self.name = name
            self.x_size = x_size
            self.y_size = y_size
        
        def string_width(self, s):
            """width = string_width(string)
            Return the width in millipoints this string would have if it
            were displayed using this font object.
            """
            return len(s) * self.x_size
