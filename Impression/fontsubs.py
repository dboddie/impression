#! /usr/bin/env python
#
# fontsubs.py - a font substitution module
# 
# Copyright (C) 2002-2010 David Boddie <david@boddie.org.uk>
# 
# This file is part of the Impression package.
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301, USA.

# Version history:
#
# 0.10 (Sun 16th March 2003)
#
# Initial version. A simple matching algorithm is used to compare strings
# with font definitions. When looking for a given substring within the string
# to be matched, it takes the first matching substring it finds rather than
# keeping all possible matches. In practice, this shouldn't be a problem
# because font definitions should be fairly precise but a more comprehensive
# algorithm may be needed in the future.

__author__ = 'David Boddie <david@boddie.org.uk>'
__date__ = 'Sun 16th March 2003'
__version__ = '0.10'


import os, string, sys

def locate_resources():

    """locate_resources()
    \r
    \rFind an appropriate file containing font substitutions, depending on the
    \roperating system in use, and return its name.
    """
    
    # Operating system specific checks should be first.
    
    # Default choices
    
    # Current directory
    path = ".fontsubs"
    
    if os.path.exists(path) and not os.path.isdir(path):
    
        return path
    
    # Home directory
    path = os.path.join(os.getenv("HOME"), ".fontsubs")
    
    if os.path.exists(path) and not os.path.isdir(path):
    
        return path


class SyntaxError(Exception):

    pass

class SimpleMatch:

    def __init__(self, s):
    
        # Create a list containing all the elements of the matching definition.
        
        self.definition = self.create_definition(s)
    
    def return_string(self, s, index, ending = None):
    
        if ending is None:
        
            return s[index:], len(s)
        
        # Find the ending character.
        at = string.find(s, ending, index)
        
        if at == -1:
        
            raise SyntaxError, "Incomplete wildcard: %s." % s[index - 1:]
        
        else:
        
            return s[index:at], at + 1
    
    def create_definition(self, s):
    
        # Use a working list for the definition.
        defn = []
        
        # Record the wildcard numbers used.
        wildcards = []
        
        index = 0
        
        while index < len(s):
        
            at = string.find(s, "[", index)
            
            if at == -1:
            
                # If there is no wildcard then the rest of the string will
                # be stored.
                defn.append(s[index:])
                
                break
                
            else:
            
                # A wildcard has been found.
                
                # Add the string leading up to any wildcard to the definition
                # list.
                if at != index:
                
                    defn.append(s[index:at])
                
                try:
                
                    # Find the wildcard number and the index into the following string.
                    value, index = self.return_string(s, at + 1, ending = "]")
                
                    # Coerce the wildcard value to an integer.
                    value = int(value)
                
                except ValueError: # Do not catch any SyntaxError exceptions.
                
                    # Do not create this definition.
                    raise SyntaxError, "Wildcard value not an integer: %s" % repr(value)
                
                # Check that the wildcard's value has not been used before.
                if value in wildcards:
                
                    # Do not create this definition.
                    raise SyntaxError, "Duplicate wildcard value: %s" % repr(value)
                
                # Record the wildcard value.
                wildcards.append(value)
                
                # Add a wildcard object (an integer) to the definition list.
                defn.append(value)
        
        # Return the definition.
        return defn
    
    def match_string(self, s):
    
        # Try to match a string against the definition, returning a dictionary
        # of wildcard matches if one is found.
        
        # The substrings corresponding to wildcards are stored under the
        # wildcard number in this dictionary.
        matches = {}
        
        # The current wildcard is initially undefined.
        current_wildcard = None
        
        # Maintain a record of the index into the string being tested.
        index = 0
        
        for d in self.definition:
        
            if type(d) == type(0):
            
                # The current piece to match is a wildcard.
                current_wildcard = d
                matches[d] = ""
            
            else:
            
                # A substring which must be found within the string being
                # matched against. Use the first instance of a matching
                # string.
                at = string.find(s, d, index)
                
                if at == -1:
                
                    # The substring could not be found.
                    return None
                
                elif current_wildcard is not None:
                
                    # Store the characters between the current index into
                    # the string and the instance of the substring in the
                    # matching dictionary under the current wildcard
                    # number.
                    matches[current_wildcard] = s[index:at]
                
                else:
                
                    # There is no current wildcard. If the substring is not
                    # found at the current index then the string does not
                    # comply with the definition.
                    if at != index:
                    
                        return None
                
                # There is no current wildcard now.
                current_wildcard = None
                
                # Move the index to the end of the matching string.
                index = at + len(d)
        
        if index < len(s):
        
            if current_wildcard is not None:
            
                # The index into the string is not at the end and there is a
                # current wildcard. Store the rest of the string in the
                # matching dictionary under that wildcard.
                matches[current_wildcard] = s[index:]
            
            else:
            
                # The index into the string is not at the end and there is no
                # current wildcard. The string has not been matched.
                return None
                
        # The string should have been successfully matched against the
        # definition.
        return matches
    
    def substitute_matches(self, matches):
    
        # Substitute a dictionary of wildcard matches into the definition,
        # returning a string. This is useful for testing the matching
        # algorithm, but also for substituting matches into a replacement
        # font name definition.
        
        output = ""
        
        for d in self.definition:
        
            if type(d) == type(0):
            
                # A wildcard: substitute the relevant match if possible.
                if matches.has_key(d):
                
                    output = output + matches[d]
                
                else:
                
                    return None
            
            else:
            
                # A substring.
                output = output + d
        
        # Return the string containing the substitutions.
        return output



class Substitutions:

    """Substitutions
    \r
    \rSubstitutions(filename = None)
    \r
    \rCreate a list of substitutions based on the contents of the file
    \rspecified. The file defines the substitutions to be made as a
    \rseries of font name specifications to match against and replacement
    \rstring definitions of the form:
    \r
    \r<matching specification> : <replacement definition>
    \r
    \rBoth the matching specification and replacement definition can be
    \rconstructed using wildcards of the form [n] where, for clarity, n is a
    \rpositive integer or zero. This allows ranges of fonts to be replaced by
    \rone or more equivalent fonts.
    \r
    \rAnnotated example file:
    \r
    \rSubstitution definition               Description
    \r
    \rTrinity.Medium      : Times-Roman     Straight substitution of the
    \r                                      unweighted font.
    \r
    \rTrinity.Medium.[0]  : Times-[0]       Convert non-weighted form in such
    \r                                      a way that its style is preserved.
    \r
    \rTrinity.[0].[1]     : Times-[0][1]    Merge weight and style for weighted
    \r                                      forms of the font.
    \r
    \rTrinity.[0]         : Times-[0]       Catch all other cases where weight
    \r                                      occurs without style.
    """
    
    def __init__(self, filename = None):
    
        if filename is not None:
        
            # Record the file in use.
            self.filename = filename
            
            # Read the substitutions from the file.
            self.read_file()
        
        else:
        
            # A new object.
            self.new()
    
    def read_file(self):
    
        self.substitutions = []
        
        # Read each line of the file and compile a list of definitions to be
        # matched and the values to be used instead, when a match occurs.
        try:
        
            f = open(self.filename, "r")
        
        except IOError:
        
            return
        
        for line in f.readlines():
        
            # Strip any whitespace from the line.
            line = string.strip(line)
            
            # Ignore any comment or empty lines.
            if line == "" or line[0] == "#":
            
                continue
            
            # Split the line at the first colon.
            at = string.find(line, ":")
            
            # Ignore any lines without colons.
            if at == -1:
            
                continue
            
            left, right = line[:at], line[at+1:]
            
            # Strip any whitespace from around each string in the pair of strings.
            left = string.strip(left)
            right = string.strip(right)
            
            # Try to create a definition for each string.
            try:
            
                match_defn = SimpleMatch(left)
                replace_defn = SimpleMatch(right)
            
            except SyntaxError:
            
                # There was a problem with the definition strings.
                # Ignore this substitution.
                continue
            
            # Store the values in the dictionary under the key given.
            self.substitutions.append( (match_defn, replace_defn) )
        
        # Close the file.
        f.close()
    
    def new(self):
    
        # Define an empty list of substitutions.
        self.substitutions = []
    
    def __getitem__(self, font):
    
        # Match the font name given against entries in the dictionary.
        
        for match_defn, replace_defn in self.substitutions:
        
            # Determine whether the font name matches this entry.
            matches = match_defn.match_string(font)
            
            if matches is not None:
            
                # The string matches the entry: substitute the matches
                # found into the replacement font definition.
                replacement = replace_defn.substitute_matches(matches)
                
                if replacement is not None:
                
                    # Return the replacement font name.
                    return replacement
        
        # At this point, a replacement for the font name could not be found.
        # Return the original name.
        return font
