#! /usr/bin/env python

# impression.py - classes for reading Computer Concepts Impression files
# 
# Copyright (C) 2002-2010 David Boddie <david@boddie.org.uk>
# 
# This file is part of the Impression package.
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301, USA.

"""
impression.py
Reads the document structure of Impression files.
"""

import os, sys, string, struct, types


__author__ = "David Boddie <david@boddie.org.uk>"
__date__ = "2016-01-11"
__version__ = "0.14"


class ImpressionError(Exception):

    pass


class Common:

    """Common
    
    A class containing a set of common functions which other classes will use.
    """
    
    def str2num(self, size, s):

        """integer = str2num(size, string)
        
        Convert a string containing the binary representation of a little endian
        integer into an integer. The operation is performed on "size" characters.
        """
        i = 0
        n = 0
        while i < size:
    
            n = n | (ord(s[i]) << (i*8))
            i = i + 1
    
        return n
    
    # Little endian reading
    
    def read_signed_word(self, s):
    
        return struct.unpack("<i", s)[0]
    
    def read_unsigned_word(self, s):
    
        return struct.unpack("<I", s)[0]
    
    def read_signed_byte(self, s):
    
        return struct.unpack("<b", s)[0]
    
    def read_unsigned_byte(self, s):
    
        return struct.unpack("<B", s)[0]
    
    def read_unsigned_half_word(self, s):
    
        return struct.unpack("<H", s)[0]
    
    def read_signed_half_word(self, s):
    
        return struct.unpack("<h", s)[0]
    
    # Unit conversions
    
    def unit_to_mpt(self, n):

        """mpt_value = unit_to_mpt(value)
        
        Convert a measurement in Impression file units to millipoints.
        This method simply returns the input value but is used for clarity and
        to be consistent with the other conversion methods.
        """
        return n

    def unit_to_pt(self, n):

        """pt_value = unit_to_pt(value)
        
        Convert a measurement in Impression file units to points.
        """

        if type(n) == types.ListType:
        
            p = []
            for i in n:
                p.append(i/1000.0)
            p = tuple(p)
            
            return p
        
        else:
        
            return n / 1000.0

    def unit_to_in(self, n):

        """in_value = unit_to_in(value)
        
        Convert a measurement in Impression file units to inches.
        """

        i = self.unit_to_pt(n)

        if type(i) == types.ListType:

            return map(lambda x: x / 72.0, i)

        else:

            return i / 72.0

    def unit_to_cm(self, n):

        """cm_value = unit_to_cm(value)
        
        Convert a measurement in Impression file units to centimetres.
        """

        c = self.unit_to_in(n)

        if type(c) == types.ListType:

            return map(lambda x: x * 2.54, c)

        else:

            return c * 2.54

    def unit_to_mm(self, n):

        """mm_value = unit_to_mm(value)
        
        Convert a measurement in Impression file units to millimetres.
        """

        c = self.unit_to_in(n)

        if type(c) == types.ListType:

            return map(lambda x: x * 25.4, c)

        else:

            return c * 25.4

    def convert(self, units = 'pt'):

        """function = convert(units = 'pt')
        
        Return a function or method suitable for converting Impression coordinates to
        the units specified. The units parameter should be one of the following:
        
          "pt", "mpt", "in", "cm", "mm"
        
        An exception is raised if the units are not recognised.
        """

        if units == 'pt':

            return self.unit_to_pt

        elif units == 'mpt':

            return self.unit_to_mpt

        elif units == 'in':

            return self.unit_to_in

        elif units == 'cm':

            return self.unit_to_cm

        elif units == 'mm':

            return self.unit_to_mm

        else:

            raise ImpressionError, 'Unknown units "%s" given for conversion.' % units

    def word2rgb(self, word):

        """colour = word2rgb(word)
        
        Convert a word to RGB representation. The value returned will be an instance
        of the RGBTColour class.
        """
        red = (word >> 8) & 0xff
        green = (word >> 16) & 0xff
        blue = (word >> 24) & 0xff
        trans = word & 0xff

        return RGBTColour(red, green, blue, trans)

    def read_string(self, offset, length = None, ending = None, include = 1):

        """string = read_string(offset, length = None, ending = None, include = 1)
        
        Return a string from the object's internal data area, starting at the offset
        specified.
        
        If an ending character is given then data will be read until the ending is
        found. If a length is specified then this provides an additional constraint
        on the amount of data returned as a string.
        
        The include flag determines whether the ending, if given, is returned as
        part of the string.
        """

        if length == None and ending == None:
    
            print 'Internal: Incorrect use of the read_string function.'
            sys.exit()
    
        if length == None:
    
            # Read until one of the endings was found
            new = ''
            while offset < len(self.data):
    
                c = self.data[offset]
                if c in ending:
                
                    if include == 1:
                        new = new + c
                    break
                else:
                    new = new + c
                
                offset = offset + 1
    
            return new
    
        elif ending == None:
    
            # Read the number of characters specified
            return self.data[offset:offset+length]
    
        else:
            # Read the number of characters specified until an ending is encountered
            new = ''
            for i in range(length):
    
                c = self.data[offset]
                if c in ending:
                
                    if include == 1:
                        new = new + c
                    break
                else:
                    new = new + c
                
                offset = offset + 1
    
            return new
    
    def read_word(self, offset):

        """integer = read_word(offset)
        
        Read the four byte, little endian word stored at the specified offset in the
        object's data area and return it as a signed integer.
        """
        
        return self.read_signed_word(self.data[offset:offset+4])
    
    def read_words(self, offset, endings, include = 0):

        """words = read_word(offset, endings, include = 0)
        
        Read a sequence of four byte, little endian words beginning at the specified
        offset in the object's data area and return them as a list of signed integers.
        
        The sequence will be terminated if an integer matching any of those in the
        endings list is found. The terminating ending is included in the list of
        words if the include flag is set.
        """

        words = []
        
        while offset < len(self.data):
        
            word = self.read_word(offset)
            offset = offset + 4
            
            if word in endings:
            
                if include == 1:
                    words.append(word)
                break
            
            words.append(word)
        
        return words

    def count_bits(self, word):

        """number = count_bits(word)
        
        Count the number of consecutive bits which are set in the word from the
        lowest upwards.
        """

        word = long(word)
        if word < 0: word = word + 0x100000000L
        
        n = 0

        while word != 0:

            if word & 1 == 1:
                n = n + 1
                word = word >> 1
            else:
                break
        
        return n


class Colour:

    """Colour
    
    The base class for Impression colour classes.
    """
    pass


class RGBTColour(Colour):

    """RGBTColour(Colour)
    
    colour = RGBTColour(red, green, blue, trans)
    
    A class which is used to represent the way colours are stored in
    Impression documents.
    
    Red, green and blue values should be between zero and 255 inclusive.
    
    The transparent value is effectively a boolean value since only transparent
    and opaque colours are supported. 
    """

    def __init__(self, red, green, blue, trans):

        self.red = red
        self.green = green
        self.blue = blue
        self.trans = (trans != 0)

    def __repr__(self):

        if self.trans == 1:

            return '<RGBTColour: transparent>'

        else:

            return '<RGBTColour: %i %i %i>' % (self.red, self.green, self.blue)


class BBox(Common):

    """BBox(Common)
    
    box = BBox(xmin, ymin, xmax, ymax)
    
    An object used for representing rectangular regions in Impression
    documents.
    
    The horizontal minimum (xmin), vertical minimum (ymin), horizontal
    maximum (xmax) and vertical maximum (ymax) values are all specified in
    millipoints.
    """

    def __init__(self, xmin, ymin, xmax, ymax):

        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax

    def __repr__(self):

        return 'BBox (%f pt, %f pt) (%f pt, %f pt)' % (
            self.unit_to_pt(self.xmin), self.unit_to_pt(self.ymin),
            self.unit_to_pt(self.xmax), self.unit_to_pt(self.ymax)
            )

    def width(self):

        return self.xmax - self.xmin

    def height(self):

        return self.ymax - self.ymin
    
    def relative_to(self, p, units = 'mpt'):

        if units == 'mpt':

            return BBox(
                self.xmin - p[0], self.ymin - p[1], self.xmax - p[0], self.ymax - p[1]
                )

        else:

            return BBox(
                self.xmin - (p[0] * 1000), self.ymin - (p[1] * 1000),
                self.xmax - (p[0] * 1000), self.ymax - (p[1] * 1000)
                )


class Page(Common):

    """Page(Common)
    
    page = Page(data, addr)
    
    A class containing methods common to the MasterPage and ChapterPage
    classes. This class should be subclassed rather than used directly.
    
    The data parameter is a string containing the binary page data from the
    Impression document. The addr parameter is a tuple; the first element of
    which is the address of the data within the original document, the second
    is the name of the file from which the data originated.
    """

    def __init__(self, data, addr):

        # Store the data.
        self.data = data

        # Store the filename and offset within the file.
        self.addr = addr

        # Interpret the data.
        self.interpret()

        # Delete the reference to the data.
        del self.data

    def interpret(self, page_type):

        i = 0

        # Unknown word
        u0 = self.read_word(i)

        i = i + 4

        # Page area
        x1, y1 = self.read_word(i), self.read_word(i+4)
        x2, y2 = self.read_word(i+8), self.read_word(i+12)

        i = i + 16

        if page_type == 'Master':

            # Definition number
            defn1 = self.read_word(i)
            defn2 = self.read_word(i+4)

        elif page_type == 'Chapter':

            # Definition number
            defn1 = self.read_word(i)

            # Chapter and page numbers for this page
            chapter_page = self.read_word(i + 4)
            self.chapter = (chapter_page & 0xffff0000L) >> 16
            self.page = chapter_page & 0xffff

        i = i + 8

        # Define page details
        self.number = defn1

        # Impression appears to store the master pages in a form in which
        # each page is positioned below the previous one. Therefore, the
        # frames appear to be positioned peculiarly. This is compensated for
        # using the page margins which were given above.
        #
        # The page "bleed" means that the origin should be moved into the
        # page. This means we add this value to the horizontal coordinate
        # and subtract it from the vertical coordinate.
        #
        # + origin
        #
        #    + origin after page bleed has been taken into account
        #
        #
        #          +----+ frame
        #          |    |
        #          +----+

        self.margins = BBox(x1, y1, x2, y2)

        self.bleed = 0

        self.origin = (
                self.margins.xmin + self.bleed,
                self.margins.ymax - self.bleed
                )

        self.frames = []

        # Extra information

        if i >= len(self.data): return

        # "Bleed" around page. This will have been implicitly added to the
        # frame definition coordinates so will have to be subtracted from them.
        bleed = self.read_word(i)

        i = i + 4

        # Unknown word (possibly horizontal or vertical grid spacing)
        grid_x = self.read_word(i)
        z = self.read_word(i+4)

        i = i + 8

        # Paper name (assume 12 bytes)
        name = self.read_string(i, length = 12, ending = '\0', include = 0)

        i = i + 12

        # Unknown word (possibly horizontal or vertical grid spacing)
        grid_y = self.read_word(i)
        z = self.read_word(i+4)

        i = i + 8

        # Zero words
        z = self.read_word(i)
        z = self.read_word(i+4)

        self.name = name
        self.bleed = bleed
        self.grid = (grid_x, grid_y)


class MasterPage(Page):

    """MasterPage(Page)
    
    page = MasterPage(data, addr)
    
    A class describing a master page within an Impression document.
    
    The data parameter is a string containing the binary page data from the
    Impression document. The addr parameter is a tuple; the first element of
    which is the address of the data within the original document, the second
    is the name of the file from which the data originated.
    """

    def __repr__(self):

        return '<MasterPage at %x within file "%s">' % self.addr

    def interpret(self):

        Page.interpret(self, 'Master')


class ChapterPage(Page):

    """ChapterPage(Page)
    
    page = ChapterPage(data, addr)
    
    A class describing the pages within an Impression document.
    
    The data parameter is a string containing the binary page data from the
    Impression document. The addr parameter is a tuple; the first element of
    which is the address of the data within the original document, the second
    is the name of the file from which the data originated.
    """

    def __repr__(self):

        return '<ChapterPage at %x within file "%s">' % self.addr

    def interpret(self):

        Page.interpret(self, 'Chapter')



class Frame(Common):

    """Frame(Common)
    
    frame = Frame(data, data_type, belongs_to, document_version)
    
    A class describing page layout frames in an Impression document.
    
    The frame data is supplied as a string containing the binary frame data
    from the Impression document. The data_type is provided by the
    instantiating object as read from the page descriptions.
    
    The belongs_to parameter is the page in which this frame is used.
    
    The document version is obtained from the original document and is used
    to control how the frame data is interpreted.
    """

    def __init__(self, data, data_type, belongs_to, document_version):

        # Store the data.
        self.data = data

        # Store the data type.
        self.data_type = data_type

        # Interpret the data type: is the frame a guide frame?
        self.guide = (self.data_type & 0x08) != 0

        # Record the document version.
        self.document_version = document_version

        # If this frame does not belong to a master page then place the page origin
        # at (0, 0).
        if belongs_to is None:

            self.page_origin = (0.0, 0.0)

        else:

            self.page_origin = belongs_to.origin

        # Interpret the data.
        self.interpret()
    
    def info(self, units = 'pt'):

        """info(units = 'pt')
        
        Print basic information on the frame's properties using the units specified
        where relevant.
        """

        # Select a units conversion method.
        convert = self.convert(units)

        # Decode the flags word.

        # Bit     Value (description)
        # 0
        # 1       0 (passive frame)     1 (repel text outside frame)
        # 2       0 (transparent frame) 1 (colour frame)

        flags_str = ''

        if self.flags & 0x2:

            flags_str = flags_str + '  * repels text\n'

        if self.flags & 0x4:

            flags_str = flags_str + '  * with colour %s\n' % repr(self.colour)

        else:

            flags_str = flags_str + '  * is transparent\n'

        if self.local:

            flags_str = flags_str + '  * is a local frame\n'

        if self.flags & 0x200:

            flags_str = flags_str + '  * is locked in place\n'

        if self.guide:

            frame = 'Guide frame'

        else:

            frame = 'Frame'

        # Determine reasonable coordinates for the page by
        # determining the position of the two points which construct the
        # bounding box relative to the top left of the page margins bounding
        # box.

        inner_bbox = self.inner_bbox.relative_to(self.page_origin)

        print '%s %i with origin within page at ' % (frame, self.frame_number) + \
                '(%.2f %s, %.2f %s), width = %.2f %s, height = %.2f %s.\n\n%s' % (
                    convert(inner_bbox.xmin), units,
                    convert(inner_bbox.ymax), units,
                    convert(inner_bbox.width()), units,
                    convert(inner_bbox.height()), units,
                    flags_str
                )

    def interpret(self):

        i = 0

        # Offset to the relevant data in the content indexed.
        content_offset = self.read_word(i)

        i = i + 4

        # Inner bounding box to contain the page content
        x1, y1 = self.read_word(i), self.read_word(i+4)
        x2, y2 = self.read_word(i+8), self.read_word(i+12)
        self.inner_bbox = BBox(x1, y1, x2, y2)

        i = i + 16

        # Flags word
        flags = self.read_word(i)
        has_border_colour = (flags & 0x04) != 0
        self.local = (flags & 0x100) == 0

        #if self.document_version == 14: # 0x0e
        #
        #    self.local_content = (flags & 0x80) != 0
        #
        #elif self.document_version == 20: # 0x14
        #
        #    self.local_content = (flags & 0x48000) != 0
        #
        #elif self.document_version == 28: # 0x1c
        #
        #    self.local_content = (flags & 0x08) == 0
        #
        #else:
        #
        #    self.local_content = 0

        # The number of the frame in the display order is found from the flags word.
        frame_number = (flags & 0xff000000L) >> 24

        i = i + 4

        # Frame content: 0xffffffff (-1) for no content. See also bit 0x08 in the flags word.
        content_item = self.read_word(i)

        i = i + 4

        # Outer bounding box for decorative borders

        # Outer frame, taking borders into account (should be the same as the
        # inner frame if there are no borders)
        x1, y1 = self.read_word(i), self.read_word(i+4)
        x2, y2 = self.read_word(i+8), self.read_word(i+12)
        self.outer_bbox = BBox(x1, y1, x2, y2)

        i = i + 16

        # Flags word
        #if self.document_version != 14: # 0x0e
        #
        #    self.local_content = (self.read_word(i) & 0xff) == 0

        #self.local = (self.read_word(i) & 0x070000) != 0

        i = i + 4

        # Passive frame or frame that repels text.
        if (flags & 0x02) != 0:
        
            self.repel_text = 1
        
        else:
        
            self.repel_text = 0
        
        # Colour
        if (flags & 0x04) != 0:

            self.colour = self.word2rgb(self.read_word(i))
            self.transparent = 0

        else:

            self.colour = RGBTColour(255, 255, 255, 255)
            self.transparent = 1

        i = i + 4

        # Frame inset values

        self.inset_h = self.read_word(i)
        self.inset_v = self.read_word(i+4)

        i = i + 8

        # Borders (0xff for none, n-1 for border n)
        # Written in big endian form:
        # (b)ottom (r)ight (l)eft (t)op
        # bbrrlltt

        self.borders = \
        {
            'top':    self.read_unsigned_byte(self.data[i]),
            'left':   self.read_unsigned_byte(self.data[i+1]),
            'right':  self.read_unsigned_byte(self.data[i+2]),
            'bottom': self.read_unsigned_byte(self.data[i+3])
        }

        i = i + 4

        # Border colour
        border_colour = self.word2rgb(self.read_word(i))

        i = i + 4

        # Define frame attributes.

        # Frame colour
        if has_border_colour:
        
            self.border_colour = border_colour

        # Frame flags
        self.flags = flags

        # Frame number
        self.frame_number = frame_number

        # Content of frame
        #self.content_flags = content_flags
        #
        #if content_flags != -1 and (flags & 0x8) == 0:
        if content_item != -1:

            self.content_ref = (content_item, content_offset)

        # Frames not containing images have no further information.
        if self.data_type != 3: return

        # Read extra information
        if self.document_version == 14: # 0x0e

            i = i + 0x0c
            
        else:

            i = i + 0x24

        # Read horizontal scale factor as a percentage of the standard height.
        #self.y_scale = (self.read_word(i) * 100.0) / 0x40000
        self.y_scale = 0x10000 * (100.0 / self.read_word(i))

        i = i + 4

        # Read horizontal scale factor as a percentage of the standard width.
        #self.x_scale = (self.read_word(i) * 100.0) / 0x40000
        self.x_scale = 0x10000 * (100.0 / self.read_word(i))

        i = i + 4

        # Read the horizontal displacement of the scaled, rotated image (-x) within the frame from the
        # left hand edge (positive values correspond to displacement to the right).

        self.x_displacement = -self.read_word(i)

        i = i + 4

        # Read the vertical displacement of the scaled, rotated image (-y) within the frame from the
        # bottom edge (positive values correspond to displacement upwards).

        self.y_displacement = -self.read_word(i)

        i = i + 4

        # [The original non-negated values could be considered to be the translation of the origin of
        # the page in the Drawfile used by Impression.]

        if self.document_version > 14: #0x0e

            # Read the angle of rotation of the image about its origin (always positive, anticlockwise
            # rotation).
            self.angle = (self.read_word(i) & 0xffff0000L) >> 16

        else:

            self.angle = 0
        


class LineInfo(Common):

    """LineInfo(Common)
    
    lineinfo = LineInfo(data)
    
    A class describing the vertical positioning of text lines on a page.
    
    The data is supplied as a string containing binary data from the
    original document.
    """

    def __init__(self, data):
    
        # Keep a copy of the data.
        self.data = data
        
        if type(data) == types.StringType:
        
            self.read_string()
        
        else:
        
            self.words = data
        
        # Some sort of length (not in millipoints)
        # Vertical line minimum (millipoints)
        # Vertical line maximum (millipoints)
        # Baseline displacement downwards (millipoints)
        
        self.strlen = self.words[0]
        self.ymin = self.words[1]
        self.ymax = self.words[2]

        # Sometimes, some of the top eight bits of this word are set, for some
        # unknown purpose.
        self.baseline = self.words[3] & 0x00ffffff
    
    def __repr__(self):
    
        return "<LineInfo 0x%x %f %f 0x%x>" % (self.strlen, self.ymin, self.ymax, self.baseline)

    def read_string(self):
    
        i = 0
        
        self.words = []
        
        while i < len(self.data):
        
            self.words.append(self.read_word(i))
            i = i + 4


class TextSetup(Common):

    """TextSetup(Common)
    
    setup = TextSetup(data, addr)
    
    A description of the text style used at the beginning of a frame.
    
    The data parameter is a string containing the binary page data from the
    Impression document. The addr parameter is a tuple; the first element of
    which is the address of the data within the original document, the second
    is the name of the file from which the data originated.
    """

    def __init__(self, data, addr):
    
        self.data = data

        # Store the filename and offset within the file.
        self.addr = addr

        # Try to interpret the raw data.
        self.interpret()

    def __repr__(self):
    
        return "<TextSetup: %s>" % repr(self.text)

    def interpret(self):

        self.text = []

        i = 0

        # Read the reference to the frame which contains this content.
        self.text.append( ['frame offset', self.read_word(i) + 4] )

        # Other words, following a zero word and preceding another, contain style hints.
        i = 8

        while i < len(self.data):

            word = self.read_word(i)

            if word == 0:

                break

            # Assume that this word refers to a style to be applied.

            # The first byte of each word represents the style or effect.
            style = word & 0xff

            # The second byte appears to be some sort of counter which represents the
            # instance of a style application, beginning when the style is set and
            # ending when it is unset.
            counter = (word & 0xffffff00L) >> 8

            self.text.append( ['apply style', (counter, style)] )

            i = i + 4


class Text(Common):

    """Text(Common)
    
    text = Text(data, block_type, lineinfo, addr, first_line, trailing)
    
    A class used to describe the properties of a line of text found in a
    document.
    
    The data parameter is a string containing the binary page data from the
    Impression document.
    
    The block_type describes whether the line is at the beginning or end of
    a paragraph and is derived from the corresponding word found in the
    relevant content area.
    
    The addr parameter is a tuple; the first element of which is the address
    of the data within the original document, the second is the name of the
    file from which the data originated.
    
    The first_line and trailing flags actually specify whether the line is a
    leading or trailing line in a paragraph and have usually been
    interpreted from the block_type value. Note that the first_line flag is
    only really useful for the first line in a frame.
    
    The text and various commands desribing the line content are stored in
    the "text" attribute of the created object. This attribute is a list in
    which pieces of text are represented as strings but where any other type
    of information is stored as a sequence (a tuple or list). The first
    elements in these sequences may be treated as a command with the other
    elements as arguments. Examples of these commands and their arguments
    follow:
    
        Command       Arguments
      "apply style"   One or more tuples of the form (counter, style) of
                      which the first tuple is usually ignored. The style is
                      an offset into the original document's style table but
                      this information can be used with a StyleCollection
                      object to obtain the appropriate style.
    
      "page number"   An string describing the format to be used when
                      describing the current page number. This will be
                      "arabic", "roman" or "Roman" for numbers of the form
                      "123", "iv" and "IV" respectively.
    
     "horizontal space"
    
                      An integer measurement of the number of millipoints
                      between the previous piece of text and the next. This
                      integer may be negative.
    
     "embedded frame" Six integers, the last two of which are the respective
                      width and height of the embedded frame. This is used
                      to place gaps in the text and does not necessarily
                      refer to the frame or its content.
    
     "special character"
    
                      A special character is given as a string.
    
        "margins"     The left and right margins are given as integer
                      adjustments, specified in millipoints, to the
                      horizontal frame boundaries after horizontal frame
                      inset has been taken into account.
    
                      If the right margin is a positive value then it is
                      measured as a rightwards adjustment to the left hand
                      frame boundary and inset, otherwise it is measured from
                      the right hand frame boundary and inset. The left
                      margin is always measured from the left hand frame
                      boundary and inset.
    
         "kern"       The kerning applied to the following text given as
                      horizontal and vertical displacments, specified for the
                      first two words in thousandths of an em then, for the
                      last two words, in millipoints.
    
    Other commands are just the hexadecimal form of the words read from the
    original document and given as strings.
    """

    #special = "\x13\x14\x15\x16\x17\x18\x1d"

    endings = "\x01\x02\x03\x04\x06\x07\x08\x09\x0a\x0b\x0c\x0e\x0f\x10" + \
              "\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f"
    
    formatting = "\x05"
    
    all_endings = endings + formatting + "\x00"
    
    #all_endings = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c" + \
    #              "\x0e\x0f\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b" + \
    #              "\x1c\x1d\x1e\x1f"

    def __init__(self, data, block_type, lineinfo, addr, first_line, trailing):
    
        self.data = data

        # Record the type of text contained in this object.
        self.block_type = block_type

        # Record information about the positioning of the line of text.
        self.lineinfo = lineinfo

        # Store the filename and offset within the file.
        self.addr = addr

        # Record whether this is the first line in a paragraph.
        self.first_line = first_line

        # Record whether this is a trailing line.
        self.trailing = trailing

        # Try to interpret the raw data.
        self.interpret()
    
    def __repr__(self):
    
        return "<Text (%x): %s>" % (self.block_type, repr(self.text))

    def interpret(self):
    
        self.text = []
        
        i = 0
        
        while i < len(self.data):
        
            byte = ord(self.data[i])
            
            words = []
            #print i, hex(byte)
            # Compare the byte against various control codes.
            
            if byte == 0x00:
            
                # String terminator: move to the next word.
                i = i + 4 - (i % 4)
            
            elif byte == 0x05:
            
                # Padding character
                words = ['0x05']
                i = i + 1
            
            elif byte == 0x07:
            
                # Found in a word with following bytes typically at the
                # end of a string. Sets/overlays a style or effect to be used.
                
                # Following data is word-aligned and zero word terminated.
                i = i + 4 - (i % 4)
                
                decode_words = self.read_words(i, endings = [0], include = 0)
                
                i = i + (len(decode_words) * 4) + 4

                words = ['apply style']

                for word in decode_words:

                    # The first byte of each word represents the style or effect.
                    style = word & 0xff

                    # The second byte appears to be some sort of counter which represents the
                    # instance of a style application, beginning when the style is set and
                    # ending when it is unset.
                    counter = (word  >> 8) & 0xffffff00L

                    words.append( (counter, style) )

            elif byte == 0x08:
            
                # Found in a word with following bytes typically at the
                # end of a string. Unsets a style or effect in use.
                
                # Following data is word-aligned and zero word terminated.
                i = i + 4 - (i % 4)
                
                decode_words = self.read_words(i, endings = [0], include = 0)
                
                i = i + (len(decode_words) * 4) + 4
                
                words = ['apply style']

                for word in decode_words:

                    # The first byte of each word represents the style or effect.
                    style = word & 0xff

                    # The second byte appears to be some sort of counter which represents the
                    # instance of a style application, beginning when the style is set and
                    # ending when it is unset.
                    counter = (word & 0xffffff00L) >> 8 

                    words.append( (counter, style) )

            elif byte == 0x0b:

                # Page number directive

                # Read the following word to discover the form of number to be used.
                i = i + 4 - (i % 4)

                number_type = ('arabic', 'Roman', 'roman')[(self.read_word(i) & 0xff00) >> 16]

                i = i + 4

                words = ['page number', number_type]

            elif byte == 0x12:
            
                # Found in a word with following bytes typically at the
                # end of a string.
                
                # Following data is word-aligned and zero word terminated.
                i = i + 4 - (i % 4)
                
                words = self.read_words(i, endings = [0], include = 0)
                
                i = i + (len(words) * 4) + 4
                
                words = ['horizontal space', words[0]]
                
                # String data may follow.
            
            elif byte == 0x13:
            
                # Usually found as 00000013 followed by word-aligned data.
                # 
                # 00000013 word1 word2
                #
                # or
                #
                # 00000013 00000018 ...
                
                i = i + 4 - (i % 4)
                
                #if self.data[i] != 0x18:
                #
                #    words = [self.read_word
                
                # Space for embedded frame?

                words = []
                
                for j in range(0, 6):

                    words.append(self.read_word(i))
                    i = i + 4
            
                # The last two words contain the width and height of the frame. 

                words = ['embedded frame'] + words

            elif byte == 0x14:
            
                # Usually found as 00000014 followed by word-aligned data
                # and zero word terminated.
                # 
                # 00000014 words 00000000

                # Special character follows this one.
                i = i + 1

                special = self.data[i]

                i = i + 4 - (i % 4)
                
                #words = self.read_words(i, endings = [0], include = 0)
                #
                #i = i + (len(words) * 4) + 4
                
                words = ['special character', special]
            
            elif byte == 0x15:
            
                # Two following words which may be the left and right margins respectively.
                i = i + 4 - (i % 4)
                
                for j in range(0, 2):
                
                    words.append(self.read_word(i))
                    i = i + 4

                words = ['margins'] + words
            
            elif byte == 0x16:
            
                # Usually found as 00000016 followed by word-aligned data, such as the left margin
                # displacement, terminated with a zero word. String data usually follows.
                
                i = i + 4 - (i % 4)
                
                # Assume two words follow.

                for j in range(0, 2):

                    words.append(self.read_word(i))
                    i = i + 4
                
                words = ['line width'] + words
                
                # String data usually follows.
            
            elif byte == 0x18:
            
                # Usually found as 00000018 with word-aligned zero terminated
                # data following which contains other codes. e.g. 0x13 and 0x14.
                # 
                # 00000018 <6 words>
                
                i = i + 4 - (i % 4)

                for j in range(0, 5):
                
                    words.append(self.read_word(i))
                    i = i + 4

                words = ['0x18'] + map(hex, words)

            elif byte == 0x1d:

                # Kerning

                i = i + 4 - (i % 4)
                
                # Four words follow:
                #
                # horizontal kerning in thousandths of an em,
                # vertical kerning in thousandths of an em,
                # horizontal kerning in millipoints(?),
                # vertical kerning in millipoints(?).

                words = \
                [
                    'kern', self.read_word(i), self.read_word(i + 4),
                    self.read_word(i + 8), self.read_word(i + 12)
                ]

                i = i + 16

            elif chr(byte) in self.endings:
            
                # Usual read word-aligned zero terminated bytes.
                #i = i + 4 - (i % 4)
                #
                #words = self.read_words(i, endings = [0], include = 0)
                #
                #i = i + (len(words) * 4) + 4
                #
                #words = map(hex, [byte] + words)
                words = [hex(byte)]
                i = i + 1
            
            else:
            
                # String data
                
                # Read text until an ending character is found.
                text = self.read_string(
                    i, ending = self.all_endings, include = 0 )
                    
                words = text
                
                i = i + len(text)

            
            # Add the text to the list.
            if words != []:
                self.text.append(words)
        

class Style(Common):

    """Style(Common)
    
    style = Style(data, addr, document_version, base = 0)
    
    The data parameter is a string containing the binary page data from the
    Impression document. The addr parameter is a tuple; the first element of
    which is the address of the data within the original document, the second
    is the name of the file from which the data originated.
    
    The document_version is found from the original document and will be used
    to determine how the data is interpreted.
    
    The base flag also determines how the data will be interpreted by
    ignoring data which the author could not decipher but which almost always
    appears in the definition for the "BaseStyle" or "Normal" style, upon
    which all the other styles are based.
    """

    justify_list = ['left', 'centre', 'right', 'full']
    tab_type_list = ['left', 'centre', 'right', 'decimal', 'vertical rule']
    
    def __init__(self, data, addr, document_version, base = 0):
    
        # Offset into the file.
        self.addr = addr

        # Actual data.
        self.data = data

        # Record the document version.
        self.document_version = document_version

        # Remember whether this is the base style.
        self.base = base

        # Try to interpret the data.
        self.interpret()

        # Delete the reference to the data.
        del self.data

    def __repr__(self):
    
        return '<Style: %x in %s>' % self.addr

    def create_class(self):

        """class = create_class()
        
        Create a new class from an instance of this class with all the attributes
        which were interpreted from the style definition filled in as default
        values.
        
        This scheme is useful when extracting style information from documents as
        it allows styles to be layered by deriving a subclass from the existing
        style and the new style to be applied. For example:
        
        # The new style attributes should override the old ones (left to right
        # inheritance lookup).
        
            class TextStyle(new_style, current_style):
        
                pass
        
            current_style = TextStyle
        
        The newly redefined current_style can then be instantiated as required.
        """

        class TextStyle(Style):

            def __init__(self):

                # The style details could be re-read here, or put off being read until
                # this style is instantiated. However, having the style verified earlier should
                # reveal any problems before the style is being relied on by applications.
                pass

            def __repr__(self):

                return '<TextStyle %s>' % self.name

        # Copy the methods and attributes which are not common to both this instance and the new
        # class from this instance to the new class.

        instance_keys = self.__dict__.keys()
        new_keys = TextStyle.__dict__.keys()

        for key in instance_keys:

            if key not in new_keys:

                TextStyle.__dict__[key] = self.__dict__[key]

        # Return the class to use.
        return TextStyle


    def info(self, units = 'pt'):

        # Select a units conversion method.
        convert = self.convert(units)

        print 'Style "%s"' % self.name
        print
        print 'Found at 0x%x in "%s"' % self.addr
        print
        print 'Specifies:'
        print

        if self.on_menu:
            print 'Shown on Impression style menu.'

        if hasattr(self, 'shortcut'):
            print 'Impression keyboard shortcut: %s' % self.shortcut

        if self.paragraph_apply:
            print 'Applied to whole paragraphs.'

        print
        print 'Font:'
        print

        if hasattr(self, 'font_name'):

            print 'Font name: %s' % self.font_name

        if hasattr(self, 'bold_on'):

            print 'This style is a bold variant of another style (%s)' % \
                ('off', 'on')[self.bold_on]

        if hasattr(self, 'italic_on'):

            print 'This style is an italic variant of another style (%s)' % \
                ('off', 'on')[self.italic_on]

        if hasattr(self, 'font_size'):

            print 'Font size: %.2f %s' % (convert(self.font_size), units)

        if hasattr(self, 'aspect_ratio'):

            print 'Font aspect ratio: %.2f%%' % self.aspect_ratio

        if hasattr(self, 'subscript'):
            print 'The style is a form of subscript (%s).' % ('off', 'on')[self.subscript]

        if hasattr(self, 'superscript'):
            print 'The style is a form of superscript (%s).' % ('off', 'on')[self.superscript]

        if hasattr(self, 'script_aspect'):

            print 'Subscript and superscript have sizes which are %.2f%% of normal text.' % \
                self.script_aspect

        if hasattr(self, 'subscript_offset'):

            print 'Subscript is displaced above normal text by %.2f%% of its height.' % \
                self.subscript_offset

        if hasattr(self, 'superscript_offset'):

            print 'Superscript is displaced above normal text by %.2f%% of its height.' % \
                self.superscript_offset

        if hasattr(self, 'foreground_colour'):

            print 'Foreground colour: %s' % self.foreground_colour

        if hasattr(self, 'background_colour'):

            print 'Background colour: %s' % self.background_colour


        print
        print 'Text:'
        print

        if hasattr(self, 'underline'):
            print 'Text is underlined with level %i.' % self.underline

        if hasattr(self, 'underline_offset'):

            print 'Underlining occurs %.2f%% of the text size below the text.' % self.underline_offset

        if hasattr(self, 'underline_size'):

            print 'The thickness of underlining is %.2f%% of text size.' % self.underline_size

        if hasattr(self, 'strikeout'):
            print 'Text is crossed out (%s).' % ('off', 'on')[self.strikeout]

        if hasattr(self, 'strikeout_colour'):

            print 'Underline/strikeout colour: %s' % self.strikeout_colour

        if hasattr(self, 'tracking'):

            print 'Tracking: %.2f/1000 em' % self.tracking

        if hasattr(self, 'hyphenation'):

            print 'Text in this style can be split across lines using hyphenation (%s).' % \
                ('off', 'on')[self.hyphenation]
            
        if hasattr(self, 'leadering'):

            print 'Leadering string: %s' % self.leadering

        if hasattr(self, 'decimal_tab'):

            print 'The decimal tab is "%s" for this style.' % self.decimal_tab

        if self.has_index_label:
            print 'Show text in the index.'

        if self.has_contents_label:
            print 'Show text at level %i in the contents.' % self.contents_labels_level


        print
        print 'Paragraph:'
        print

        if hasattr(self, 'above_para'):

            print 'Space above paragraphs in this style: %.2f %s' % (convert(self.above_para), units)

        if hasattr(self, 'below_para'):

            print 'Space below paragraphs in this style: %.2f %s' % (convert(self.below_para), units)

        if hasattr(self, 'justification'):

            print 'Text is aligned using the "%s" scheme.' % self.justification

        if hasattr(self, 'line_spacing'):

            print 'Line spacing: %.2f %s' % (self.line_spacing, self.line_spacing_units)

        if hasattr(self, 'page_grid_lock'):

            print 'Text in this style is locked to the page grid (%s).' % \
                ('off', 'on')[self.page_grid_lock]

        if hasattr(self, 'keep_together_length'):

            print 'Keep paragraphs together up to length: %.2f %s' % \
                (convert(self.keep_together_length), units)

        if hasattr(self, 'keep_together_single_para'):

            print 'Keep single paragraphs together (%s).' % \
                ('off', 'on')[self.keep_together_single_para]

        if hasattr(self, 'keep_together_multiple_para'):

            print 'Keep multiple paragraphs together (%s).' % \
                ('off', 'on')[self.keep_together_multiple_para]

        if hasattr(self, 'keep_with_next_para'):

            print 'Keep paragraphs together with following ones (%s).' % \
                ('off', 'on')[self.keep_with_next_para]


        print
        print 'Ruler:'
        print

        if hasattr(self, 'auto_indent'):
            print 'Automatically indents text (%s).' % ('off', 'on')[self.auto_indent]

        if hasattr(self, 'first_line_left_margin'):

            print 'First line left hand margin: %.2f %s' % (convert(self.first_line_left_margin), units)

        if hasattr(self, 'left_margin'):

            print 'Left hand margin: %.2f %s' % (convert(self.left_margin), units)

        if hasattr(self, 'right_margin'):

            print 'Right hand margin: %.2f %s' % (convert(self.right_margin), units)

        if hasattr(self, 'left_rule_off_margin'):

            print 'Left vertical rule margin: %.2f %s' % (convert(self.left_rule_off_margin), units)

        if hasattr(self, 'right_rule_off_margin'):

            print 'Right vertical rule margin: %.2f %s' % (convert(self.right_rule_off_margin), units)

        if self.tab_defns != []:

            print 'Tab definitions:'
            print

            for position, tab_type in self.tab_defns:

                print '  %s at %.2f %s' % (tab_type, convert(position), units)

            print


        print
        print 'Rule-offs:'
        print

        if hasattr(self, 'rule_off_thickness'):

            print 'Rule-off thickness: %.2f %s' % (convert(self.rule_off_thickness), units)

        if hasattr(self, 'rule_off_above'):

            print 'A rule-off appears above text (%s).' % ('off', 'on')[self.rule_off_above]

            if hasattr(self, 'rule_off_above_offset'):

                print 'Rule-off above text, offset: %.2f %s' % \
                    (convert(self.rule_off_above_offset), units)

        if hasattr(self, 'rule_off_below'):

            print 'A rule-off appears below text (%s).' % ('off', 'on')[self.rule_off_below]

            if hasattr(self, 'rule_off_below_offset'):

                print 'Rule-off below text, offset: %.2f %s' % \
                    (convert(self.rule_off_below_offset), units)

        if hasattr(self, 'rule_off_colour'):

            print 'Rule-off colour: %s' % self.rule_off_colour

        if hasattr(self, 'vertical_rule_off_width'):

            print 'Vertical rule-off width: %.2f %s' % (convert(self.vertical_rule_off_width), units)

        print

    def interpret(self):

        # Flags word (0x0)

        # Flags discovered: 0100 1111 1101 0111 0111 1111 1000 0010

        # Whether the style is shown on the Style menu is defined in the flags word at 0x0.
        self.on_menu = (self.read_word(0x0) & 0x40000000) != 0

        # Whether the style can have bold or italic effects applied to it is defined in the
        # flags word at 0x0. If these are not set then normal behaviour applies.
        has_bold = (self.read_word(0x0) & 0x08000000) != 0
        has_italic = (self.read_word(0x0) & 0x04000000) != 0

        # Override rule-offs
        has_rule_offs = (self.read_word(0x0) & 0x02000000) != 0
        # Also, the bits at 0x00088400 are set in 0x4 when any rule-offs are enabled.

        # Override lock to page grid
        has_page_grid_lock = (self.read_word(0x0) & 0x01000000) != 0

        # Background colour flag
        has_background_flag1 = ((self.read_word(0x0) & 0x800000) != 0)

        # Override rule-off colour
        has_rule_off_colour = (self.read_word(0x0) & 0x400000) != 0

        # Override underline/strikeout colour
        has_underline_strikeout_colour = (self.read_word(0x0) & 0x100000) != 0

        # Foreground colour flag
        has_foreground_flag1 = (self.read_word(0x0) & 0x040000) != 0

        # New keep together settings
        has_keep_with_next_para = (self.read_word(0x0) & 0x020000) != 0

        # Override decimal tab setting
        has_decimal_tab = (self.read_word(0x0) & 0x010000) != 0

        # Override hyphenation
        has_hyphenation = (self.read_word(0x0) & 0x4000) != 0

        has_keep_single_para = (self.read_word(0x0) & 0x1000) != 0
        has_keep_multiple_para = (self.read_word(0x0) & 0x2000) != 0

        # Override alignment
        has_alignment = (self.read_word(0x0) & 0x0800) != 0

        # Override strikeout
        has_strikeout = (self.read_word(0x0) & 0x0400) != 0

        # Override script settings
        has_script = (self.read_word(0x0) & 0x0200) != 0

        # Override underline
        has_underline = (self.read_word(0x0) & 0x0100) != 0

        # Unknown font name flag
        has_font_name_flag1 = ((self.read_word(0x0) & 0x80) != 0)

        # Whether auto indent is used is defined at 0x0.
        has_auto_indent = (self.read_word(0x0) & 0x02) != 0



        # Flags word (0x4)
        # Flags discovered: 0000 0011 0110 ?111 0111 1011 1111 1111

        # New text colours (some strange interpretation of the flags is required)

        has_foreground_flag2 = ((self.read_word(0x4) & 0x01000000) != 0)
        has_foreground_colour = has_foreground_flag1 or has_foreground_flag2
        #has_foreground_colour = ((self.read_word(0x4) & 0x03000000) != 0)

        has_background_flag2 = ((self.read_word(0x4) & 0x02000000) != 0)
        has_background_colour = has_background_flag1 and has_background_flag2 

        # New tracking in thousandths of an em.
        has_tracking = (self.read_word(0x4) & 0x400000) != 0

        has_font_name_flag2 = ((self.read_word(0x4) & 0x200000) != 0)

        if self.document_version == 20: # 0x14

            has_font_name = has_font_name_flag1

        elif self.document_version >= 28: # 0x1c

            has_font_name = has_font_name_flag1 and has_font_name_flag2

        else:
        
            has_font_name = has_font_name_flag1 and has_font_name_flag2

        # Some unknown property
        has_unknown_flag_080000 = (self.read_word(0x4) & 0x080000) != 0

        # New vertical rule-off width
        has_vertical_rule_off_width = (self.read_word(0x4) & 0x040000) != 0

        # New leadering string
        has_leadering = (self.read_word(0x4) & 0x020000) != 0

        # New keep together settings
        has_keep_together = (self.read_word(0x4) & 0x010000) != 0 

        # If defined, the aspect ratio of the text (font width expressed as a percentage of the font height)
        # is at 0x90.
        has_aspect_ratio = (self.read_word(0x4) & 0x4000) != 0

        # If defined in the word at (0x4), the text size is specified for this style at 0x8c,
        # otherwise it is inherited from the base style.
        has_font_size = (self.read_word(0x4) & 0x2000) != 0

        # Rule-off margins are defined in this word.
        has_right_rule_off_margin = (self.read_word(0x4) & 0x1000) != 0
        has_left_rule_off_margin = (self.read_word(0x4) & 0x0800) != 0

        # New underline offset and size
        has_underline_offset = (self.read_word(0x4) & 0x0300) == 0x300

        # New space above paragraph definition.
        has_space_above_para = (self.read_word(0x4) & 0x80) != 0

        # New space below paragraph definition.
        has_space_below_para = (self.read_word(0x4) & 0x40) != 0

        # New line spacing is determined by a flag in 0x4.
        has_line_spacing = (self.read_word(0x4) & 0x20) != 0

        # New script size
        has_script_size = (self.read_word(0x4) & 0x10) != 0

        # New script offset
        has_script_offset = (self.read_word(0x4) & 0x08) != 0

        # The presence of text margins in the definition are defined in the word at 0x4.
        has_left_ruler_margin = (self.read_word(0x4) & 0x01) != 0
        has_right_ruler_margin = (self.read_word(0x4) & 0x02) != 0
        has_first_line_left_ruler_margin = (self.read_word(0x4) & 0x04) != 0


        # Word at offset 0x8

        # Ruler tab definitions follow if bits are set in 0x8.
        has_ruler_tabs = self.read_word(0x8) != 0

        # The number of tabs is given by the number of bits
        number_of_ruler_tabs = self.count_bits(self.read_word(0x8))
         

        i = 0x0c

        if self.document_version != 14: # 0x0e

            i = i + 8 # to 0x14

        # Flags word (0x14)

        # Whether the style is applied to paragraphs is defined in the flags word at 0x14.
        self.paragraph_apply = (self.read_word(i) & 0x20000000) != 0

        # New index and contents labels
        self.has_index_label = (self.read_word(i) & 0x80000000L) != 0
        self.has_contents_label = (self.read_word(i) & 0x40000000) != 0

        # The contents labels level
        self.contents_labels_level = (self.read_word(i) & 0x0f000000) >> 24

        # Rule-offs

        # Note that the word at 0x14 changes to 0x20000000 when rule-offs are used.
        #rule_offs = (self.read_word(0x14) & 0x20000000) != 0

        # Keyboard shortcut at 0x14 (the lowest byte and possibly all of the next highest byte).
        # The lowest byte records the letter used (only letters and function keys can be used,
        # it seems) using its position in the alphabet (a = 1, b = 2, ...).
        # For function keys, bit 8 (0x100) is set and the lowest four bits represent the
        # function key number.
        # Use of the Ctrl is always assumed. If the Shift key was used then bit 9 (0x200) is set.

        shortcut_value = self.read_word(i)

        shortcut = []

        if (shortcut_value & 0x0100) == 0x0100:

            # Function key shortcut
            if (shortcut_value & 0x20) == 0x20:

                shortcut.append('<Ctrl>')

            if (shortcut_value & 0x10) == 0x10:

                shortcut.append('<Shift>')

            shortcut.append('F' + str(shortcut_value & 0x0f))

        elif (shortcut_value & 0xffffff) != 0:

            # Normal alphabetical shortcut
            shortcut.append('<Ctrl>')
            
            if (shortcut_value & 0x0200) == 0x0200:
    
                shortcut.append('<Shift>')
    
            shortcut.append(chr(64 + (shortcut_value & 0xff)))

        elif self.base:

            # Default shortcut
            shortcut.append('<Ctrl>')
            shortcut.append('N')

        else:

            # No shortcut
            shortcut = []

        if shortcut != []:

            self.shortcut = string.join(shortcut, ' ')

        i = i + 4 # was 0x18


        # The style name is at offset 0x18 within the style data (null terminated, 32 characters
        # maximum).

        self.name = self.read_string(i, length = 32, ending = '\0', include = 0)

        # Since some of the following information is optional, introduce an offset to use to
        # move through the data.
        i = i + 0x20


        # Cheat: for the base style, there are flags which I don't understand so skip
        # the next word.
        if self.base: i = i + 1


        if has_auto_indent:

            # If auto indent is used then it can be either off (0) or on (1), as defined in the
            # lowest byte.
            self.auto_indent = (self.read_unsigned_byte(self.data[i]) & 0x01) != 0

            i = i + 1


        # Cheat: for the base style, there are flags which I don't understand so skip
        # the the rest of the bytes in this word.
        if self.base: i = i + 4 - (i % 4) 


        # Cheat: for the base style, there are flags which I don't understand so skip
        # the next byte.
        if self.base: i = i + 3


        # If line spacing is enabled, a byte appears here which I don't recognise.
        # [Needs further investigation.]
        if has_font_name_flag1: # and self.document_version != 20: # 0x14

            i = i + 1

        # The underline, script, decimal tab character and rule-off values are given in a
        # series of bytes if they are defined.
        if has_underline:

            # The word at 0x40 determines the level of underline to be displayed (by default)
            # with the text. This is either 0 (no underline), 1 or 2.
            self.underline = self.read_unsigned_byte(self.data[i])

            i = i + 1

        if has_script:

            # At 0x40 the flags determine whether the text should be presented as subscript or
            # superscript. Note that the text can be either or neither, but not both.
            self.subscript = (self.read_unsigned_byte(self.data[i]) & 0x03) == 0x01
            self.superscript = (self.read_unsigned_byte(self.data[i]) & 0x03) == 0x02

            i = i + 1

        if has_strikeout:

            # The strikeout effect is given in the word at 0x40 and can either be 0 (off) or 1 (on).
            self.strikeout = (self.read_unsigned_byte(self.data[i]) & 0x01) != 0

            i = i + 1

        if has_alignment:

            # Justification of the paragraph text is specified using a byte value from 0 to 3.
            justify = self.read_unsigned_byte(self.data[i])

            self.justification = self.justify_list[justify]

            i = i + 1

        if has_keep_single_para:

            # Keep together single paragraphs (lowest byte in 0x44).
            self.keep_together_single_para = (self.read_unsigned_byte(self.data[i]) & 0x01) == 1

            i = i + 1

        if has_keep_multiple_para:

            # Keep multiple paragraphs
            self.keep_together_multiple_para = (self.read_unsigned_byte(self.data[i]) & 0x01) == 1

            i = i + 1

        if has_hyphenation:

            # Hyphenation is specified at 0x44. It has a value of 0 (off) or 1 (on).
            self.hyphenation = (self.read_unsigned_byte(self.data[i]) & 0x01) != 0

            i = i + 1


        # Cheat: for the base style, there are flags which I don't understand so skip
        # the next byte.
        if self.base: i = i + 1


        # The decimal tab may share a word with the script (subscript/superscript) flag.
        # More investigation is required.
        
        if has_decimal_tab:

            # The decimal tab character is given at 0x48.
            self.decimal_tab = self.data[i]

            i = i + 1

        if has_keep_with_next_para:

            # Keep with next paragraph
            self.keep_with_next_para = (self.read_unsigned_byte(self.data[i]) & 0x01) == 1

            i = i + 1


        # Foreground colour flag is set
        if has_foreground_colour:

            i = i + 1

        # Unknown flag 0x080000
        if has_unknown_flag_080000:

            i = i + 1

        # Cheat: for the base style, there are flags which I don't understand so skip
        # the next four bytes.
        if self.base: i = i + 4


        if has_page_grid_lock:

            # Style is locked to page grid (lowest byte of 0x50).
            self.page_grid_lock = (self.read_unsigned_byte(self.data[i]) & 0x01) == 1

            i = i + 1

        # Whether rule-offs above and below are present is specified in the flags at 0x50.
        if has_rule_offs:

            self.rule_off_above = (self.read_unsigned_byte(self.data[i]) & 0x02) != 0
            self.rule_off_below = (self.read_unsigned_byte(self.data[i]) & 0x04) != 0

            i = i + 1

        # The next flag bytes determine whether this style is a bold or italic derivative of
        # another style. The first byte corresponds to italic, the next byte corresponds
        # to bold. Values of 0x01 (on) indicate that a bold or italic effect is always appled;
        # values of 0x00 (off) indicate the normal effect behaviour.
        if has_italic:

            self.italic_on = (self.read_unsigned_byte(self.data[i]) & 0xff) == 1

            i = i + 1

        if has_bold:

            self.bold_on = (self.read_unsigned_byte(self.data[i]) & 0xff) == 1

            i = i + 1

        excess = i % 4

        if excess != 0: i = i + (4 - excess)


        # Cheat: for the base style, there are flags which I don't understand so skip
        # the next word.
        if self.base: i = i + 4


        # Text margins

        if has_left_ruler_margin:

            # Left margin
            self.left_margin = self.read_word(i)

            i = i + 4

        if has_right_ruler_margin:

            # Right margin (negative displacement from the right hand side of the current frame)
            self.right_margin = self.read_word(i)

            i = i + 4

        if has_first_line_left_ruler_margin:

            # First line left margin
            self.first_line_left_margin = self.read_word(i)

            i = i + 4


        # Script text handling

        # Presumably, if values are given which are not sensible, reasonable default values
        # are used. Therefore, the default values of 0% for script text size and offsets
        # are presumably never used.

        if has_script_offset or self.base:

            # The "offset" for subscript text is given in the lower half of the word at 0x64 and is
            # expressed in the same form as the script text size.
            self.subscript_offset = ((self.read_word(i) & 0xffff) * 100.0) / 0x8000

            # The "offset" for superscript text is given in the upper half of the word at 0x64 and
            # is expressed in the same form as the script text size.
            self.superscript_offset = (((self.read_word(i) >> 16) & 0xffff) * 100.0) / 0x8000

            i = i + 4   # 0x68

        if has_script_size or self.base:

            # The size of script text as a percentage of the text size given above is at 0x68.
            # This appears to be expressed in a form similar to that used for the aspect ratio
            # of text, but with 100% being expressed as 0x8000.
            self.script_aspect = (self.read_word(i) * 100.0) / 0x8000

            i = i + 4   # 0x6c


        if has_line_spacing:

            # Line spacing, if defined, is expressed at 0x6c. Whether it is expressed as a percentage
            # of text size is determine by the top bit.
            if (self.read_word(i) & 0x80000000L) != 0:

                # A percentage
                self.line_spacing = ((self.read_word(i) & 0x7fffffff) * 100.0) / 0x10000
                self.line_spacing_units = '%'

            else:

                # A length in millipoints
                self.line_spacing = self.read_word(i)
                self.line_spacing_units = 'mpt'

            i = i + 4   # 0x70

        # Paragraphs and text layout

        if has_space_below_para:

            # Space above and below paragraphs in millipoints are given at 0x74 and 0x70 respectively.
            self.below_para = self.read_word(i)

            i = i + 4   # 0x74

        if has_space_above_para:

            self.above_para = self.read_word(i)

            i = i + 4   # 0x78

        # Underline text handling

        if has_underline_offset or self.base:

            # The offset and size of underlined text, expressed as a percentage, presumably of
            # the text size, are at 0x78 and 0x7c respectively. These are given in the form
            # used to express the sizes and offsets of script text.
            self.underline_offset = (self.read_word(i) * 100.0) / 0x8000

            i = i + 4   # 0x7c

            self.underline_size = (self.read_word(i) * 100.0) / 0x8000

            i = i + 4   # 0x80

        if has_rule_offs or self.base:

            # Rule-off thickness is at 0x80.
            self.rule_off_thickness = self.read_word(i)

            i = i + 4


        # Rule-off margins

        if has_left_rule_off_margin or self.base:

            # Left rule-off margin
            self.left_rule_off_margin = self.read_word(i)

            i = i + 4

        if has_right_rule_off_margin or self.base:

            # Right rule-off margin
            self.right_rule_off_margin = self.read_word(i)

            i = i + 4


        if has_font_size:

            # Read the font size and convert it from sixteenths of a point to millipoints like all other measurements.
            self.font_size = self.read_word(i) * 1000.0 / 16.0
            i = i + 4   # 0x90

        if has_aspect_ratio:

            # The top two bytes in the word are the number of hundreds, the bottom two
            # are the units expressed as a fraction of hundreds.
            self.aspect_ratio = (self.read_word(i) * 100.0) / 0x10000
            i = i + 4   # 0x94

        if has_rule_offs:

            # The offset of the rule-off below the text is given in the word at 0x94.
            self.rule_off_below_offset = self.read_word(i)

            i = i + 4   # 0x98

        if has_keep_together:

            # Keep paragraphs together

            # Keep together length (0x98).
            self.keep_together_length = self.read_word(i)

            i = i + 4

        if has_leadering:

            # The leadering string is given at 0x9c.
            self.leadering = self.read_string(i, length = 4, ending = '\0', include = 0)

            i = i + 4   # 0xa0

        if has_vertical_rule_off_width:

            # Vertical rule-off width is at 0xa0.
            self.vertical_rule_off_width = self.read_word(i)

            i = i + 4   # 0xa4

        if has_rule_offs:

            # The offset of the rule-off above the text is given in the word at 0xa4.
            self.rule_off_above_offset = self.read_word(i)
            
            i = i + 4


        # Cheat: for the base style, there are flags which I don't understand so skip
        # the next two words.
        if self.base: i = i + 8


        if has_tracking:

            # Tracking (specified in thousandths of an em) is given at 0xb0.
            self.tracking = (self.read_word(i) * 1000.0) / 0x10000

            i = i + 4


        # Cheat: for the base style, there are flags which I don't understand so skip
        # the next six words.
        if self.base: i = i + 0x18

        # Some extra colour related flag words are found here.
        if has_foreground_flag2:

            i = i + 4

        if has_background_flag2:

            i = i + 4

        # A number appears here whose purpose I don't understand.
        # It may be the font handle.
        # [Needs further investigation.]
        #if has_font_name and has_font_name_flag2:
        if has_font_name_flag2:

            self.font_handle = self.read_word(i)

            i = i + 4

        # Tab information should be here. Each word contains the tab position and its type;
        # the position is in the top three bytes with the type in the lowest byte.

        self.tab_defns = []

        for j in range(0, number_of_ruler_tabs):

            word = self.read_word(i)
            self.tab_defns.append( ((word & 0xffffff00L) >> 8, self.tab_type_list[word & 0xff]) )

            i = i + 4


        # Cheat: for the base style, there are flags which I don't understand and the
        # number of tabs tends to fill 0x80 bytes so discard the following words until
        # the filled area has been reached.
        if self.base: i = i + 0x80 - (number_of_ruler_tabs * 4)


        if has_font_name:

            # The font name (null terminated, possibly limited to 0x28 characters).
            self.font_name = self.read_string(i, ending = '\0', include = 0)

            l = len(self.font_name) + 1

            if l % 4 != 0: l = l + 4 - (l % 4)

            i = i + 0x28

        if has_foreground_colour or self.base:

            # The text foreground colour
            self.foreground_colour = self.word2rgb(self.read_word(i))

            i = i + 4

        if has_background_colour:
        
            # The text background colour
            self.background_colour = self.word2rgb(self.read_word(i))

            i = i + 4

        elif self.base:

            # The base style appears to define this anyway.
            i = i + 4

        if has_underline_strikeout_colour:

            # The underline/strikeout colour
            self.strikeout_colour = self.word2rgb(self.read_word(i))
            
            # [Shouldn't this move the pointer onwards.]


        # Cheat: for the base style, there are flags which I don't understand so skip
        # the next word.
        if self.base: i = i + 4


        if has_rule_off_colour:

            # Rule-off colour
            # The word at 0x4c changes to 0x000c0000 when this is set.
            self.rule_off_colour = self.word2rgb(self.read_word(i))



class Border(Common):

    """Border(Common)
    
    border = Border(data, addr)
    
    A container for border data, which usually takes the form of a Drawfile.
    
    The data parameter is a string containing the binary page data from the
    Impression document. The addr parameter is a tuple; the first element of
    which is the address of the data within the original document, the second
    is the name of the file from which the data originated.
    """

    def __init__(self, data, addr):
    
        # Offset into the file.
        self.addr = addr

        # Actual data.
        self.data = data

        # The Drawfile is not decoded here; a suitable library should be used. However, it is
        # worth noting that it should be made up of two objects: the vertical left hand border
        # and the top left border pieces. The width of the bounding box containing the left hand
        # edge piece determines how far away the corner piece is displayed from the top left corner.

    def __repr__(self):
    
        return '<Border (Drawfile): %x in %s>' % self.addr



class UnknownPiece(Common):

    """UnknownPiece(Common)
    
    piece = UnknownPiece(start, end, data, addr)
    
    A placeholder for unknown blocks of data found in a content area of a
    document. Ideally, this class should never need to be used, but is
    employed as a means of storing pieces of content which are not
    understood by the author.
    
    The start and end parameters are integers representing the words found at
    the beginning and end of the block of data.
    
    The data parameter is a string containing the binary page data from the
    Impression document. The addr parameter is a tuple; the first element of
    which is the address of the data within the original document, the second
    is the name of the file from which the data originated.
    """

    def __init__(self, start, end, data, addr):

        self.start = start
        self.end = end
        self.data = data

        # Store the filename and offset within the file.
        self.addr = addr

    def __repr__(self):

        return '<UnknownPiece at %x in "%s": %x - %s - %x>' % \
            (self.addr[0], self.addr[1], self.start, map(hex, self.data), self.end)



class Content(Common):

    """Content(Common)
    
    content = Content(data, start, addr, strict = 1)
    
    A class to represent a content area found in an Impression document. This
    class is usually instantiated by a ContentCollection object.
    
    The data parameter is a string containing the binary page data from the
    Impression document.
    
    The addr parameter is a tuple; the first element of which is the address
    of the data within the original document, the second is the name of the
    file from which the data originated.
    
    The strict flag is used to determine whether blocks in the linked list
    used in Impression document content areas should be interpreted strictly
    by their declared lengths or whether, in the case of problems, the end of
    a bad block should be sought. This approach was necessary when attempting
    to read a particular Impression Junior document.
    
    An instance of this class will attempt to interpret the data provided
    and, in the case of textual content, create a list containing pieces of
    content. This is stored in the "pieces" attribute of the object. If the
    data represents a Drawfile then the "content_type" object attribute will
    be set to "Draw"; if the data is textual then "content_type" will be set
    to "Text"; for Artworks illustrations, it will be set to "Artworks".
    """

    def __init__(self, data, start, addr, strict = 1):
    
        # Actual data.
        self.data = data

        # Start interpreting at an offset within the data.
        self.start = start

        # Address information
        self.addr = addr

        # The strictness determines whether the content should be check for self consistency.
        self.strict = strict

        # Content type
        self.content_type = ''
        
        # Pieces of text.
        self.pieces = []
        
        # Try to interpret the data.
        self.interpret()

        # Delete the reference to the data.
        if self.content_type == 'Text':
        
            del self.data
    
    def __repr__(self):
    
        # Store the filename and offset within the file, if given.
        return '<Content (%s) at %x within file "%s">' % \
            (self.content_type, self.addr[0], self.addr[1])

    def info(self):
    
        if self.content_type == 'Text':
        
            for p in self.pieces:
            
                if isinstance(p, Text):
                    
                    output = ""
                    for item in p.text:
                        
                        if type(item) == types.StringType:
                            
                            if item[-1] == "\x0d":
                                print string.expandtabs(output+item[:-1], 4)
                                output = ""
                            else:
                                output = output + item

                        elif item[0] == 0x12:
                            
                            output = output + "\t"
                        
                    if output != "":
                        print string.expandtabs(output, 4)
    
    def after_text(self, i):

        # Continue reading bytes until the end of the word is found
        # or a zero byte is found.
        
        j = i % 4
        
        while j < 4:
        
            if ord(self.data[i]) == 0:
        
                # Move to next word and leave this loop.    
                i = i + 4 - j
                break
            
            i = i + 1
            j = j + 1
        
        return i

    def seek_block(self, i):

        while i < len(self.data):

            word = self.read_word(i)
            #print '>', hex(i), hex(word)

            if (word & 0xff) != 0 and (((word >> 8) & 0xffffff) % 4) == 0:

                break

            i = i + 4

        return i

    def interpret(self):
    
        # Check whether the content represents a Drawfile.
        if self.data[:4] == 'Draw':
        
            self.content_type = 'Draw'
            return

        elif self.data[:4] == 'Top!':

            self.content_type = 'Artworks'
            return

        else:

            self.content_type = 'Text'

        # Current style.
        style = None
        lineinfo = None
        
        words = []

        # Skip the first word.
        #i = 0x04

        # Look for blocks of content in the linked list.
        i = self.seek_block(self.start)

        # Keep a record of whether a frame header, containing text style usage
        # declarations, has been found.
        frame_header = 0

        while i < len(self.data):
        
            # Read an identifier word.
            start = self.read_word(i)
            #print hex(i), hex(start)

            # Record the offset into the data.
            start_offset = i
            
            ## Check that the first byte is 0x25.
            #if start & 0xff != 0x25:
            #
            #    raise ImpressionError, "The identifier %x at %x " + \
            #          "did not begin with 0x25" % (start, self.offset + i)
            
            # The type of block is the first byte in the word.
            block_type = start & 0xff
            
            # The word which marks the end of the definition has the same
            # second byte.
            next = start & 0xffffff00L
            
            # The length is the everything but the first byte in the word.
            length = (start >> 8) & 0xffffff
            
            i = i + 4
            
            # Read the correct number of words.
            words = []
            
            # Decrease the length by eight as we have already read the
            # first word in the block, and we will check the last word
            # at the end.
            length = length - 8

            while i < len(self.data) and length > 0:
            
                word = self.read_word(i)
                
                i = i + 4
                length = length - 4
            
                # Collect all words between the start and end identifiers.
                if word == 0x12345679:
                    end = word
                    break
                
                words.append(word)
            else:
                # We must find 0x??00.
                end = self.read_word(i)
            
            if end == 0x12345679:
            
                break
            
            elif end != next:

                if self.strict == 1:

                    # Be strict about the problem found.
                    raise ImpressionError, \
                          'The identifier %x at %x did not match %x at %x in "%s"' % (
                            end, self.addr[0] + i, start, self.addr[0] + start_offset,
                            self.addr[1]
                            )

                else:

                    # Look for the next block of content.
                    i = self.seek_block(i + 4)

            else:

                # Examine the next word (and the next block).
                i = i + 4

            if (block_type & 0xff) == 0x01:

                # End of content for this frame.
                break

            if (block_type & 0xff) == 0x02:

                if frame_header == 0:

                    # A frame header has been encountered and no others were
                    # previously found.
                    text_setup = TextSetup(
                        self.data[(start_offset + 4):(i - 4)],
                        (self.addr[0] + start_offset, self.addr[1])
                        )
                
                    self.pieces.append( text_setup )

                    frame_header = 1

                else:

                    # A frame header has been encountered for a second time.
                    # Content for another frame follows.
                    break

            elif (block_type & 0x05) == 0x05:
            
                # Justification?
                justify = (block_type & 0xf0) >> 4
                
                # 0x25 left justification?
                # 0x05 full justification?
                # 0xa5

                # Bit 0x20: trailing line
                trailing = (block_type & 0x20) != 0
                #first_line = (block_type & 0x80) != 0 or (block_type & 0x08) != 0
                #first_line = (block_type & 0x08) != 0
                first_line = (block_type & 0x80) != 0 # 0x08 ?

                # Line information: four words followed by text.
                
                lineinfo = LineInfo( words[:4] )
                
                #print hex(self.offset + i), len(self.data[(start_offset + 20):(i - 4)]), repr(self.data[(start_offset + 20):(i - 4)])
                
                text = Text(
                    self.data[(start_offset + 20):(i - 4)],
                    block_type, lineinfo,
                    (self.addr[0] + start_offset, self.addr[1]),
                    first_line, trailing
                    )
                #print text
                
                self.pieces.append( text )
            
            elif 0x12345679 in words:
            
                break
            
            else:

                piece = UnknownPiece(start, end, words, (self.addr[0] + start_offset, self.addr[1]))

                self.pieces.append(piece)


class StyleCollection:

    """StyleCollection
    
    style_collection = StyleCollection(filename, offset_list, old_format,
                                       document_version)
    
    A class for managing text styles based on the tables of styles in the
    original document.
    
    The filename is a string containing the name of the file in which the
    style information resides. For old format documents, this is the name of
    the directory which contains the document information.
    
    The offset_list is a list of addresses within the file mentioned, created
    by methods of the main Document class.
    
    The old_format flag, when set, specifies that the document is split into
    a number of files contained in an application directory.
    
    The document_version is found from the original document and will be used
    to determine how the style data is interpreted.
    """

    def __init__(self, filename, offset_list, old_format, document_version):

        # Record the file corresponding to the document in which the styles reside.
        #
        # For old style documents, this will be a !DocData file.
        self.filename = filename
        
        # Record the document version.
        self.document_version = document_version

        # Record the list of offsets referring to the styles in the document.
        self.style_offsets = offset_list

        # Read styles on demand rather than all at once. This dictionary is built up from the
        # style objects found from dereferencing items in the list of styles offsets.
        
        # Define a dictionary to contain instances of these styles.
        self.styles = {}

    def keys(self):

        k = []

        # Scan the list of style offsets looking for valid styles, ignoring the
        # last offset which refers to the next area in the document.
        for index in range(0, len(self.style_offsets) - 1):

            if self.style_offsets[index] is not None:

                k.append(index)

        return k

    def __len__(self):

        # Do not include the end of style table in the number of styles available.
        return len(self.style_offsets) - 1

    def __getitem__(self, n):

        # Find the content by deferencing the offsets in the style offset list for any except the
        # last offset.
        if n >= len(self.style_offsets) - 1:

            raise IndexError, 'list index out of range'


        # If the style has not already been read and interpreted then find it in the document.
        if not self.styles.has_key(n):

            #if self.debug and n < (len(self.style_offsets) - 1): print n + 1,

            # Do not attempt to dereference empty entries in the offset list.
            offset = self.style_offsets[n]

            if offset is None:

                raise ImpressionError, \
                    'No style with reference number %i in %s.' % (n, self.filename) 

            # Open the document.
            try:
                f = open(self.filename, 'rb')

                # Find the style data.
                f.seek(self.style_offsets[n])

            except IOError:

                raise ImpressionError, \
                    'Could not read style in %s - the file may have been ' % self.filename + \
                    'moved or deleted.'

            # Find the next populated list entry. The last entry (end of style area)
            # will be found if no others are.
            m = n + 1

            while m < (len(self.style_offsets) - 1):

                if self.style_offsets[m] is not None: break
                m = m + 1

            amount = self.style_offsets[m] - offset

            if amount > 0:

                # The base style ("Normal") is treated differently to other styles because
                # there are flags used I can't investigate just by using Impression.
                base = (n == 0)

                # Create an instance of the abstract Style class for each style.
                this_style = Style(
                    f.read(amount), (self.style_offsets[n], self.filename),
                    self.document_version, base
                    )

                # Create a class from that instance for use with other style classes so that
                # inheritance can mimic the behaviour of overlaying document styles.
                self.styles[n] = this_style.create_class()

            # Close the file.
            f.close()

        # Return the style class.
        return self.styles[n]
    

class PageCollection(Common):

    """PageCollection(Common)
    
    page_collection = PageCollection(filename, page_offsets, PageClass,
                                     old_format, document_version)
    
    A container for page definitions from the descriptions found in an
    Impression document. Actual page objects are supplied on demand rather
    than being created all at once.
    
    The filename is a string containing the name of the file in which the
    style information resides. For old format documents, this is the name of
    the directory which contains the document information.
    
    A list of page offsets is provided from the information found in the
    original document and specifies the addresses within the file where the
    page definitions reside.
    
    The PageClass determines which class is used to instantiate page objects
    within this collection and should be either ChapterPage or MasterPage.
    
    The old_format flag, when set, specifies that the document is split into
    a number of files contained in an application directory.
    
    The document_version is found from the original document and will be
    used to determine how the style data is interpreted.
    """

    def __init__(self, filename, page_offsets, PageClass, old_format, document_version):

        # Record the file corresponding to the document in which the pages reside.
        #
        # For old documents, this will refer to a !DocData file.
        self.filename = filename
        
        # Record the document version.
        self.document_version = document_version

        # The offsets to the page definitions in the file.
        self.page_offsets = page_offsets

        # The class of page to be instantiated for pages in this area.
        self.PageClass = PageClass

        # Read pages on demand rather than all at once. This dictionary is built up from the
        # page objects found from dereferencing items in the list of page offsets.

        # Define a dictionary to contain instances of these pages.
        self.pages = {}

    def read_word(self, addr = None):

        if addr is None:

            return Common.read_signed_word(self, self.file.read(4))

        else:

            old_addr = self.file.tell()
            self.file.seek(addr, 0)
            word = Common.read_signed_word(self, self.file.read(4))
            self.file.seek(old_addr, 0)

            return word

    def keys(self):

        k = []

        # Do not include the end of the page collection table in the list of
        # available keys.
        for index in range(0, len(self.page_offsets)-1):

            k.append(index)

        return k

    def __len__(self):

        # Do not include the end of the page collection table in the number of pages
        # available.
        return len(self.page_offsets) - 1

    def __getitem__(self, n):

        # If the page has not already been read and interpreted then find it in the document.
        if self.pages.has_key(n):

            # Return the page instance.
            return self.pages[n]

        # Find the content by deferencing the offsets in the style offset list for any except the
        # last offset.
        if n >= len(self.page_offsets) - 1:

            raise IndexError, 'list index out of range'


        # Open the document.
        try:
            self.file = open(self.filename, 'rb')

            # Find the style data.
            self.file.seek(self.page_offsets[n])

        except IOError:

            raise ImpressionError, \
                'Could not read page in %s - the file may have been ' % self.filename + \
                'moved or deleted.'

        # Set the type of data to follow. [This doesn't mean much here, but when page margins and frames
        # are being defined, it will affect their properties.]
        data_type = self.read_word() & 0xff

        # Current definition
        current = None
        current_frames = {}

        # Offset to next definition.
        if n == len(self.page_offsets) - 1:

            next_defn = self.page_offsets[-1]

        else:

            next_defn = self.page_offsets[n + 1]

        # Read definitions until we reach the next section of the file structure
        while self.file.tell() < next_defn:

            # Read an identifier.
            start_offset = self.file.tell()

            start_block = self.read_word()

            block_length = ((start_block & 0xffffff00L) >> 8)
            next_offset = start_offset + block_length

            # Look for a word with this second byte.
            if data_type == 1:

                # Page definition.

                # Read all the words between the start and end words.
                data = self.file.read(block_length - 8)

                # The word which marks the end of the definition has the same
                # second byte.
                # The end of the block must match the beginning.
                end_offset = self.file.tell()
                end_block = self.read_word()

                if end_block & 0xffffff00L != start_block:

                    raise ImpressionError, \
                          "The identifier %x at %x did not match %x at %x" % \
                          (end_block, end_offset, start_block, start_offset)

                # Create a new current page of the appropriate class using the data, depending
                # on whether the page is a master or chapter page.
                current = self.PageClass(data, addr = (start_offset, self.filename))

            elif data_type & 0x07 == 2 and current is not None:

                # Frame definitions.

                # Read all the words between the start and end words.
                data = self.file.read(block_length - 8)

                # The word which marks the end of the definition has the same
                # second byte.
                # The end of the block must match the beginning.
                end_offset = self.file.tell()
                end_block = self.read_word()

                if end_block & 0xffffff00L != start_block:

                    raise ImpressionError, \
                          "The identifier %x at %x did not match %x at %x" % \
                          (end_block, end_offset, start_block, start_offset)

                # Add this frame to the dictionary of frames associated with the current
                # master page.
                frame = Frame(data, data_type, current, self.document_version)
                current_frames[frame.frame_number] = frame

            elif data_type & 0x07 == 3 and current is not None:

                # Frame definitions for header/footer.

                # Read all the words between the start and end words.
                data = self.file.read(block_length - 8)

                # The word which marks the end of the definition has the same
                # second byte.
                # The end of the block must match the beginning.
                end_offset = self.file.tell()
                end_block = self.read_word()

                if end_block & 0xffffff00L != start_block:

                    raise ImpressionError, \
                          "The identifier %x at %x did not match %x at %x" % \
                          (end_block, end_offset, start_block, start_offset)

                # Add this frame to the dictionary of frames associated with the current
                # master page.
                frame = Frame(data, data_type, current, self.document_version)
                current_frames[frame.frame_number] = frame

            elif data_type & 0x07 == 4 and current is not None:

                # Frame definitions.

                # Read all the words between the start and end words.
                data = self.file.read(block_length - 8)

                # The word which marks the end of the definition has the same
                # second byte.
                # The end of the block must match the beginning.
                end_offset = self.file.tell()
                end_block = self.read_word()

                if end_block & 0xffffff00L != start_block:

                    raise ImpressionError, \
                          "The identifier %x at %x did not match %x at %x" % \
                          (end_block, end_offset, start_block, start_offset)

                # Add this frame to the dictionary of frames associated with the current
                # master page.
                frame = Frame(data, data_type, current, self.document_version)
                current_frames[frame.frame_number] = frame

            elif block_length > 0:

                # Unknown type of data

                # Read all the words between the start and end words.
                data = self.file.read(block_length - 8)

                # The word which marks the end of the definition has the same
                # second byte.
                # The end of the block must match the beginning.
                end_offset = self.file.tell()
                end_block = self.read_word()

                if end_block & 0xffffff00L != start_block:

                    raise ImpressionError, \
                          "The identifier %x at %x did not match %x at %x" % \
                          (end_block, end_offset, start_block, start_offset)

            else:

                break

            # The type of following frame is given by the lowest word in the end of block marker.
            data_type = end_block & 0xff

        # After reading the definition, ensure that the frames on this page are stored
        # inside it.
        if current is not None:

            # Sort any remaining frames into the correct order and add them to the page.
            keys = current_frames.keys()
            keys.sort()

            for k in keys:

                current.frames.append(current_frames[k])

            # Ensure that the last page definition is added to the list.
            self.pages[n] = current

            if self.PageClass == ChapterPage:

                # Index chapter pages by chapter and page number.
                self.pages[ (current.chapter, current.page) ] = current

            elif hasattr(current, 'name'):

                # Index master pages by name, where possible.
                self.pages[ current.name ] = current

        # Return the page instance, or None if there was no page.
        return current



class ContentCollection(Common):

    """ContentCollection(Common)
    
    content_collection = ContentCollection(
        filename, offset_list, start_addr = None, content_index = None,
        strict = 1
        )
    
    A container for document content in an Impression document.
    Actual content areas are supplied on demand rather than being created all
    at once.
    
    The filename is a string containing the name of the file in which the
    style information resides. For old format documents, this is the name of
    the directory which contains the document information.
    
    A list of offsets to content is provided from the information found in
    the original document and specifies where in the original document the
    content areas reside. For old format documents a virtual addressing
    scheme is used to reference content which will be found in many files
    inside the document directory.
    
    The strict flag determines whether each content area should be
    interpreted with strict checks on its integrity. This flag is passed to
    each Content object on its creation for this purpose.
    """

    def __init__(self, filename, offset_list, content_index = None, strict = 1):

        # Record the file corresponding to the document in which the content resides.
        self.filename = filename

        # Record the list of offsets referring to the content in the document.
        self.content_offsets = offset_list

        # If the document being read is an old format directory-based one then a content index
        # will provide an dictionary of virtual address ranges which correspond to content
        # stored in separate files.
        self.content_index = content_index

        # Use the strictness setting to determine whether the content should be checked for
        # self consistency.
        self.strict = strict

        # Read content on demand rather than all at once. This dictionary is built up from the
        # Content objects found from dereferencing items in the list of content offsets.
        
        # Define a dictionary to contain instances of this content.
        self.content = {}

        # Keep a record of which offsets have already been dereferenced and their associated
        # content so that duplicate entries in the content table are not dereferenced more than
        # once.
        self.content_found = {}

    def read_word(self, addr = None):

        if addr is None:

            return Common.read_signed_word(self, self.file.read(4))

        else:

            old_addr = self.file.tell()
            self.file.seek(addr, 0)
            word = Common.read_signed_word(self, self.file.read(4))
            self.file.seek(old_addr, 0)

            return word

    def keys(self):

        k = []

        for index in range(0, len(self.content_offsets)):

            k.append( (index, 0) )

        return k

    def __len__(self):

        # Do not include the end of the content table in the number of content areas available.
        return len(self.content_offsets) - 1

    def __getitem__(self, pair):

        if type(pair) == types.IntType:

            item = pair
            offset = 0

        else:

            item, offset = pair 

        # Check whether the content has already been found.
        if self.content.has_key( (item, offset) ):

            # Return any content already present.
            return self.content[ (item, offset) ]

        # Find the content by deferencing the offsets in the content offset list for any except the
        # last offset.
        if item >= len(self.content_offsets) - 1:

            raise IndexError, 'list index out of range'

        # Find the content data if not already present. Note that different items in the content
        # table can point to the same content.
        address = self.content_offsets[item]

        if address not in self.content_found.keys():

            # Open the relevant file.
            if self.content_index is None:

                # For new style documents, just reopen the document file.
                try:
                    f = open(self.filename, 'rb')

                    # Go to the content data.
                    f.seek(address)

                except IOError:

                    raise ImpressionError, \
                        'Could not read content in %s - the file ' % self.filename + \
                        'may have been moved or deleted.'

            else:

                # For old style documents, find the relevant file by comparing the content offset
                # with the keys in the content index.

                filename = ''

                for start, end in self.content_index.keys():

                    if address >= start and address < end:

                        # The virtual address is within this range.

                        # Read the relevant filename, and offsets within the file.
                        filename, start_within, end_within = self.content_index[(start, end)]
                        break

                if filename == '':

                    raise ImpressionError, 'No content at address 0x%x.' % address

                # Open the relevant file.

                try:
                    f = open(filename, 'rb')

                    # Go to the content data. The offset into the data is (address - start)
                    # but the data begins in the file at start_within.

                    address = start # always start at the beginning of the data

                    f.seek(start_within + address - start)

                except IOError:

                    raise ImpressionError, \
                        'Could not read content in %s - the file ' % filename + \
                        'may have been moved or deleted.'

            # Find the amount of data in this piece of content by finding the the beginning of the
            # next piece of content to follow this one. Note that the references immediately following
            # this one in the content table may refer to the same item of content.
            for j in range(item + 1, len(self.content_offsets)):

                amount = self.content_offsets[j] - address

                if amount != 0: break

            # Read the data and interpret it.
            if amount == 0:

                raise ImpressionError, \
                      'The content referred to at %x has unknown length.' % address

            data = f.read(amount)

            # Close the file.
            f.close()

            # Remember that this data has already been read.

            if self.content_index is None:

                # For new format files, just record the data.
                self.content_found[address] = data

            else:

                # For old format files, record the data, the virtual start address of the file
                # it came from, its filename, and the offset within that file of the data.
                self.content_found[address] = data, start, filename, start_within

        else:

            # Read the data already found.
            if self.content_index is None:

                # Retrieve just the data for new format files.
                data = self.content_found[address]

            else:

                # Retrieve the data, its virtual start address, the filename of the originating
                # file, and the start address of the data within the file.
                data, start, filename, start_within = self.content_found[address]

        # Read the structure using the data.
        if self.content_index is None:

            # Interpret the content, giving the appropriate information about the new style
            # document file.
            structure = Content( data, offset, (address, self.filename), strict = self.strict )

        else:

            # Interpret the content, giving the appropriate information about the old style
            # document file.
            structure = Content( data, offset, (start_within, filename), strict = self.strict )

        # Add the structure to the content dictionary.
        self.content[ (item, offset) ] = structure

        # Return the structure found. 
        return structure



class Document(Common):

    """Document(Common)
    
    document = Document(filename, borders = 1, strict = 1, verbose = 0)
    
    Reads the document stored in the file or directory specified by the
    filename string, converting the structure and content into appropriate
    objects where possible.
    
    The borders flag determines whether any frame borders found in the
    document should be read.
    
    The integrity of content areas in the document can be checked by setting
    the strict flag, which will be passed to ContentCollection and Content
    objects when necessary.
    
    The process of reading and interpreting the document structure and
    content can be monitored by setting the verbose flag. Progress will be
    printed as it is made.
    
    If the document is read without problems and the conversion process is
    successful, a number of attributes will be created which can be used to
    extract document information for reuse.
    
      chapter_pages     A collection of ChapterPage objects corresponding to
                        each page used in the document.
      
      master_pages      A collection of MasterPage objects corresponding to
                        each master page declared in the document.
    """
    
    def __init__(self, filename, borders = 1, strict = 1, verbose = 0):
    
        # Check whether the file exists.
        if os.path.exists(filename) == 0:

            raise ImpressionError, 'No such file: %s' % filename

        # Open the file or directory.

        if os.path.isdir(filename):

            # Open the !DocData file within the document directory.
            
            file = self.require_resource(filename, "!DocData", 0, 1)
            
            try:
                self.file = open(file, 'rb')
            except IOError:

                raise ImpressionError, \
                    "Couldn't open the document (!DocData file was missing): %s" % file

            self.old_format = 1
            self.docdata = file

        else:

            # New format document

            try:
                self.file = open(filename, 'rb')
            except IOError:

                raise ImpressionError, "Couldn't open the file %s." % filename

            self.old_format = 0
            self.docdata = filename


        # Record the filename.
        self.filename = filename

        # Determine the document version and attributes.
        self.find_document_details()

        # Record the verbose flag.
        self.verbose = verbose

        # Record the strictness requirements.
        self.strict = strict

        if verbose:

            import time

        # Read the main table of offsets (first unique entry is assumed to be zero).
        if verbose:
            print 'Reading offsets...',

        self.read_offsets()
        
        if verbose:
            print 'Done'

        # Read the styles table (second unique entry).
        self.read_styles()
        
        #show_table('Styles', self.styles)
        
        # Read the borders (third unique entry).
        if borders and self.offsets.has_key('borders'):

            self.read_borders()

        # Read the frame content table (fifth unique entry or fourth from end).
        if verbose: tick = time.time()

        # If the document is an old format document then scan the content files and create
        # an index mapping virtual addresses to addresses within the content files.
        if self.old_format:

            # Read the mapping table for old format documents (fourth unique entry).
            self.read_mapping_area()

            # Create content index.
            self.create_content_index() 

        else:

            # If the document format version is not 1.4 or the document is in a single
            # file then read the content table.
            self.read_content_table()

        """
        #if self.version == 14 and self.old_format == 1: # 0x0e
        if self.old_format == 1:

            # For old format documents regenerate the content table from the mapping area.
            #if self.version == 28: # 0x1c
            #
            #    padding = 0
            #
            #else:
            #
            #    padding = 4
            #
            #self.create_content_table(padding)
            self.create_content_table()
        """

        if verbose: print 'Document content scanned in %.1f seconds.' % (time.time() - tick)

        # Read the page definitions (sixth unique entry or third from end).
        if verbose:

            print 'Scanning and reading master pages...'
            tick = time.time()

        self.read_master_pages_table()
        self.read_master_pages()
        
        if verbose:
            print 'Done in %.1f seconds.' % (time.time() - tick)

        # Read the chapter pages (seventh unique entry or second from end)
        if verbose:

            print 'Scanning chapter pages...'
            tick = time.time()

        self.read_chapter_pages_table()
        
        if verbose:
            print 'Done in %.1f seconds.' % (time.time() - tick)

        # Note that the last unique entry in the table of offsets points to the
        # document content. This is referenced through the structure table.
        
        # Close the file.
        self.file.close()

    def require_resource(self, path, name, directory, variations = 0):

        """full_path = require_resource(self, path, name, directory, variations = 0)
        
        Return the full path of a file or directory in the path given with a name
        which matches the specified value of "name", regardless of the use of upper
        or lower case text.
        
        The "directory" parameter, when non-zero indicates that a directory is
        required.
        
        The variations flag, when set, allows files and directories to be returned
        if their names begin with the name given.
        
        If no matching resource is found then an ImpressionError exception is
        raised.
        """
        # List the files in that path.
        files = os.listdir(path)

        length = len(name)

        for file in files:

            if variations == 0 and string.lower(file) == string.lower(name):
                break
            
            elif variations == 1 and string.lower(file)[:length] == string.lower(name):
                break
        else:
            raise ImpressionError, 'Required resource not found: %s in %s' % (name, path)

        full_path = os.path.join(path, file)

        if os.path.exists(full_path) == 0:

            if directory == 0:
                raise ImpressionError, 'Required file not found: %s' % full_path

            elif directory == 1:
                raise ImpressionError, 'Required directory not found: %s' % full_path

        elif os.path.isdir(full_path) != directory:

            if directory == 0:

                raise ImpressionError, 'Expected %s to be a file.' % full_path

            elif directory == 1:

                raise ImpressionError, 'Expected %s to be a directory.' % full_path

        # The resource was found, so return its full path.
        return full_path

    def read_master_pages(self):

        # Read the definitions of all the master pages in order to create named entries
        # in the master_pages dictionary and to compile a list of content areas which
        # are used by master pages.

        self.master_content_refs = []

        for page in self.master_pages:

            for frame in page.frames:

                if hasattr(frame, 'content_ref'):

                    self.master_content_refs.append(frame.content_ref)


    def in_master_page(self, content_ref):

        # Checks whether a content reference is directed at the master pages
        # (MasterChap) part of the document.

        # Find the address in the content area represented by the content reference.
        #address = self.content.content_offsets[content_ref[0]]

        #return address == self.content.content_offsets[0]
        return content_ref in self.master_content_refs

    def read_document(self):

        """read_document()
        
        Read the entire document, interpreting it as it is read.
        """

        # Read the chapter pages (seventh unique entry or second from end)
        if self.verbose:

            print 'Reading chapter pages...'

        for i in range(0, len(self.chapter_pages)):

            print self.chapter_pages[i]
        
        if self.verbose:
            print 'Done'
    
    def read_word(self, file = None, addr = None):

        if file is None: file = self.file
        
        if addr is None:

            return Common.read_signed_word(self, file.read(4))

        else:

            old_addr = file.tell()
            file.seek(addr, 0)
            word = Common.read_signed_word(self, file.read(4))
            file.seek(old_addr, 0)

            return word

    def read_string(self, length = None, ending = None, include = 1, addr = None):
    
        if length == None and ending == None:
    
            print 'Internal: Incorrect use of the read_string function.'
            sys.exit()
    
        if addr is not None:

            old_addr = self.file.tell()
            self.file.seek(addr, 0)

        if length == None:
    
            # Read until one of the endings was found
            new = ''
            while 1:
    
                c = self.file.read(1)
                if not c:
                    break
                elif c in ending:
                
                    if include == 1:
                        new = new + c
                    break
                else:
                    new = new + c
    
        elif ending == None:
    
            # Read the number of characters specified
            new = self.file.read(length)
    
        else:
            # Read the number of characters specified until an ending is encountered
            new = ''
            for i in range(length):
    
                c = self.file.read(1)
                if not c:
                    break
                elif c in ending:
                
                    if include == 1:
                        new = new + c
                    break
                else:
                    new = new + c

        if addr is not None:

            self.file.seek(old_addr, 0)

        return new
    
    def info(self):
    
        print 'Information on %s:' % self.filename
        print
        print ' Creator: %s' % self.creator
        print ' Internal filename: %s' % self.internal_filename
        print
        print ' %4i styles' % len(self.styles.keys())
        print ' %4i items of content' % len(self.content)
        print ' %4i page definitions' % len(self.master_pages)
        print ' %4i coordinate definitions' % len(self.chapter_pages)
        print
    
    def show_str(self):
    
        for s in self.content:
        
            if s.content_type == 'Text':
            
                print s
                
                print
                raw_input("Press return")
    
    def find_document_details(self):

        # Read the document version (presumed to be at 0x08).
        self.version = self.read_word(addr = 0x08)

        # Make a guess about the name of the package used to generate the document
        # based on the version number.
        if self.version == 29:

            self.creator = 'Impression Publisher Plus'

        elif self.version == 28:

            # This version is used in Impression Publisher (4.01).
            self.creator = 'Impression Publisher'

        elif self.version == 22:

            self.creator = 'Impression Junior'

        elif self.version >= 20 and self.version < 30:

            self.creator = 'Impression II'

        elif self.version == 14:

            # Found from a document "!ITest" which mentions the version number
            # of the software used to create it (1.03).
            self.creator = 'Impression'

        else:

            self.creator = 'Unknown Impression package'
            #self.creator = 'Impression (%i.%i)' % (
            #    int(self.version)/10, self.version % 10
            #    )

        if self.version >= 28:

            # Calculate the scale of the view onto the document.
            self.scale = (100.0 * 0x190) / self.read_word(addr = 0x40)

        else:

            # Assume a 100% scale factor.
            self.scale = 100.0

    def read_offsets(self):
    
        # Read the main table of offsets
        offsets = {}

        # Find the table. This will depend on the document version.
        if self.version == 29: # 0x1d

            table_format = \
            {
                0x110: 'zero',    0x114: 'unknown', 0x118: 'unknown', 0x11c: 'unknown',
                0x120: 'unknown', 0x124: 'styles',  0x128: 'borders', 0x12c: 'borders',
                0x130: 'borders', 0x134: 'borders', 0x138: 'borders', 0x13c: 'borders',
                0x140: 'borders', 0x144: 'borders', 0x148: 'mapping', 0x14c: 'mapping',
                0x150: 'content table',    0x154: 'content table',
                0x158: 'master pages' ,    0x15c: 'master pages',
                0x160: 'end master pages', 0x164: 'chapter pages',
                0x168: 'content',          0x16c: 'content'
            }

        elif self.version == 28: # 0x1c

            table_format = \
            {
                0x110: 'zero',    0x114: 'styles',  0x118: 'styles',  0x11c: 'styles',
                0x120: 'styles',  0x124: 'styles',  0x128: 'borders', 0x12c: 'borders',
                0x130: 'borders', 0x134: 'borders', 0x138: 'borders', 0x13c: 'borders',
                0x140: 'borders', 0x144: 'borders', 0x148: 'mapping', 0x14c: 'mapping',
                0x150: 'content table', 0x154: 'content table',
                0x158: 'master pages' , 0x15c: 'master pages',
                0x160: 'end master pages', 0x164: 'chapter pages',
                0x168: 'content', 0x16c: 'content'
            }

        elif self.version == 22: # 0x16

            table_format = \
            {
                0x0b0: 'styles',  0x0b4: 'styles',  0x0b8: 'styles',  0x0bc: 'styles',
                0x0c0: 'styles',  0x0c4: 'borders', 0x0c8: 'borders', 0x0cc: 'borders',
                0x0d0: 'borders', 0x0d4: 'borders', 0x0d8: 'borders', 0x0dc: 'borders',
                0x0e0: 'borders', 0x0e4: 'mapping', 0x0e8: 'mapping',
                0x0ec: 'content table',
                0x0f0: 'content table', 0x0f4: 'master pages',
                0x0f8: 'master pages' , 0x0fc: 'end master pages',
                0x100: 'chapter pages', 0x104: 'end chapter pages',
                0x108: 'content',
            }

        elif self.version == 20: # 0x14

            table_format = \
            {
                0x0b0: 'styles',  0x0b4: 'styles',  0x0b8: 'styles',  0x0bc: 'styles',
                0x0c0: 'styles',  0x0c4: 'borders', 0x0c8: 'borders', 0x0cc: 'borders',
                0x0d0: 'borders', 0x0d4: 'borders', 0x0d8: 'borders', 0x0dc: 'borders',
                0x0e0: 'borders', 0x0e4: 'mapping', 0x0e8: 'mapping',
                0x0ec: 'content table',
                0x0f0: 'content table', 0x0f4: 'master pages',
                0x0f8: 'master pages' , 0x0fc: 'end master pages',
                0x100: 'chapter pages', 0x104: 'end chapter pages',
                0x108: 'content',
            }

        elif self.version == 14: # 0x0e

            table_format = \
            {
                0xb0: 'styles',  0xb4: 'unknown', 0xb8: 'unknown', 0xbc: 'unknown',
                0xc0: 'unknown', 0xc4: 'unknown', 0xc8: 'unknown', 0xcc: 'unknown',
                0xd0: 'unknown', 0xd4: 'mapping', 0xd8: 'mapping',
                0xdc: 'content table',
                0xe0: 'content table', 0xe4: 'master pages',
                0xe8: 'master pages' , 0xec: 'chapter pages',
                0xf0: 'chapter pages', 0xf4: 'unknown',
                0xf8: 'content',
            }

        else:

            raise ImpressionError, 'Sorry, the version number found in the file' + \
                ' (%s) corresponds to a document version which' % hex(self.version) + \
                ' is not supported.'

        offsets = {}

        # Examine the values at each word given by a key in the relevant table format
        # dictionary, using the categories to classify the values in a new dictionary.
        keys = table_format.keys()
        keys.sort()

        for key in keys:
        
            # Read the value from the table.
            offset = self.read_word(addr = key)

            # Read the category under which this value will be stored.
            category = table_format[key]

            if not offsets.has_key(category):

                # Create a new category in the offsets dictionary if necessary.
                offsets[category] = []

            # Store the table entry under this
            offsets[category].append(offset)

        # Check the values given in each category for variations in value.
        # Only keep categories for which there is a single value given.
        
        self.offsets = {}

        for category, values in offsets.items():

            not_same = 0
            for value in values[1:]:

                if value != values[0]:

                    # If the value found is not the same as the first value in the
                    # list then the values were not all the same.
                    not_same = 1
                    break

            if not_same == 0:

                # All the values were the same, so use this common value in the
                # list to be used throughout the class.
                self.offsets[category] = values[0]
            
            else:
            
                # Assume that the last one in the list is correct.
                self.offsets[category] = values[-1]
        
        # The twelve byte whitespace-terminated string following the table is
        # the file name.
        self.file.seek(keys[-1] + 4, 0)

        self.internal_filename = \
            self.read_string(length = 12, ending = '\0', include = 0)

    
    def read_styles(self):
    
        # Each of the offsets points to another table.
        
        # The first non-zero offset in the main table points to a style lookup table.
        style_ptr = self.offsets['styles']
        
        if self.verbose:

            print 'Reading style offsets...',

        #    print hex(keys[1]), hex(keys[2])
        
        style_offsets = []
        
        self.file.seek(style_ptr, 0)

        # The offsets are relative to the start of this table, so add the table
        # start address to each in order to obtain an address within the file.

        # Read the first offset.
        style_offsets.append(self.read_word() + style_ptr)
        
        while self.file.tell() < style_offsets[0]:
        
            offset = self.read_word()
            if offset == 0:
                style_offsets.append(None)
            else:
                style_offsets.append(offset + style_ptr)


        # Note: The last entry in the style offsets table is the address of the next
        # entry to be added. This should equal the address of the next section of the
        # file.
        #
        # i.e. style_offsets[-1] == self.offsets[2]

        self.styles = StyleCollection(
            self.docdata, style_offsets, self.old_format, self.version
            )

        if self.verbose:

            print 'Done'

    def read_borders(self):
    
        # Unknown section, usually containing a Drawfile.
        border_ptr = self.offsets['borders']
        
        if self.verbose:

            print 'Reading borders...',

        border_offsets = []
        
        self.file.seek(border_ptr, 0)

        # The offsets are relative to the start of this table, so add the table
        # start address to each in order to obtain an address within the file.

        # Read the first offset.
        border_offsets.append(self.read_word() + border_ptr)
        
        while self.file.tell() < border_offsets[0]:
        
            offset = self.read_word()
            if offset != 0:
                border_offsets.append(offset + border_ptr)


        # Note: The last entry in the border offsets table is the address of the next
        # entry to be added. This should equal the address of the next section of the
        # file.
        #
        # i.e. decor_offsets[-1] == self.offsets[3]

        self.borders = []
        
        for i in range(len(border_offsets)):

            if self.verbose and i < (len(border_offsets) - 1): print i + 1,

            # Find the border data.
            self.file.seek(border_offsets[i])
            
            if i < (len(border_offsets) - 1):
            
                # If not the last in the list:
                amount = border_offsets[i+1] - border_offsets[i]
            
            else:
            
                amount = self.offsets['mapping'] - border_offsets[i]

            if amount > 0:

                # Create an instance of the border class for each border (Drawfile).
                border = Border( self.file.read(amount), (border_offsets[i], self.filename) )

                # Create a class from that instance for use with other border classes so that
                # inheritance can mimic the behaviour of overlaying document borders.

                self.borders.append(border)
    
        if self.verbose:

            print 'Done'


    def read_mapping_area(self):

        # Read the various definitions in this section.
        self.file.seek(self.offsets['mapping'])

        # Assume that this area contains entries containing:
        #
        # - for Text files, the length of the file and the identity numbers of the
        #   pieces of content inside each file
        #
        # - for Story file, the length of the file and the number of the picture
        #   (e.g. 3 for "Story3")

        # Each entry appears to begin with a number which, for Text files, takes the
        # form 0x5003 or 0x7003 followed by a zero word then the length of the file.
        # For content within the file, a word every 0x38 bytes from the starting word
        # is read. If the bit 0x2 is set then the following word is the identity number
        # of the piece of content then sometimes a zero word.
        #
        # So, every 0x38 bytes there may occur the declaration that a file of a
        # particular length is to be read (0x03) followed every 0x38 bytes by possible
        # occurances of tags within that file (0x02).
        #
        # If words with the 0x01 bit are found instead then these are interpreted as
        # being files (common bit with Text files) but are pictures. The length appears
        # at the same place as for Text files and a number appears in the same place
        # as for content. However, this number is the story number.
        # The filetype appears to be encoded at 0x20 bytes into each 0x38 byte block as
        # 0x00xxxyyy where xxx = yyy are the 12 bit filetypes.

        if self.version == 29: # 0x1d

            block_length = 0x38

        elif self.version == 28: # 0x1c

            block_length = 0x38

        elif self.version == 22: # 0x16

            block_length = 0x28

        elif self.version == 20: # 0x14

            block_length = 0x28

        elif self.version == 14: # 0x0e

            block_length = 0x20

        # Compile a list of file lengths, identity words and pictures within each chapter to use
        # as signatures to check against actual directory contents.

        # Table signatures ----------

        end_addr = self.offsets['content table']

        table_signatures = []
        current_length = None
        current_elements = []

        while self.file.tell() < end_addr:

            start_offset = self.file.tell()

            # Read the type of data.
            data_type = self.read_word()

            if (data_type & 0x07) == 4:

                # A placeholder element
                current_elements.append( ('placeholder', self.read_word()) )

            elif (data_type & 0x07) == 3:

                # A Text file is being described.

                if current_length is not None:

                    # Store any current information.
                    table_signatures.append(
                        {'length': current_length, 'elements': current_elements}
                        )

                # Clear the current identities and stories lists.
                current_elements = [('new file', hex(data_type))]
                current_stories = {}

                # Skip a word.
                self.read_word()

                # Read the file length.
                current_length = self.read_word()

            elif (data_type & 0x07) == 2:

                # An identity within the current file has been found.

                # Add an identity to the list of elements.
                current_elements.append( ('identity', self.read_word()) )

            elif (data_type & 0x07) == 1:

                # A Story file is being described.

                # Record the story number, the file length and the file type.
                number = self.read_word()
                length = self.read_word()

                #self.file.seek(start_offset + 0x20, 0)
                #file_type = self.read_word()

                # Add the Story file details to the list of elements.
                current_elements.append( ('story', number, length) ) #, file_type

            else:

                current_elements.append( (hex(data_type),) )

            # Jump to the next entry.
            self.file.seek(start_offset + block_length, 0)

        # Add any remaining file descriptions to the list of signatures.
        if current_length is not None:

            # Store any current information.
            table_signatures.append(
                {'length': current_length, 'elements': current_elements}
                )


        # File signatures ----------

        # Examine the contents of the document directory, looking for the MasterChap directory
        # and any Chapter directories. Each of these should contain a Text file, and may also
        # contain Story files.

        # Check for all the required resources and create an index for them.
        required = []
    
        # Look for chapter directories inside the document directory and add them to
        # the required resources list.

        chapters = {}

        files = os.listdir(self.filename)

        for file in files:

            if string.lower(file[:7]) == 'chapter':

                # Enter any objects called "Chapter" into a dictionary indexed by number.
                try:
                    chapters[int(file[7:])] = file
                except ValueError:

                    raise ImpressionError, 'Unknown form for chapter directory in %s: %s' % \
                        (self.filename, file)

            elif string.lower(file[:8]) == 'masterch':

                # Enter the "MasterChap" object into a dictionary with a zero key.
                chapters[0] = file

        # Sort the chapters by number.
        keys = chapters.keys()
        keys.sort()

        # Add the chapters, sorted by number, to the required resources list.
        for number in keys:

            required.append(chapters[number])

        # We now have a list of directory names in the correct order.

        # Compile a dictionary of signatures for each directory within the document directory.

        file_signatures = {}

        for name in required:

            res_path = os.path.join(self.filename, name)

            # Clear the current identities and stories lists.
            current_identities = []
            current_id_addresses = {}
            current_stories = {}

            # Examine the contents of each directory. There may be a file called
            # "Text" in each.
            try:
                file_path = self.require_resource(res_path, 'Text', 0, 1)
            
            except ImpressionError:
                ### To do: Handle the case where a Text file is not needed separately
                ### differently to the other cases.
                current_length = 0
            
            else:
                # Set the length of the current file.
                current_length = os.path.getsize(file_path)

                # Scan the file and compile a list of identities present within.

                # Open the file.
                try:
                    file = open(file_path, 'rb')

                except IOError:

                    raise ImpressionError, \
                        'Problem with file: %s' % file_path

                # Reset the file address.
                file_addr = 0

                while file.tell() < current_length:

                    # Read two words, the first of which is an offset to the next piece of content
                    # in the file, or the end of the file.
                    next = self.read_word(file = file)
                    identity = self.read_word(file = file)

                    # Add the identity to the list with the start and end addresses of the data.
                    current_identities.append(identity)
                    current_id_addresses[identity] = (file_addr + 8, file_addr + next)

                    # Move the file address to the start of the next block of data.
                    file_addr = file_addr + next
                    file.seek(file_addr, 0)

                # Close the Text file.
                file.close()

            # Look for pictures in the chapter directory and add them to the resource
            # index.

            files = os.listdir(res_path)

            pictures = {}

            for file in files:

                if string.lower(file[:5]) == 'story':

                    # Enter any objects called "Story" into a dictionary under their number.
                    file_number = file[5:]
                    
                    # Remove any trailing filetype information, if found.
                    
                    for punct in (".", ","):
                    
                        at = string.find(file_number, punct)
                        
                        if at != -1:
                        
                            file_number = file_number[:at]
                    
                    try:
                        pictures[int(file_number)] = os.path.join(res_path, file)
                    except ValueError:

                        raise ImpressionError, 'Unknown form for picture file: %s' % \
                            os.path.join(res_path, file)

            # Sort the pictures by number.
            keys = pictures.keys()
            keys.sort()

            # Add the pictures, sorted by number, to the required resources list.
            for number in keys:

                # Add the story file to the list of stories.
                current_stories[number] = os.path.getsize(pictures[number]) #, 0) )

            # Store any current signature information in the signatures dictionary.
            file_signatures[res_path] = \
            {
                'length': current_length, 'identities': current_identities,
                'stories': current_stories, 'id addresses': current_id_addresses
            }

        # If, after all this searching, there is no MasterChap directory and therefore
        # no signature for it, create an empty one as a placeholder.
        # Search the previously used chapters dictionary to check, as case issues
        # make filename checking tedious.
        if 0 not in chapters.keys():

            file_signatures[os.path.join(self.filename, 'MasterChap')] = \
            {
                'length': 0, 'identities': [], 'stories': {}, 'id addresses': {}
            }


        # Using the signatures, compile a list of resources in the order in which they
        # appear in a virtual addressing scheme which aims to represent all the files in
        # the document directory as a single file.

        # Verify the resources exist and are correct, listing them for later use.
        self.content_list = []

        # Add resources to the list in the order in which they appear in the table of signatures.
        for signature in table_signatures:

            found = 0

            # Look for an signature in the dictionary of file signatures which satisfies the
            # requirements of the table signature.
            for name, fs in file_signatures.items():

                # The order in which Story files and identity words can occur in documents
                # is not necessarily based on ascending order. The use of a dictionary to
                # compare the presence of Story files is very useful, but more files may be
                # stored in a Chapter directory than are referenced in the document so the
                # dictionary of Story files is just used to check that elements in the table
                # signature are present.

                if fs['length'] == signature['length']:

                    # Examine all the elements in the table signature.
                    
                    # The identities found in the table signature should occur in order in the
                    # relevant Text file. Introduce an index into the file signature's identity list
                    # to ensure that this order is maintained.
                    id_index = 0

                    failed = 0

                    for element in signature['elements']:

                        if element[0] == 'identity':

                            if id_index >= len(fs['identities']):

                                # No more identities in the file signature to compare against.
                                failed = 1
                                break

                            if element[1] != fs['identities'][id_index]:

                                # The next identity in the Text file was not equal to the
                                # one in the table.
                                failed = 1
                                break

                            else:

                                # Check the next identity next time.
                                id_index = id_index + 1

                        elif element[0] == 'story':

                            # Compare the Story file numbers and lengths.

                            if element[1] not in fs['stories'].keys():

                                # A Story with this number was not found in this Chapter.
                                failed = 1
                                break

                            elif element[2] != fs['stories'][element[1]]:

                                # A Story with this number and equal length was not found
                                # in this Chapter.
                                failed = 1
                                break

                    if failed == 0 and id_index == len(fs['identities']):

                        # No discrepancies were found and all the identities were used.
                        
                        found = 1
                        
                        if self.verbose:

                            print 'Found chapter "%s"' % name

                        # Add details about the Text file and any Story files in this Chapter
                        # to the content list.

                        self.content_list.append( (name, signature, fs) )

                        # Stop comparing this table signature with file signatures.
                        break

            if found == 0:

                raise ImpressionError, 'Could not find a Chapter directory with contents' + \
                    ' fitting the description of the signature (%i) given in the document.' % \
                        table_signatures.index(signature)


    def create_content_index(self):

        # Starting at the content start address, scan each file in the list of content files,
        # creating mappings between virtual address ranges (used as keys in the index) and
        # the address ranges within the files.

        self.content_index = {}
        self.content_offsets = []

        addr = self.offsets['content']

        #master = 1

        #for file, file_type, file_length in self.content_list:
        for chapter, signature, fs in self.content_list:

            #print chapter

            # Examine each of the elements in the signature, creating an address mapping for
            # them.

            # Check for empty signatures.
            #if signature['length'] == 0: continue

            for element in signature['elements']:

                if element[0] == 'story':

                    # The whole file maps into the virtual addressing scheme.
                    start, end = addr, addr + element[2]
                    
                    # Find the Story file.
                    file = self.require_resource(chapter, 'Story%i' % element[1], 0, 1)
                    
                    self.content_index[ (start, end) ] = \
                    (
                        file, 0, element[2]
                    )

                    addr = addr + element[2]

                elif element[0] == 'identity':

                    # Read the start and end addresses of the data from the file signature.
                    start_addr, end_addr = fs['id addresses'][element[1]]

                    #print hex(start_addr), hex(end_addr)

                    start, end = addr, addr + (end_addr - start_addr)

                    # Create a mapping between the range corresponding to the length of the data
                    # and the file addresses of the range.
                    
                    # Find the Text file.
                    file = self.require_resource(chapter, 'Text', 0, 1)
                    
                    self.content_index[ (start, end) ] = \
                    (
                        file, start_addr, end_addr
                    )

                    addr = addr + (end_addr - start_addr)

                elif element[0] == 'placeholder':

                    # Store the current address as both the beginning and end of the range.
                    start, end = addr, addr

                    self.content_index[ (addr, addr) ] = None

                elif element[0] == 'new file':

                    # Store the current address as both the beginning and end of the range.
                    start, end = addr, addr

                    self.content_index[ (addr, addr) ] = None

                # Add the address to the list of content offsets.
                self.content_offsets.append(start)

        # Add the address of the end of the area.
        self.content_offsets.append(end)
        
        # Create a container for the content in the virtual file.
        self.content = ContentCollection(
            self.filename, self.content_offsets, self.content_index, strict = self.strict
            )
    

    def read_content_table(self):
    
        # Content table
        self.file.seek(self.offsets['content table'])

        if self.verbose:

            print 'Reading content offsets...',

        # Construct a list of offsets within the table so that they can be referenced by index.
        # i.e.   0     1     2   ...
        #      addr0 addr1 addr2 ...
        self.content_offsets = []
        
        # Read the offsets in the table, including the end of file offset at the end of the table.
        while self.file.tell() < self.offsets['master pages']:
    
            # Only include the offsets to items of content which aren't pointing to
            # the end of the file.
            self.content_offsets.append( self.read_word() )
            
            # Only add unique offsets.
            #if struct_offsets == [] or offset not in struct_offsets:
            #
            #    struct_offsets.append(offset)

        # Create a collection object to manage the content.
        if self.old_format == 0:

            self.content = ContentCollection(
                self.filename, self.content_offsets, strict = self.strict
                )

        else:

            self.content = ContentCollection(
                self.filename, self.content_offsets, self.content_index, strict = self.strict
                )

        if self.verbose:

            print 'Done'


    def read_master_pages_table(self):
    
        # Page definitions
        start_addr = self.offsets['master pages']    # -3 5

        if self.offsets.has_key('end master pages'):

            end_addr = self.offsets['end master pages']

        else:

            end_addr = self.offsets['chapter pages']      # -2 6

        page_offsets = self.construct_page_offsets(start_addr, end_addr)

        self.master_pages = PageCollection(
            self.docdata, page_offsets, MasterPage, self.old_format, self.version
            )

    def read_chapter_pages_table(self):
    
        # Page definitions
        start_addr = self.offsets['chapter pages']    # -2 6

        if self.offsets.has_key('end chapter pages'):

            end_addr = self.offsets['end chapter pages']

        else:

            end_addr = self.offsets['content']      # -1 7

        page_offsets = self.construct_page_offsets(start_addr, end_addr)

        self.chapter_pages = PageCollection(
            self.docdata, page_offsets, ChapterPage, self.old_format, self.version
            )

    def construct_page_offsets(self, start_addr, end_addr):

        # The pages are stored in the document in a sequence of definitions rather than being
        # referenced by pointers in a table. Therefore, this sequence needs to be traversed
        # and offsets to definitions found and stored in a list for convenience.
        page_offsets = []

        self.file.seek(start_addr)
    
        if self.verbose:

            print '  Reading definitions...',
            verbose_defn = 0

        # Set the type of data to follow. [This doesn't mean much here, but when page margins and frames
        # are being defined, it will affect their properties.]
        data_type = self.read_word() & 0xff

        # Current definition details
        current = None

        # Read definitions until we reach the next section of the file structure
        while self.file.tell() < end_addr:
        
            # Read an identifier.
            start_offset = self.file.tell()

            start_block = self.read_word()

            block_length = ((start_block & 0xffffff00L) >> 8)
            next_offset = start_offset + block_length
            
            # Look for a word with this second byte.
            if data_type == 1:
            #if start_block == 0x4c00 or start_block == 0x2400:
            
                # Page definition.

                # Skip all the words between the start and end words.
                end_offset = start_offset + block_length - 4
                self.file.seek(end_offset, 0)

                # The word which marks the end of the definition has the same
                # second byte.
                # The end of the block must match the beginning.
                end_block = self.read_word()

                if end_block & 0xffffff00L != start_block:

                    raise ImpressionError, \
                          "The identifier %x at %x did not match %x at %x" % \
                          (end_block, end_offset, start_block, start_offset)

                # Add the current definition to the list and reset the
                # current dictionary.
                if current is not None:

                    # Add the current page to the list of definitions.
                    page_offsets.append(current)

                # There is now a new page being read. Include the word before the start
                # block to provide the data type of frames.
                current = start_offset - 4

                if self.verbose:

                    verbose_defn = verbose_defn + 1
                    print verbose_defn,

            elif block_length > 0:

                # Unknown type of data

                # Skip all the words between the start and end words.
                end_offset = start_offset + block_length - 4

                if end_offset >= end_addr: break

                # Only look at the end offset if it is within the page definition area.
                self.file.seek(end_offset, 0)

                # The word which marks the end of the definition has the same
                # second byte.
                # The end of the block must match the beginning.
                end_block = self.read_word()

                if end_block & 0xffffff00L != start_block:

                    raise ImpressionError, \
                          "The identifier %x at %x did not match %x at %x" % \
                          (end_block, end_offset, start_block, start_offset)

            else:

                break

            # The type of following frame is given by the lowest word in the end of block marker.
            data_type = end_block & 0xff


        if current is not None:

            # Ensure that the last page definition is added to the list.
            page_offsets.append(current)

        # Add the end address to the list of page offsets.
        page_offsets.append(end_addr)

        if self.verbose:

            print 'Done'

        # Return the page list and index.
        return page_offsets

    
    def _old_read_coordinates(self):
    
        # "Coordinates" area
        self.file.seek(self.offsets[-2]) # -2 6
        
        self.coordinates = []
        
        # We then find about that about (0x11 * 8) - 4 bytes in the details begin
        # (the number 0x11 appears, but this may be coincidence).
        self.file.seek(self.offsets[-2] + 0x0c, 0)  # -2 6
        
        # Current page.
        current = {}
        current_frame = []
        
        # Read definitions until we reach the next section of the file structure
        while self.file.tell() < self.offsets[-1]:  # -1 7
        
            # Read an identifier.
            start = self.read_word()
            
            if start == 0x4c00:
            
                # Page area definition: add the page definition to the
                # coordinates list and reset the page dictionary.
                if current != {}:
                
                    self.coordinates.append(current)
                    current = {}
                
                # Zero word
                z = self.read_word()
                
                # Margins and paper name
                x1, y1 = self.read_word(), self.read_word()
                x2, y2 = self.read_word(), self.read_word()
                
                # Some other words
                
                # Paper definition number (from the page definitions)
                number = self.read_word()
                
                # Some other word
                u1 = self.read_word()
                # Zero word
                z = self.read_word()
                
                # Some other word
                u2 = self.read_word()
                # Zero word
                z = self.read_word()
                
                # Paper name (assume 12 bytes)
                name = self.read_string( length = 12, ending = '\0',
                                         include = 0)
                
                # Move to the next word, if necessary.
                excess = self.file.tell() % 4
                if excess != 0:
                
                    self.file.seek(self.file.tell() + 4 - excess, 0)
                
                # Some other word
                u4 = self.read_word()
                # Zero word
                z = self.read_word()
                # Zero word
                z = self.read_word()
                # Zero word
                z = self.read_word()
                
                # Define page attributes.
                current['paper'] = name
                current['paper number'] = number
                current['bbox'] = (x1, y1, x2, y2)
                
                next = start & 0xff00
                        
            elif start == 0x7000:
            
                # Some other word
                u1 = self.read_word()
                
                # Frame definition.
                x1, y1 = self.read_word(), self.read_word()
                x2, y2 = self.read_word(), self.read_word()
                
                current_frame = (x1, y1, x2, y2)
                
                # Some other words
                u2 = self.read_word()
                u3 = self.read_word()
                
                #current['unknown'] = (u1, u2, u3)
                
                # Frame definition again
                x1, y1 = self.read_word(), self.read_word()
                x2, y2 = self.read_word(), self.read_word()
                
                # There is usually some following information which I don't
                # understand.
            
                """
                if u1 >= self.offsets[1] and u1 <= self.offsets[2]:
                
                    # Could be a pointer to a style or effect
                    self.coordinates[n]['style'] = self.interpret_style(u1)
                    self.coordinates[n]['unknown'] = (u2,)
                    
                    self.coordinates[n]['frame'] = \
                        self.unit_to_pt( (x1, y1, x2, y2) )
                else:
                    self.coordinates[n]['unknown'] = (u1, u2)
                    self.coordinates[n]['guide?'] = \
                        self.unit_to_pt( (x1, y1, x2, y2) )
                """
            
            elif start == 0xa800:
            
                # Some other word
                u1 = self.read_word()
                
                # Frame bounding box
                x1, y1 = self.read_word(), self.read_word()
                x2, y2 = self.read_word(), self.read_word()
                
                # Some other words
                u2 = self.read_word()
                u3 = self.read_word()
                
                current_frame = (x1, y1, x2, y2)
                
                # Frame again
                x1, y1 = self.read_word(), self.read_word()
                x2, y2 = self.read_word(), self.read_word()
                
                # There is usually some following information which I don't
                # understand.
                
            elif start == 0x7c00:
            
                # There is usually some following information which I don't
                # understand.
                pass
        
            # The word which marks the end of the definition has the same
            # second byte.
            next = start & 0xff00
            
            # Read words until the correct word is found.
            while self.file.tell() < self.offsets[-1]:  # -1 7
            
                word = self.read_word()
                
                if word & 0xff00 == next:
                    break
            
            # Store any current frame information.
            if current_frame != []:

                # The least significant byte is used to store the current frame
                # information in the current definition.
                number = word & 0xff
                
                current[number] = current_frame
            
        # Ensure that the last page definition is added to the list.
        if current != {}:
        
            self.coordinates.append(current)


def show_offsets(name, items):

    print name+' offsets:'
    for i in items:

        print hex(i)

    print

def show_table(name, items):

    print name+' table:'
    for i in items:

        print i

    print

def verify(doc):

    i = 0
    for p in doc.chapter_pages:

        j = 0
        for f in p.frames:

            if hasattr(f, 'content_ref'):

                print 'Page %i, frame %i (%x, %i), content %s' % \
                    (i, j, f.flags, f.local, f.content_ref)
                print doc.content[f.content_ref]

            j = j + 1

        i = i + 1

def show_content_refs(doc):

    cr = []

    for p in doc.chapter_pages:

        for f in p.frames:

            if hasattr(f, 'content_ref'):

                cr.append(f.content_ref)

    cr.sort()

    area, offsets = max(cr)
    i = 0

    last = cr[0][0]

    for c in cr:

        if c[0] != last: print

        while c[0] > i:

            print '-'
            i = i + 1

        last = c[0]
                
        print c,
        i = i + 1

def show_mapping_area(doc):

    for c in doc.content_list:

        for e in c[1]['elements']:

            print e

def frame_info(doc, text_only = 0):

    i = 0

    for p in doc.chapter_pages:

        print i, p.page, p.origin

        i = i + 1

        for f in p.frames:

            if text_only == 0:

                print '%8x' % f.flags, f.inner_bbox

            if hasattr(f, 'content_ref'):

                c = doc.content[f.content_ref]

                if c.content_type == 'Text':

                    if text_only == 1:
    
                        print '%8x' % f.flags, f.inner_bbox, f.content_ref
    
                    for p in c.pieces:

                        if isinstance(p, Text):

                            print 'First line at', p.lineinfo
                            break
