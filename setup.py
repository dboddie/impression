#! /usr/bin/env python

from distutils.core import setup

from Impression import impression

setup(
    name         = "Impression",
    description  = "A package for reading Computer Concepts Impression files.",
    author       = "David Boddie",
    author_email = "david@boddie.org.uk",
    url          = "http://www.boddie.org.uk/david/Projects/Python/Impression/",
    version      = impression.__version__,
    packages     = ["Impression"],
    scripts      = ["Tools/impdraw.py",
                    "Tools/impscribus.py",
                    "Tools/impxml.py"]
    )
