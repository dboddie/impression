"""
htmlwriter.py

General writer classes for Impression documents.
"""

from Impression import fontsubs, impression
import cmdsyntax, string, sys


def clean_string(s):

    new = ''

    for i in s:

        if ord(i) >= 32:

            new = new + i

    return new


# A style to provide the default style attributes which the base style in many documents fails to provide.

class UnderBaseStyle:

    background_colour = impression.RGBTColour( 255, 255, 255, 255 )


class Text:

    def __init__(self, text, y1, y2, style):

        self.text = text
        self.y1 = y1
        self.y2 = y2
        self.style = style

        font_name, font_h = style.font_name, style.font_size
        font_w = font_h * (style.aspect_ratio / 100.0)

        self.font = font_name
        self.font_width = font_w
        self.font_height = font_h


class Space(Text):

    def __init__(self, style):

        Text.__init__(self, ' ', 0.0, 0.0, style)

class HSpace:

    def __init__(self, value, style):

        self.value = value
        self.style = style

    def width(self):

        return self.value

class Kern:

    def __init__(self, hkern, vkern, style):

        self.hkern = hkern
        self.vkern = vkern
        self.style = style

    def width(self):

        return self.hkern


class HTMLLine:

    def write_line(self, writer, first_line, trailing):

        """write_line(writer, first_line, trailing)
        \r
        \r writer:      The instance of the Writer class which created this object.
        \r              This reference is often needed because the Writer object will
        \r              maintain lists of required fonts and other information.
        \r
        \r first_line:  A flag which indicates whether this line is the first
        \r              in a paragraph.
        \r
        \r trailing:    A flag which indicates whether this line is the last
        \r              in a paragraph.
        \r
        \r
        \rThis subclass will:
        \r
        \r - Add the items in the text list (self.text_list) to the writer
        \r   object's output stream with appropriate markup.
        \r
        \r - Add underline and strikeout markup where relevant.
        \r
        \r - Add horizontal and vertical rule-offs where relevant.
        \r
        \r - Add the line container to the container object passed as a parameter
        \r   to this method.
        """
        
        # Convert the coordinates of this object to native coordinates for
        # later use.
        
        # Find the alignment of the text line using the paragraph information
        # stored in the writer object.
        left_margin, right_margin, align_style = \
            self.find_text_placement(first_line, trailing)
        
        # Horizontal distributution of text on the baseline is performed
        # by the browser.
        align = align_style.justification
        
        # Add the items in the text list to the line group.
        
        for item in self.text_list:
        
            if isinstance(item, writer.Text):
            
                # Create some text.
                pass



class HTMLWriter(writer):

    def __init__(self, filename, strict = 1):
    
        # Open the Impression document.
        try:

            self.document = impression.Document(
                loader.filename, verbose = 0, strict = strict
                )

        except impression.ImpressionError:

            raise WriterError, \
                "Could not read %s - it may be a " % loader.filename + \
                "version I don't understand."
        
        # Record the loader object. This will create a circular reference.
        self.loader = loader
        
        # Use a page counter to index the Drawfiles.
        pages = len(self.document.chapter_pages)
        digits = len(str(pages))
        format = '%%0%ii' % digits
    
    def read_page(self, page_number):
    
        # Examine each page.
        
        if page_number < 1 or page_number > len(self.document.chapter_pages):
        
            raise ImpressionLoader_Error, \
                "Page number does not correspond to a page in the document."
        
        page_number = page_number - 1
        #page_number = min(max(0, page_number - 1), len(self.document.chapter_pages) - 1)
        
        # Keep a record of which styles we have applied in this page.
        self.reset_styles()
        
        # Reset the list of fonts in use.
        self.fonts = []
        
        # Record any fonts used in the page (incuding the default font).
        self.record_font_use()
        
        # Place the objects on the page and return the page dimensions.
        return self.write_page(self.document.chapter_pages[page_number])
    
    def write_page(self, page):
    
        # Maintain a list of frame backgrounds in the order in which they were created
        # which will be added to the beginning of the Drawfile's list of objects after
        # all the other objects have been found.
        backgrounds = []

        # Find the page dimensions (not including the page bleed).
        page_width = page.margins.width() - (2 * page.bleed)
        page_height = page.margins.height() - (2 * page.bleed)
        
        # Examine each frame.
        for frame in page.frames:
        
            # Draw frame outlines.

            # Determine the frame's bounding box relative to the page origin.
            frame_bbox = frame.inner_bbox.relative_to(page.origin)

            # Employ a nasty hack to avoid dealing with frames off the page (we
            # jump to the next item in the loop).
            
            if frame_bbox.xmin > page_width: continue
            if frame_bbox.xmax < 0: continue
            if frame_bbox.ymin > 0: continue
            if frame_bbox.ymax < -page_height: continue

            coords = impression_to_sketch(
                [ frame_bbox.xmin, frame_bbox.ymin, frame_bbox.xmax, frame_bbox.ymax ],
                page_height
                )
            
            draw_coords = impression_to_draw(
                [ frame_bbox.xmin, frame_bbox.ymin, frame_bbox.xmax, frame_bbox.ymax ],
                page_height
                )
            
            self.loader.create_path(
                ( (coords[0], coords[1]), (coords[2], coords[1]),
                  (coords[2], coords[3]), (coords[0], coords[3]) ),
                None, frame.colour, 1, closed = 1
                )
            
            # Border styles
            self.draw_borders(page, frame, frame_bbox, draw_coords)
            
            if hasattr(frame, 'content_ref'):

                # If there is content associated with this frame then place it within a
                # group object.
                
                #self.loader.begin_composite(MaskGroup)
                self.loader.begin_group()
                
                # Create an object representing the clip path.
                path = (
                    (coords[0], coords[1]), (coords[2], coords[1]),
                    (coords[2], coords[3]), (coords[0], coords[3])
                    )
                
                self.loader.create_path(path, None, None, 1, closed = 1)
                
                # Find content.
                content = self.document.content[ frame.content_ref ]
                
                content_type = content.content_type
        
                if (frame.data_type & 0x07) == 3 and \
                    content.content_type == 'Draw':

                    self.write_frame_picture(content, page, frame, draw_coords)

                else:

                    self.write_frame_text(
                        content, page, page_width, page_height, frame, frame_bbox
                        )
                
                #self.loader.end_composite()
                self.loader.end_group()
                
                if content_type != "Text":
                
                    # Technique from the cmxloader import filter to create a mask
                    # group from the objects in the group just created.
                    group = self.loader.pop_last()
                    
                    if group is not None and group.is_Group:
                                    
                        objects = group.GetObjects()
                        object = MaskGroup(objects)
                    
                    self.loader.append_object(object)
        
        # Return the page dimensions in Sketch coordinates.
        return (page_width / imp_scale), (page_height / imp_scale)
    
    def write_frame_picture(self, content, page, frame, coords):
    
        # Obtain information about the orientation of the picture within the frame.
        xs, ys = frame.x_scale, frame.y_scale
        # Use Drawfile units for the displacements.
        dx = (frame.x_displacement * drawfile.units_per_point) / 1000.0
        dy = (frame.y_displacement * drawfile.units_per_point) / 1000.0
        angle = frame.angle

        #print 'Picture'
        #print
        #print 'x scale: %.3f%%' % xs
        #print 'y scale: %.3f%%' % ys
        #print 'x displacement: %.3f mm' % (frame.x_displacement * 25.4 / 72000.0)
        #print 'y displacement: %.3f mm' % (frame.y_displacement * 25.4 / 72000.0)
        #print 'angle: %.3f degrees' % angle
        #print
        #print 'Extracting content...',

        # Take the content and transform it.
        df = cStringIO.StringIO(content.data)
        d = drawfile.drawfile(df)

        # Scale the content first.
        for object in d.objects:
        
            drawtrans.stretch(
                object, (xs / 100.0, ys / 100.0), origin = (0, 0)
                )
        
        if angle != 0.0:

            # Rotate the content about the centre of the picture.
            for object in d.objects:

                drawtrans.rotate(object, angle, origin = (0, 0))

        # Translate the content relative to the bottom left of the frame then
        # displace it.

        for object in d.objects:
        
            drawtrans.translate( object, (coords[0] + dx, coords[1] + dy) )
        
        # Use the read_objects method of the DrawfileLoader parent class to
        # place the transformed Drawfile onto the page.
        self.loader.read_objects(d.objects)
    
    def write_frame_text(self, content, page, page_width, page_height, frame,
                         frame_bbox):

        # Assume that the frame does not begin with a new paragraph.
        new_para = 0

        # Only the first piece uses the first line flag stored in the text.
        first_piece = 1

        for piece in content.pieces:
        
            if isinstance(piece, impression.TextSetup):

                self.reset_styles()
                            
                for i in piece.text:

                    # A sequence of words, beginning with a command
                    
                    if i[0] == 'apply style':

                        # Apply each style.

                        for counter, style in i[1:]:

                            self.apply_style(counter, style)

                # Modify the text style.
                self.record_font_use()

            elif isinstance(piece, impression.Text):

                #print piece
                # Construct a bounding box from the frame and line information.

                ymin = piece.lineinfo.ymin
                ymax = piece.lineinfo.ymax

                #print ymin, page.margins.ymax, self.document.master_pages[page.number].margins.ymax

                #if self.document.in_master_page(frame.content_ref):
                #
                #    master_page = self.document.master_pages[page.number]
                #
                #    #if ymin < master_page.origin[1] and not frame.local_content:
                #    #if not frame.local_content:
                #
                #    # The line position needs to be found relative to the page origin.
                #    ymin = ymin - self.document.master_pages[page.number].origin[1]
                #    ymax = ymax - self.document.master_pages[page.number].origin[1]

                #while ymin < page.margins.ymin:
                #
                #    ymin = ymin + page.margins.height()
                #    ymax = ymax + page.margins.height()

                # The line position needs to be found relative to the page origin.
                #else: # elif ymin < page.origin[1]:

                #if ymin < page.origin[1] and not frame.local_content:
                #if not frame.local_content:
                if not self.document.in_master_page(frame.content_ref):

                    if ymin < page.margins.ymax and ymax > page.margins.ymin:

                        # The line position needs to be found relative to the page origin
                        # to "normalise" its position on the page.
                        ymin = ymin - page.origin[1]
                        ymax = ymax - page.origin[1]
    
                        #if self.document.version < 28: # 0x1c
                        if self.document.old_format:
    
                            ymin = ymin + self.document.chapter_pages[0].origin[1]
                            ymax = ymax + self.document.chapter_pages[0].origin[1]

                    else:

                        # The content lies outside the chapter page margins.

                        # Displace the content until it lies within the "normalised"
                        # page margins.
    
                        normal_top = self.document.chapter_pages[0].origin[1]
                        normal_base = normal_top - page.margins.height()
    
                        while ymin < normal_base and ymax < normal_base:
    
                            # Displace the content upward by the page height plus the vertical
                            # origin "offset".
                            ymin = ymin - normal_base
                            ymax = ymax - normal_base

                else:

                    if ymin < self.document.master_pages[page.number].margins.ymax and \
                        ymax > self.document.master_pages[page.number].margins.ymin:

                        # The line position needs to be found relative to the page origin
                        # to "normalise" its position on the page.
                        ymin = ymin - self.document.master_pages[page.number].origin[1]
                        ymax = ymax - self.document.master_pages[page.number].origin[1]
    
                        #if self.document.old_format:
                        if self.document.old_format and self.document.version < 28: # 0x1c
    
                            ymin = ymin + self.document.chapter_pages[0].origin[1]
                            ymax = ymax + self.document.chapter_pages[0].origin[1]

                    else:

                        # The content lies outside the master page margins.

                        # Displace the content until it lies within the "normalised"
                        # page margins.
    
                        normal_top = self.document.master_pages[0].origin[1]
                        normal_base = normal_top - \
                            self.document.master_pages[page.number].margins.height()
    
                        while ymin < normal_base and ymax < normal_base:
    
                            # Displace the content upward by the page height plus the vertical
                            # origin "offset".
                            ymin = ymin - normal_base
                            ymax = ymax - normal_base


                # The baseline is specified as a downward displacement from the top of the
                # line bounding box.
                baseline = piece.lineinfo.baseline

                coords = [
                    frame_bbox.xmin, ymin, frame_bbox.xmax, ymax
                    ]
                
                # Record whether this is the first line in a new paragraph.
                if first_piece == 1:
                
                    first_line = piece.first_line
                    first_piece = 0
                
                else:
                
                    first_line = new_para # or piece.first_line # self.new_para
                
                # Also note whether this is a trailing line in a paragraph.
                trailing = piece.trailing
                
                # If this is a trailing line then the next line will begin a new paragraph.
                new_para = trailing

                #print
                #print first_line, trailing

                # Begin a new line of text to use.
                line = SketchLine(coords, frame.inset_h)

                for i in piece.text:

                    if type(i) == types.StringType:

                        # Split the text up if it contains spaces.
                        words = string.split(clean_string(i), ' ')

                        for word in words[:-1]:

                            if word != '':

                                line.add_word(
                                    writer.Text(word, ymin, ymax, self.current_style)
                                    )

                            # Add a space between each word.
                            line.add_word( writer.Space(self.current_style) )

                        # The last word should be written without a following space.
                        if words[-1] != '':

                            line.add_word(
                                writer.Text(words[-1], ymin, ymax, self.current_style)
                                )

                        if '\r' in i:

                            # A newline was encountered. This shouldn't occur
                            # mid-line but a new line object can be created if
                            # necessary as it will not be written if it is not
                            # used.
                            if line.empty == 0:

                                # Add the line to the list of objects in the group.
                                line.write_line(
                                    self.loader, self, page_height, baseline,
                                    first_line, trailing
                                    )

                            # Following text is part of a new paragraph.
                            first_line = 1
                            new_para = 1

                            # Begin a new line, just in case more text follows on this "line".
                            line = SketchLine(coords, frame.inset_h)

                        else:

                            # Plain text
                            new_para = 0

                    else:

                        # A sequence of words, beginning with a command
                        if i[0] == 'apply style':

                            self.reset_styles()
                            
                            # Apply each style listed only if its application
                            # number (counter) has not been recorded before.

                            for counter, style in i[2:]:

                                #if counter not in self.applied:

                                self.apply_style(counter, style)

                            # Modify the text style.
                            self.record_font_use()

                        elif i[0] == 'horizontal space':

                            line.add_word( writer.HSpace(i[1], self.current_style) )

                        elif i[0] == 'kern':

                            # Ignore the first two values and interpret the
                            # final two as displacements in millipoints.
                            h_kern = i[3]
                            v_kern = i[4]

                            # Only vertical displacement is possible in text
                            # areas so ignore horizontal kerning.
                            line.add_word(
                                writer.Kern(h_kern, v_kern, self.current_style)
                                )

                        elif i[0] == 'margins':

                            # Override the styles on the line by explicitly
                            # specifying the margins.
                            line.left_margin = i[1]
                            line.right_margin = i[2]                            

                        elif i[0] == 'page number':

                            word = self.write_number(page.page, i[1])

                            line.add_word(
                                writer.Text(
                                    word, ymin, ymax, self.current_style
                                    )
                                )

                        elif i[0] == 'embedded frame':

                            # Just add horizontal space.
                            line.add_word(
                                writer.HSpace(i[5], self.current_style)
                                )

                        elif i[0] == 'special character':

                            # Add a special character to the text.
                            line.add_word(
                                writer.Text(
                                    i[1], ymin, ymax, self.current_style
                                    )
                                )

                        elif i[0] == 'line width':
                        
                            # Redefine the line margins based on this value.
                            #line.left_margin = i[2]
                            line.right_margin = i[1]

                # Add the line to the list of objects in the group, if appropriate.
                if line.empty == 0:

                    line.write_line(
                        self.loader, self, page_height, baseline, first_line,
                        trailing
                        )
    
    def record_font_use(self):

        # Determine the font name and size of the text.
        font_name = self.current_style.font_name
        
        # Check whether this name has already been declared.
        if font_name not in self.fonts:

            # Add the new font style to the font declarations.
            self.fonts.append(font_name)
    
    
    def write_number(self, number, style):

        if style == 'arabic':

            return '%i' % number

        else:

            # Roman numerals.
            s = ''

            # Find the number of thousands first.
            thousands = int(number / 1000)
            s = s + ('M' * thousands)

            number = number - (thousands * 1000)

            # If the result is greater than five hundred then add the relevant symbols.
            if number > 500:

                if number < 800:

                    s = s + 'D'
                    number = number - 500

                # Determine how many hundreds can be taken away from a thousand to
                # produce a number smaller than the one we have.
                elif number < 900:

                    s = s + 'CCM'
                    number = number - 800

                else:

                    s = s + 'CM'
                    number = number - 900

            # Find the number of hundreds remaining.
            hundreds = int(number / 100)
            s = s + ('C' * hundreds)

            number = number - (hundreds * 100)

            # If the result is greater than fifty then add the relevant symbol.
            if number > 50:

                if number < 80:

                    s = s + 'L'
                    number = number - 50

                # Determine how many tens can be taken away from a hundred to
                # produce a number smaller than the one we have.
                elif number < 90:

                    s = s + 'XXC'
                    number = number - 80

                else:

                    s = s + 'XC'
                    number = number - 90

            # Find the number of tens remaining.
            tens = int(number / 10)
            s = s + ('M' * tens)

            # If the result is greater than five then add the relevant symbols.
            if number > 5:

                if number < 8:

                    s = s + 'V'
                    number = number - 5

                # Determine how many ones can be taken away from a ten to
                # produce a number smaller than the one we have.
                elif number < 9:

                    s = s + 'IIX'
                    number = number - 8

                else:

                    s = s + 'IX'
                    number = number - 9

            # Find the number of ones remaining.
            s = s + ('I' * number)

        if style == 'roman':

            return string.lower(s)

        else:

            return s
    
    
    # Define the types of Draw object which can be rendered in a border.
    drawfile_render = \
    (
        drawfile.text, drawfile.path, drawfile.sprite, drawfile.group
    )
    
    def draw_borders(self, page, frame, frame_bbox, coords):

        # Determine which borders are present.

        corners = {
            'top left': {}, 'top right': {}, 'bottom left': {},
            'bottom right': {}
            }
        
        borders_used = {}

        # The actual border number is one greater than the number given but
        # the number is effectively an offset into the document's border list.
        # However, borders 1-10 (offsets 0-9) are usually not declared and are
        # internal to Impression.
        # Border 11 (offset 10) is usually found as the first border in
        # documents.

        objects = []

        for border, number in frame.borders.items():

            if border == 'top' and number != 0xff:

                if borders_used.has_key(number):

                    edge, corner = borders_used[number]

                else:

                    edge, corner = self.find_border(number)

                    borders_used[number] = (edge, corner)

                if corner is not None:

                    if number not in corners['top left'].keys():

                        # Add the border number to the corner dictionary to
                        # avoid duplication.
                        corners['top left'][number] = corner

                    if number not in corners['top right'].keys():

                        # Add the border number to the corner dictionary to
                        # avoid duplication.
                        corners['top right'][number] = corner

                if edge is not None and (edge.y2 > edge.y1):

                    # Ensure that the bounding box only encompasses the points
                    # and does not include space due to line widths.
                    edge = drawtrans.stretch(edge, (1, 1), copy_object = 1)

                    # Determine the stretch factor in advance.
                    stretch = (
                        1, float(coords[2] - coords[0]) / (edge.y2 - edge.y1)
                        )

                    # Stretch the edge piece.
                    edge = drawtrans.stretch(edge, stretch, copy_object = 1)
    
                    # Rotate the edge piece.
                    drawtrans.rotate(edge, 270)
    
                    # Translate the edge piece to align its lower side with the
                    # top side of the inner frame.
                    drawtrans.translate(
                        edge, (coords[0] - edge.x1, coords[3] - edge.y1)
                        )

                    # Add the object to the list of border objects to be added
                    # to the output.
                    objects.append(edge)

            elif border == 'left' and number != 0xff:

                if borders_used.has_key(number):

                    edge, corner = borders_used[number]

                else:

                    edge, corner = self.find_border(number)

                    borders_used[number] = (edge, corner)

                if corner is not None:

                    if number not in corners['top left'].keys():

                        # Add the border number to the corner dictionary to
                        # avoid duplication.
                        corners['top left'][number] = corner

                    if number not in corners['bottom left'].keys():

                        # Add the border number to the corner dictionary to
                        # avoid duplication.
                        corners['bottom left'][number] = corner

                if edge is not None and (edge.y2 > edge.y1):

                    # Ensure that the bounding box only encompasses the points
                    # and does not include space due to line widths.
                    edge = drawtrans.stretch(edge, (1, 1), copy_object = 1)

                    # Determine the stretch factor in advance.
                    stretch = (1, float(coords[3] - coords[1]) / (edge.y2 - edge.y1))

                    # Stretch the edge piece.
                    edge = drawtrans.stretch(edge, stretch, copy_object = 1)
    
                    # Translate the edge piece.
                    drawtrans.translate(
                        edge, (coords[0] - edge.x2, coords[1] - edge.y1)
                        )

                    # Add the object to the list of border objects to be added
                    # to the output.
                    objects.append(edge)

            elif border == 'bottom' and number != 0xff:

                if borders_used.has_key(number):

                    edge, corner = borders_used[number]

                else:

                    edge, corner = self.find_border(number)

                    borders_used[number] = (edge, corner)

                if corner is not None:

                    if number not in corners['bottom right'].keys():

                        # Add the border number to the corner dictionary to
                        # avoid duplication.
                        corners['bottom right'][number] = corner

                    if number not in corners['bottom left'].keys():

                        # Add the border number to the corner dictionary to
                        # avoid duplication.
                        corners['bottom left'][number] = corner

                if edge is not None and (edge.y2 > edge.y1):

                    # Ensure that the bounding box only encompasses the points
                    # and does not include space due to line widths.
                    edge = drawtrans.stretch(edge, (1, 1), copy_object = 1)

                    # Determine the stretch factor in advance.
                    stretch = (
                        1, float(coords[2] - coords[0]) / (edge.y2 - edge.y1)
                        )

                    # Stretch the edge piece.
                    edge = drawtrans.stretch(edge, stretch, copy_object = 1)
    
                    # Rotate the edge piece.
                    drawtrans.rotate(edge, 90)
    
                    # Translate the edge piece.
                    drawtrans.translate(
                        edge, (coords[0] - edge.x1, coords[1] - edge.y2)
                        )

                    # Add the object to the list of border objects to be added
                    # to the output.
                    objects.append(edge)

            elif border == 'right' and number != 0xff:

                if borders_used.has_key(number):

                    edge, corner = borders_used[number]

                else:

                    edge, corner = self.find_border(number)

                    borders_used[number] = (edge, corner)

                if corner is not None:

                    if number not in corners['bottom right'].keys():

                        # Add the border number to the corner dictionary to
                        # avoid duplication.
                        corners['bottom right'][number] = corner

                    if number not in corners['top right'].keys():

                        # Add the border number to the corner dictionary to
                        # avoid duplication.
                        corners['top right'][number] = corner

                if edge is not None and (edge.y2 > edge.y1):

                    # Ensure that the bounding box only encompasses the points
                    # and does not include space due to line widths.
                    edge = drawtrans.stretch(edge, (1, 1), copy_object = 1)

                    # Determine the stretch factor in advance.
                    stretch = (
                        1, float(coords[3] - coords[1]) / (edge.y2 - edge.y1)
                        )

                    # Stretch the edge piece.
                    edge = drawtrans.stretch(edge, stretch, copy_object = 1)
    
                    # Rotate the edge piece.
                    drawtrans.rotate(edge, 180)
    
                    # Translate the edge piece.
                    drawtrans.translate(
                        edge, (coords[2] - edge.x1, coords[1] - edge.y1)
                        )

                    # Add the object to the list of border objects to be added
                    # to the output.
                    objects.append(edge)

        # Place all the corner pieces to be rendered.

        for piece in corners['top left'].values():

            # Copy the corner piece and position it so that the bottom right
            # corner coincides with the top left corner of the inner frame.
            corner = drawtrans.translate(
                piece, ( coords[0] - piece.x2, coords[3] -piece.y1 ),
                copy_object = 1
                )

            # Add the object to the list to be rendered.
            objects.append(corner)

        for piece in corners['top right'].values():

            # Copy the corner piece and rotate it about its lower left point.
            corner = drawtrans.rotate(piece, 270, copy_object = 1)

            # Position the second corner piece so that the bottom left corner
            # of it coincides with the top right corner of the inner frame.
            drawtrans.translate(
                corner, ( coords[2] - corner.x1, coords[3] - corner.y1 )
                )

            # Add the object to the list to be rendered.
            objects.append(corner)

        for piece in corners['bottom left'].values():

            # Copy the corner piece and rotate it about its lower left point.
            corner = drawtrans.rotate(piece, 90, copy_object = 1)

            # Position the second corner piece.
            drawtrans.translate(
                corner, ( coords[0] - corner.x2, coords[1] - corner.y2 )
                )

            # Add the object to the list to be rendered.
            objects.append(corner)

        for piece in corners['bottom right'].values():

            # Copy the corner piece and rotate it about its lower left point.
            corner = drawtrans.rotate(piece, 180, copy_object = 1)
            
            # Copy the corner piece and position it.
            drawtrans.translate(
                corner, ( coords[2] - corner.x1, coords[1] - corner.y2 )
                )

            # Add the object to the list to be rendered.
            objects.append(corner)


        # Recolour the objects, if necessary.
        if hasattr(frame, 'border_colour') and (frame.border_colour.trans == 0):

            colour = (
                0,
                frame.border_colour.red,
                frame.border_colour.green,
                frame.border_colour.blue
                )

            for object in objects:

                drawtrans.recolour(object, colour)


        if objects != []:

            # Return a group object to be rendered.
            group = drawfile.group()
            group.objects = objects
            group.name = 'Frame borders'

            group.x1, group.y1, group.x2, group.y2 = \
                drawtrans.find_bbox(group.objects)

            objects = [group]
            
            # Try to get the loader to place the border objects on the page.
            self.loader.read_objects(objects)
    
    def find_border(self, number):

        if number <= 9:

            # Internal Impression border: use a substitute.
            edge, corner = self.create_default_border_pieces(number)

        else:

            # Try to find the border in the file.
            try:
                border = self.document.borders[number - 10]

            except ImpressionError:

                border = None

            if border is not None:

                # Create/extract the pieces to use for the edge and corners.
                edge, corner = self.create_border_pieces(border)

        # Return the pieces found.
        return edge, corner

    def create_border_pieces(self, border):

        # Create a Drawfile from the border data.
        f = cStringIO.StringIO(border.data)
        d = drawfile.drawfile(f)

        # Extract the first two rendered objects in the Drawfile.
        objects = []
        for obj in d.objects:

            if obj.__class__ in self.drawfile_render:

                # Add this to the list of objects to use.
                objects.append(obj)

                if len(objects) == 2: break

        # Return the pieces to use for the edges and corners of the border.
        return objects

    def create_default_border_pieces(self, number):

        # Create a default border piece depending on the border number.
        edge = drawfile.path()
        edge.outline = (0, 0, 0, 0)
        edge.fill = (255, 255, 255, 255)
        edge.path = [('move', (0, 0)), ('draw', (0, 100)), ('end',)]
        edge.x1, edge.y1 = 0, 0
        edge.x2, edge.y2 = 0, 100

        #corner = drawfile.path()
        #corner.outline = (255, 255, 255, 255)
        #corner.path = [('move', (0, 0)), ('end',)]
        corner = None

        return edge, corner
    
    def format_output(self, text):
    
        pass
    
    def write_output(self, text):
    
        pass
    
    def write_line(self, line_object):

        # The way the writer deals with the line object depends on the output
        # format. By default, just pass the writer object to the write_line
        # method of the line_object and let it discover the information it
        # needs.
        
        # Write the line using the line object specified, passing this object
        # to the line object to use for formatting and writing output.
        line_object.write_line(self, self.first_line, self.trailing)



if __name__ == "__main__":

    