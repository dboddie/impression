#! /usr/bin/env python
#
# impxml.py - a utility for converting Computer Concepts Impression
#             documents to files containing XML descriptions of their contents
#
# Copyright (C) 2005-2010 David Boddie <david@boddie.org.uk>
# 
# This file is part of the Impression package.
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301, USA.

"""
impxml.py

Write the contents of an Impression document to an XML file.
"""

import os, string, sys, types
from xml.dom import minidom

from Impression import *
from cmdsyntax import Syntax


class WriterError(Exception):

    pass


def clean_string(s):

    new = ''

    for i in s:

        if ord(i) >= 32:

            new = new + i

    return new



# A style to provide the default style attributes which the base style in many documents fails to provide.

class UnderBaseStyle:

    background_color = (255, 255, 255, 255)


class Text:

    def __init__(self, text, y1, y2, style):

        self.text = text
        self.y1 = y1
        self.y2 = y2
        self.style = style

        font_name, font_h = style.font_name, style.font_size
        font_w = font_h * (style.aspect_ratio / 100.0)

        self.font = font_name
        self.font_width = font_w
        self.font_height = font_h

        # Create a font instance.
        try:
            self.font = fonts.Font(font_name, font_w, font_h)

        except fonts.FontError:

            # It would be useful to introduce a fallback option to cover this
            # situation.
            print "Warning: No support for fonts."

    def width(self):

        return self.font.string_width(self.text)


class Space(Text):

    def __init__(self, style):

        Text.__init__(self, ' ', 0.0, 0.0, style)

class HSpace:

    def __init__(self, value, style):

        self.value = value
        self.style = style

    def width(self):

        return self.value

class Kern:

    def __init__(self, hkern, vkern, style):

        self.hkern = hkern
        self.vkern = vkern
        self.style = style

    def width(self):

        return self.hkern

class Line:

    def __init__(self, coords, inset_h):

        # Record the bounding box coordinates.
        self.x1, self.y1, self.x2, self.y2 = coords

        # Record the parent frame's horizontal inset margin.
        self.inset_h = inset_h

        # Store text objects in a list.
        self.text_list = []

        # Margins are initially undefined and can be specified by commands in
        # the text.
        self.left_margin = None
        self.right_margin = None

        # Line is initially empty.
        self.empty = 1

    def add_word(self, word):

        #if isinstance(word, Text): print word.text,
        self.text_list.append(word)
        self.empty = 0

    def write_line(self, document, frame_element, writer, page_height,
                   baseline, first_line, trailing):

        # Read the text styles, establishing the alignment of the text based
        # on the rules that
        # 
        # - paragraph styles override non-paragraph styles
        # - earlier non-paragraph styles override later ones
        # - later paragraph styles override earlier ones (maybe the other way
        #   round).

        para_align = []
        non_para_align = []

        at = 0

        for text in self.text_list:

            if text.style.paragraph_apply:

                para_align.append(text.style)

            else:

                non_para_align.append(text.style)

            at = at + 1

        if para_align == []:

            if non_para_align != []:

                # If no paragraph styles were used then use the non-paragraph
                # style alignment.
                align_style = non_para_align[0]

            else:

                print 'Information: expected text list but found', self.text_list
                return

        else:

            # Otherwise, use the paragraph alignment.
            align_style = para_align[0] # [-1]

        # Record the alignment of the text.
        alignment = align_style.justification

        # Check whether the margins are explicitly given.
        if self.left_margin is not None:

            left_margin = self.left_margin

        else:

            # Indent the first line correctly.
            if first_line == 1: # and trailing == 0:

                # This is the first line in a paragraph.
                left_margin = align_style.first_line_left_margin

            else:

                # This line is part of a paragraph.
                left_margin = align_style.left_margin

        #print align_style.first_line_left_margin, align_style.left_margin, left_margin

        # Left and right margin positions
        left_margin = min(
            max(self.x1 + left_margin, self.x1 + self.inset_h),
            self.x2 - self.inset_h
            )

        if self.right_margin is not None:

            right_margin = self.right_margin

        else:

            right_margin = align_style.right_margin

        if right_margin > 0.0:

            # Measure the right margin from the left hand side of the frame.
            right_margin = min(
                max(self.x1 + right_margin, self.x1 + self.inset_h),
                self.x2 - self.inset_h
                )

        else:
        
            # Normally, measure the margin from the right hand side of the frame.
            right_margin = min(self.x2 - self.inset_h + right_margin, self.x2 - self.inset_h)


        # Read the text.

        # Distribute the text horizontally on the line.
        x = left_margin

        # Vertically displace the items by the distance between the top of the line and the
        # baseline.
        y = self.y2 - baseline
            
        # Calculate the space used by non-whitespace in the line.
        space_used = 0.0
        number_of_spaces = 0

        for item in self.text_list:

            # Store the cursor position within the object itself.
            item.x = x

            if hasattr(item, 'y1'):

                height = item.y2 - item.y1
                item.y1 = y
                item.y2 = y + height

            if isinstance(item, Space):

                number_of_spaces = number_of_spaces + 1

            else:

                # Add the width of the item to the space used unless it is soft
                # whitespace.
                space_used = space_used + item.width()

            if isinstance(item, Kern):

                # Horizontal kerning is taken care of with the width method, but vertical
                # kerning is accounted for here.
                y = y + item.vkern

            # Move the cursor.
            x = x + item.width()


        if (alignment == 'full' and trailing == 0) or (x > right_margin):
            #(isinstance(self.text_list[-1], Space) or ):

            # Redistribute the text according to the amount of soft whitespace available
            # on the line, not including trailing soft whitespace.

            i = len(self.text_list) - 1

            while i >= 0:

                if isinstance(self.text_list[i], Space):
                    number_of_spaces = number_of_spaces - 1
                    i = i - 1
                else:
                    break

            if number_of_spaces != 0:

                each_space_width = float((right_margin - left_margin) - space_used) / number_of_spaces

                x = left_margin

                for item in self.text_list[:i+1]:

                    # Put items where they are supposed to be in this alignment scheme.
                    if x != item.x:

                        item.x = x

                    # Update the cursor, being flexible where soft whitespace is found.
                    if isinstance(item, Space):

                        x = x + each_space_width

                    else:

                        x = x + item.width()

        elif alignment == 'centre':

            # Move the text so that it is centred within the margins.
            displace_by = (right_margin - x) / 2.0

            for item in self.text_list:

                # Move all items to the right by the specified distance.
                item.x = item.x + displace_by

        elif alignment == 'right':

            # Move the text so that it is aligned with the right margin.
            displace_by = (right_margin - x)

            for item in self.text_list:

                # Move all items to the right by the specified distance.
                item.x = item.x + displace_by


        # Impression uses page coordinates in the region y <= 0 whereas
        # Draw uses y >= 0. The coordinates need to be translated upwards
        # by the page height and converted to Drawfile units.

        # Create a new element to represent the line.
        line_element = document.createElement("line")

        line_element.setAttribute("x1", str(self.x1))
        line_element.setAttribute("y1", str(self.y1))
        line_element.setAttribute("x2", str(self.x2))
        line_element.setAttribute("y2", str(self.y2))

        # Add the items in the text list to the line group.

        for item in self.text_list:

            if isinstance(item, Text):

                text_element = document.createElement("text")
                text_element.appendChild(document.createTextNode(item.text))
                coords = [ item.x, item.y1, item.x + item.width(), item.y2 ]

                x1, y1, x2, y2 = coords
                text_element.setAttribute("x1", str(x1))
                text_element.setAttribute("y1", str(y1))
                text_element.setAttribute("x2", str(x2))
                text_element.setAttribute("y2", str(y2))

                # Instances of the Writer class contain the fonts used.
                text_element.setAttribute("style", item.style.font_name)
                text_element.setAttribute("width", str(item.font_width))
                text_element.setAttribute("height", str(item.font_height))
                
                text_element.setAttribute("baseline_x", str(x1))
                text_element.setAttribute("baseline_y", str(y1))
                
                if hasattr(item.style, "background_colour"):
                
                    text_element.setAttribute(
                        "background", "#%02x%02x%02x" % (
                        item.style.background_colour.red,
                        item.style.background_colour.green,
                        item.style.background_colour.blue
                        ))

                text_element.setAttribute(
                    "foreground", "#%02x%02x%02x" % (
                    item.style.foreground_colour.red,
                    item.style.foreground_colour.green,
                    item.style.foreground_colour.blue
                    ))
                
                # Add the text object to the line group.
                line_element.appendChild(text_element)

                # Add underline and strikeout where relevant.

                # Strikeout
                if item.style.strikeout:

                    strike_element = document.createElement("strikeout")
                    strike_y = int((y1 + y2) / 2)
                    strike_element.setAttribute("x1", str(x1))
                    strike_element.setAttribute("x2", str(x2))
                    strike_element.setAttribute("y", str(strike_y))
                    
                    colour = item.style.strikeout_colour
                    strike_element.setAttribute(
                        "outline", "#%02x%02x%02x" % (
                            colour.red, colour.green, colour.blue
                        ))
                    
                    line_element.appendChild(strike_element)
                
                # Underline
                if item.style.underline:

                    underline_element = document.createElement("underline")
                    underline_y = y1 - (
                        (item.font_height * (item.style.underline_offset / 100.0))
                        )
                    
                    underline_element.setAttribute("x1", str(x1))
                    underline_element.setAttribute("x2", str(x2))
                    underline_element.setAttribute("y", str(underline_y))
                    
                    # Same colour as for strikeout.
                    colour = item.style.strikeout_colour
                    underline_element.setAttribute(
                        "outline", "#%02x%02x%02x" % (
                            colour.red, colour.green, colour.blue
                        ))
                    underline_element.setAttribute(
                        "width", str(
                        (item.font_height * (item.style.underline_size / 100.0))
                        ))

                    line_element.appendChild(underline_element)

        # Add horizontal rule-offs where relevant.

        if align_style.rule_off_below or align_style.rule_off_above:
        
            # Read any rule-off margins.
            left_end = self.x1 + align_style.left_rule_off_margin

            right_end = align_style.right_rule_off_margin

            if right_end <= 0.0:

                # Negative right margin offsets indicate measurement from the right hand side
                # of the frame.
                right_end = self.x2 + right_end

            else:

                # Positive right margin offsets indicate measurement from the left hand side
                # of the frame.
                right_end = self.x1 + right_end

        # Below the text
        if align_style.rule_off_below == 1 and trailing == 1:

            rule_off_element = document.createElement("rule")
            rule_off_y = self.y1 - align_style.rule_off_below_offset
            rule_off_element.setAttribute("x1", str(left_end))
            rule_off_element.setAttribute("x2", str(right_end))
            rule_off_element.setAttribute("y", str(rule_off_y))
            
            colour = align_style.rule_off_colour
            rule_off_element.setAttribute(
                "outline", "#%02x%02x%02x" % (
                    colour.red, colour.green, colour.blue
                ))
            rule_off_element.setAttribute(
                "width", str(align_style.rule_off_thickness)
                )

            line_element.appendChild(rule_off_element)

        # Above the text
        if align_style.rule_off_above == 1 and first_line == 1:

            rule_off_element = document.createElement("rule")
            rule_off_y = self.y2 + (
                align_style.rule_off_above_offset
                )
            rule_off_element.setAttribute("x1", str(left_end))
            rule_off_element.setAttribute("x2", str(right_end))
            rule_off_element.setAttribute("y", str(rule_off_y))
            
            colour = align_style.rule_off_colour
            rule_off_element.setAttribute(
                "outline", "#%02x%02x%02x" % (
                    colour.red, colour.green, colour.blue
                ))
            rule_off_element.setAttribute(
                "width", str(align_style.rule_off_thickness)
                )
            
            line_element.appendChild(rule_off_element)

        # Vertical rule-offs

        if align_style.tab_defns != []:

            # Read the vertical rule-off width.
            rule_off_width = align_style.vertical_rule_off_width

            # Read the rule-off colour and convert it into a string.
            colour = align_style.rule_off_colour
            rule_off_outline = "#%02x%02x%02x" % \
                (colour.red, colour.green, colour.blue)

        for position, tab_type in align_style.tab_defns:

            if tab_type == 'vertical rule':

                # Create a path for this vertical rule.
                rule_off_element = document.createElement("rule")

                rule_off_x = self.x1 + position

                rule_off_element.setAttribute("y1", str(self.y1))
                rule_off_element.setAttribute("y2", str(self.y2))
                rule_off_element.setAttribute("x", str(rule_off_x))
                
                rule_off_element.setAttribute("outline", str(rule_off_outline))
                rule_off_element.setAttribute("width", str(rule_off_width))
                
                line_element.appendChild(rule_off_element)
        
        # Add the line group to the parent group.
        frame_element.appendChild(line_element)



class Writer:

    def reset_styles(self):

        # Record the application numbers used as styles are applied.
        self.applied = []

        # The application numbers are used as keys in a dictionary of style
        # instances.
        self.styles = {}

        # The current style class being used.
        self.current_style = UnderBaseStyle

        self.apply_style(0, 0)

    def apply_style(self, counter, style_no):

        style = self.document.styles[style_no]

        self.applied.append(counter)
        self.styles[counter] = style

        # Derive a new class based on the current style.
        if self.current_style is not None:

            # The new style attributes should override the old ones (left to right inheritance lookup).

            class TextStyle(style, self.current_style):

                pass
            
            self.current_style = TextStyle

        else:

            self.current_style = style


    def remove_style(self, counter, style_no):

        if style_no >= len(self.document.styles):

            # Index exceeds length of document styles list so find the last style to
            # appear on the style menu.
            i = len(self.document.styles) - 1

            while i >= 0:

                if self.document.styles[i].on_menu == 1:

                    break

                i = i - 1

            style_no = max(0, i)

        style = self.document.styles[style_no]

        # Search the list of applied styles for the given style and remove it.

        i = len(self.applied) - 1

        while i >= 0:

            if self.applied[i] == counter:

                break

            i = i - 1

        self.applied = self.applied[:i] + self.applied[i+1:]

        if counter not in self.applied:

            del self.styles[counter]

    def read_style_attribute(self, name):

        # Return the style attribute by examining the applied styles in
        # reverse order until the relevant information has been found.

        i = len(self.applied) - 1

        while i >= 0:

            counter = self.applied[i]

            if hasattr(self.styles[counter], name):

                return getattr(self.styles[counter], name)

            i = i - 1

        return None



class XMLWriter(Writer):

    def __init__(self, infile, strict):

        # Open the Impression document.
        try:

            self.document = impression.Document(infile, verbose = 1, strict = strict)

        except impression.ImpressionError:

            raise WriterError, \
                "Could not read %s - it may be a version I don't understand." % infile
    
    def write(self, outdir, begin_page, end_page, outline, show_all_pictures):
    
        # Outline flag: if set then show only an outline (no pictures).
        self.outline = outline

        # Show all pictures flag: don't discard pictures which fall outside their frame
        # boundaries.
        self.show_all_pictures = show_all_pictures

        # Read all master pages (temporary solution to referencing master pages
        # by name).
        #self.document.read_master_pages()

        # New paragraph flag
        self.new_para = 1

        # Use a page counter to index the files.
        pages = len(self.document.chapter_pages)
        digits = len(str(pages))
        format = '%%0%ii' % digits

        # Examine each page.

        begin_page = min(max(0, begin_page - 1), len(self.document.chapter_pages) - 1)

        if end_page == -1: end_page = len(self.document.chapter_pages)
        
        end_page = min(max(1, end_page, begin_page + 1), len(self.document.chapter_pages))

        print
        print 'Writing pages:'
        print

        for page in range(begin_page, end_page):

            # Keep a record of which styles we have applied in this page.
            self.reset_styles()
    
            # Reset the list of fonts in use.
            self.fonts = []
    
            # Record any fonts used in the page (including the default font).
            self.record_font_use()
    
            self.write_content(
                outdir + os.sep + format % (page + 1) + os.extsep + "xml",
                self.document.chapter_pages[page]
                )


    def write_content(self, path, page):

        print path

        try:

            f = open(path, 'wb')

        except IOError:

            print 'Failed to open %s for writing the page.' % path
            return

        # Create an XML file for each page.
        document = minidom.Document()

        # Find the page dimensions (not including the page bleed).
        page_width = page.margins.width() - (2 * page.bleed)
        page_height = page.margins.height() - (2 * page.bleed)

        # Give the page a bounding box corresponding to the page dimensions.
        page_element = document.createElement("page")
        page_element.setAttribute("x", str(0))
        page_element.setAttribute("y", str(0))
        page_element.setAttribute("width", str(page_width))
        page_element.setAttribute("height", str(page_height))
        
        # Examine each frame.
        for frame in page.frames:

            # Write frame information to the document.
            frame_element = document.createElement("frame");

            # Determine the frame's bounding box relative to the page origin.
            frame_bbox = frame.inner_bbox.relative_to(page.origin)

            # Employ a nasty hack to avoid dealing with frames off the page
            # (we jump to the next item in the loop).
            
            frame_element.setAttribute("x", str(frame_bbox.xmin))
            frame_element.setAttribute("y", str(frame_bbox.ymin))
            frame_element.setAttribute("width", str(frame_bbox.xmax - frame_bbox.xmin))
            frame_element.setAttribute("height", str(frame_bbox.ymax - frame_bbox.ymin))
            
            # 00 rr gg bb
            # Frame outline colour
            if hasattr(frame, 'colour'):
            
                if frame.colour.trans == 0:
                
                    frame_element.setAttribute(
                        "colour", "#%02x%02x%02x" % (
                            frame.colour.red, frame.colour.green, frame.colour.blue
                            )
                        )

            # Border styles

            # Add the border objects to the frame.
            for edge, number in frame.borders.items():
            
                if number != 0xff:
                
                    border = document.createElement("border")
                    border.setAttribute("edge", str(edge))
                    border.setAttribute("number", str(number))
                    frame_element.appendChild(border)
            
            if hasattr(frame, 'content_ref'):

                # Process the contents of this frame.

                # Find content.
                content = self.document.content[ frame.content_ref ]
                
                if (frame.data_type & 0x07) == 3 and self.outline == 0 and \
                    content.content_type == 'Draw':

                    self.write_frame_picture(
                        document, frame_element, content, page, frame
                        )

                else:

                    self.write_frame_text(
                        document, frame_element, content, page, page_width,
                        page_height, frame, frame_bbox
                        )
            
            # Add the frame to the page.
            page_element.appendChild(frame_element)
        
        # Append the page to the document.
        document.appendChild(page_element)
        
        # Write the XML file.
        f.write(document.toprettyxml(indent = " "))
        
        # Close the file.
        f.close()


    def write_frame_picture(self, document, frame_element, content, page,
                            frame):

        # Obtain information about the orientation of the picture within
        # the frame.

        #print 'Picture'
        #print
        #print 'x scale: %.3f%%' % xs
        #print 'y scale: %.3f%%' % ys
        #print 'x displacement: %.3f mm' % (dx * 25.4 / 72000.0)
        #print 'y displacement: %.3f mm' % (dy * 25.4 / 72000.0)
        #print 'angle: %.3f degrees' % angle
        #print
        #print 'Extracting content...',

        # Take the content and transform it.
        picture = document.createElement("picture")
        picture.setAttribute("type", "image/x-drawfile")
        picture.setAttribute("xscale", str(frame.x_scale))
        picture.setAttribute("yscale", str(frame.y_scale))
        picture.setAttribute("dx", str(frame.x_displacement))
        picture.setAttribute("dy", str(frame.y_displacement))
        picture.setAttribute("angle", str(frame.angle))
        content = document.createTextNode(content.data)
        picture.appendChild(content)
        
        frame_element.appendChild(picture)
    
    def copy_attribute(self, cl, attr, element, attribute):
    
        if hasattr(cl, attr):
        
            element.setAttribute(attribute, str(getattr(cl, attr)))
    
    def create_style_element(self, document, frame_element):
    
        style_element = document.createElement("style")
        self.copy_attribute(self.current_style, "name",
                           style_element, "name")
        self.copy_attribute(self.current_style, "font_name",
                            style_element, "font")
        self.copy_attribute(self.current_style, "bold_on",
                            style_element, "bold")
        self.copy_attribute(self.current_style, "italic_on",
                            style_element, "italic")
        self.copy_attribute(self.current_style, "font_size",
                            style_element, "size")
        self.copy_attribute(self.current_style, "aspect_ratio",
                            style_element, "aspect")
        self.copy_attribute(self.current_style, "superscript",
                            style_element, "superscript")
        self.copy_attribute(self.current_style, "subscript",
                            style_element, "subscript")
        self.copy_attribute(self.current_style, "script_aspect",
                            style_element, "scriptaspect")
        self.copy_attribute(self.current_style, "subscript_offset",
                            style_element, "subscriptoffset")
        self.copy_attribute(self.current_style, "superscript_offset",
                            style_element, "superscriptoffset")
        self.copy_attribute(self.current_style, "foreground_colour",
                            style_element, "foreground")
        self.copy_attribute(self.current_style, "background_colour",
                            style_element, "background")

        if hasattr(self.current_style, "underline"):

            underline = document.createElement("underline")
            self.copy_attribute(self.current_style, "underline_offset",
                                underline, "offset")
            self.copy_attribute(self.current_style, "underline_size",
                                underline, "size")
            style_element.appendChild(underline)

        if hasattr(self.current_style, "strikeout"):

            strikeout = document.createElement("strikeout")
            self.copy_attribute(self.current_style, "strikeout_colour",
                                strikeout, "colour")
            style_element.appendChild(strikeout)

        self.copy_attribute(self.current_style, "tracking",
                            style_element, "tracking")
        self.copy_attribute(self.current_style, "hyphenation",
                            style_element, "hyphenation")
        self.copy_attribute(self.current_style, "leadering",
                            style_element, "leadering")
        self.copy_attribute(self.current_style, "decimal_tab",
                            style_element, "decimal")

        self.copy_attribute(self.current_style, "above_para",
                            style_element, "abovepara")
        self.copy_attribute(self.current_style, "below_para",
                            style_element, "belowpara")
        self.copy_attribute(self.current_style, "justification",
                            style_element, "justification")
        self.copy_attribute(self.current_style, "line_spacing",
                            style_element, "linespacing")
        self.copy_attribute(self.current_style, "line_spacing_units",
                            style_element, "linespacingunits")
        self.copy_attribute(self.current_style, "page_grid_lock",
                            style_element, "pagegridlock")
        self.copy_attribute(self.current_style, "keep_together_length",
                            style_element, "keeptogetherlength")
        self.copy_attribute(self.current_style, "keep_together_single_para",
                            style_element, "keeptogethersinglepara")
        self.copy_attribute(self.current_style, "keep_together_multiple_para",
                            style_element, "keeptogethermultiplepara")
        self.copy_attribute(self.current_style, "keep_with_next_para",
                            style_element, "keepwithnextpara")
        self.copy_attribute(self.current_style, "auto_indent",
                            style_element, "autoindent")
        self.copy_attribute(self.current_style, "first_line_left_margin",
                            style_element, "firstlineleftmargin")
        self.copy_attribute(self.current_style, "left_margin",
                            style_element, "leftmargin")
        self.copy_attribute(self.current_style, "right_margin",
                            style_element, "rightmargin")
        self.copy_attribute(self.current_style, "left_rule_off_margin",
                            style_element, "leftruleoffmargin")
        self.copy_attribute(self.current_style, "right_rule_off_margin",
                            style_element, "rightruleoffmargin")

        if hasattr(self.current_style, "tab_defns"):

            tabs_element = document.createElement("tabs")
            for tab_position, tab_type in self.current_style.tab_defns:

                tab_element = document.createElement("tab")
                tab_element.setAttribute("type", str(tab_type))
                tab_element.setAttribute("position", str(tab_position))
                tabs_element.appendChild(tab_element)

            style_element.appendChild(tabs_element)

        self.copy_attribute(self.current_style, "rule_off_thickness",
                            style_element, "ruleoffthickness")
        self.copy_attribute(self.current_style, "rule_off_above",
                            style_element, "ruleoffabove")
        self.copy_attribute(self.current_style, "rule_off_below",
                            style_element, "ruleoffbelow")
        self.copy_attribute(self.current_style, "rule_off_colour",
                            style_element, "ruleoffcolour")
        self.copy_attribute(self.current_style, "vertical_rule_off_width",
                            style_element, "verticalruleoffwidth")

        frame_element.appendChild(style_element)
    
    def write_frame_text(self, document, frame_element, content, page,
                         page_width, page_height, frame, frame_bbox):

        # Assume that the frame does not begin with a new paragraph.
        new_para = 0

        # Only the first piece uses the first line flag stored in the text.
        first_piece = 1

        for piece in content.pieces:

            if isinstance(piece, impression.TextSetup):

                self.reset_styles()

                for i in piece.text:

                    # A sequence of words, beginning with a command

                    if i[0] == 'apply style':

                        # Apply each style.

                        for counter, style in i[1:]:

                            self.apply_style(counter, style)
                
                self.create_style_element(document, frame_element)

            elif isinstance(piece, impression.Text):

                #print piece
                # Construct a bounding box from the frame and line information.

                ymin = piece.lineinfo.ymin
                ymax = piece.lineinfo.ymax

                #print ymin, page.margins.ymax, self.document.master_pages[page.number].margins.ymax

                #if self.document.in_master_page(frame.content_ref):
                #
                #    master_page = self.document.master_pages[page.number]
                #
                #    #if ymin < master_page.origin[1] and not frame.local_content:
                #    #if not frame.local_content:
                #
                #    # The line position needs to be found relative to the page origin.
                #    ymin = ymin - self.document.master_pages[page.number].origin[1]
                #    ymax = ymax - self.document.master_pages[page.number].origin[1]

                #while ymin < page.margins.ymin:
                #
                #    ymin = ymin + page.margins.height()
                #    ymax = ymax + page.margins.height()

                # The line position needs to be found relative to the page origin.
                #else: # elif ymin < page.origin[1]:

                #if ymin < page.origin[1] and not frame.local_content:
                #if not frame.local_content:
                if not self.document.in_master_page(frame.content_ref):

                    if ymin < page.margins.ymax and ymax > page.margins.ymin:

                        # The line position needs to be found relative to the page origin
                        # to "normalise" its position on the page.
                        ymin = ymin - page.origin[1]
                        ymax = ymax - page.origin[1]
    
                        #if self.document.version < 28: # 0x1c
                        if self.document.old_format:
    
                            ymin = ymin + self.document.chapter_pages[0].origin[1]
                            ymax = ymax + self.document.chapter_pages[0].origin[1]

                    else:

                        # The content lies outside the chapter page margins.

                        # Displace the content until it lies within the "normalised"
                        # page margins.
    
                        normal_top = self.document.chapter_pages[0].origin[1]
                        normal_base = normal_top - page.margins.height()
    
                        while ymin < normal_base and ymax < normal_base:
    
                            # Displace the content upward by the page height plus the vertical
                            # origin "offset".
                            ymin = ymin - normal_base
                            ymax = ymax - normal_base

                else:

                    if ymin < self.document.master_pages[page.number].margins.ymax and \
                        ymax > self.document.master_pages[page.number].margins.ymin:

                        # The line position needs to be found relative to the page origin
                        # to "normalise" its position on the page.
                        ymin = ymin - self.document.master_pages[page.number].origin[1]
                        ymax = ymax - self.document.master_pages[page.number].origin[1]
    
                        #if self.document.old_format:
                        if self.document.old_format and self.document.version < 28: # 0x1c
    
                            ymin = ymin + self.document.chapter_pages[0].origin[1]
                            ymax = ymax + self.document.chapter_pages[0].origin[1]

                    else:

                        # The content lies outside the master page margins.

                        # Displace the content until it lies within the "normalised"
                        # page margins.
    
                        normal_top = self.document.master_pages[0].origin[1]
                        normal_base = normal_top - \
                            self.document.master_pages[page.number].margins.height()
    
                        while ymin < normal_base and ymax < normal_base:
    
                            # Displace the content upward by the page height plus the vertical
                            # origin "offset".
                            ymin = ymin - normal_base
                            ymax = ymax - normal_base


                # The baseline is specified as a downward displacement from the top of the
                # line bounding box.
                baseline = piece.lineinfo.baseline

                coords = [
                    frame_bbox.xmin, ymin, frame_bbox.xmax, ymax
                    ]

                # Record whether this is the first line in a new paragraph.
                if first_piece == 1:

                    first_line = piece.first_line
                    first_piece = 0

                else:

                    first_line = new_para # or piece.first_line # self.new_para

                # Also note whether this is a trailing line in a paragraph.
                trailing = piece.trailing

                # If this is a trailing line then the next line will begin a new paragraph.
                new_para = trailing

                #print
                #print first_line, trailing

                # Begin a new line of text to use.
                line = Line(coords, frame.inset_h)

                for i in piece.text:

                    if type(i) == types.StringType:

                        # Split the text up if it contains spaces.
                        words = string.split(clean_string(i), ' ')

                        for word in words[:-1]:

                            if word != '':

                                line.add_word( Text(word, ymin, ymax, self.current_style) )

                            # Add a space between each word.
                            line.add_word( Space(self.current_style) )

                        # The last word should be written without a following space.
                        if words[-1] != '':

                            line.add_word( Text(words[-1], ymin, ymax, self.current_style) )

                        if '\r' in i:

                            # A newline was encountered. This shouldn't occur mid-line but
                            # a new line object can be created if necessary as it will not
                            # be written if it is not used.
                            if line.empty == 0:

                                # Add the line to the list of objects in the group.
                                line.write_line(
                                    document, frame_element, self, page_height,
                                    baseline, first_line, trailing
                                    )

                            # Following text is part of a new paragraph.
                            first_line = 1
                            new_para = 1

                            # Begin a new line, just in case more text follows on this "line".
                            line = Line(coords, frame.inset_h)

                        else:

                            # Plain text
                            new_para = 0

                    else:

                        # A sequence of words, beginning with a command
                        if i[0] == 'apply style':

                            self.reset_styles()
                            
                            # Apply each style listed only if its application number
                            # (counter) has not been recorded before.

                            for counter, style in i[2:]:

                                #if counter not in self.applied:

                                self.apply_style(counter, style)

                            self.create_style_element(document, frame_element)

                        elif i[0] == 'horizontal space':

                            line.add_word( HSpace(i[1], self.current_style) )

                        elif i[0] == 'kern':

                            # Ignore the first two values and interpret the final two as displacements
                            # in millipoints.
                            h_kern = i[3]
                            v_kern = i[4]

                            # Only vertical displacement is possible in text areas so ignore horizontal
                            # kerning.
                            line.add_word( Kern(h_kern, v_kern, self.current_style) )

                        elif i[0] == 'margins':

                            # Override the styles on the line by explicitly specifying the
                            # margins.
                            line.left_margin = i[1]
                            line.right_margin = i[2]                            

                        elif i[0] == 'page number':

                            if hasattr(page, "page"):
                            
                                word = self.write_number(page.page, i[1])
                                
                                line.add_word( Text(word, ymin, ymax, self.current_style) )

                        elif i[0] == 'embedded frame':

                            # Just add horizontal space.
                            line.add_word( HSpace(i[5], self.current_style) )

                        elif i[0] == 'special character':

                            # Add a special character to the text.
                            line.add_word( Text(i[1], ymin, ymax, self.current_style) )

                        elif i[0] == 'line width':

                            # Redefine the line margins based on this value.
                            line.left_margin = i[2]
                            line.right_margin = i[1]

                # Add the line to the list of objects in the group, if appropriate.
                if line.empty == 0:

                    line.write_line(document, frame_element, self, page_height,
                                    baseline, first_line, trailing)


    def record_font_use(self):

        # Determine the font name and size of the text.
        #font_name = self.read_style_font()
        font_name = self.current_style.font_name
        font_size = self.current_style.font_size / 1000.0

        # Check whether this combination already exists.
        if font_name not in self.fonts:

            # Add the new font style to the font declarations.
            self.fonts.append(font_name)


    def write_number(self, number, style):

        if style == 'arabic':

            return '%i' % number

        else:

            # Roman numerals.
            s = ''

            # Find the number of thousands first.
            thousands = int(number / 1000)
            s = s + ('M' * thousands)

            number = number - (thousands * 1000)

            # If the result is greater than five hundred then add the relevant symbols.
            if number > 500:

                if number < 800:

                    s = s + 'D'
                    number = number - 500

                # Determine how many hundreds can be taken away from a thousand to
                # produce a number smaller than the one we have.
                elif number < 900:

                    s = s + 'CCM'
                    number = number - 800

                else:

                    s = s + 'CM'
                    number = number - 900

            # Find the number of hundreds remaining.
            hundreds = int(number / 100)
            s = s + ('C' * hundreds)

            number = number - (hundreds * 100)

            # If the result is greater than fifty then add the relevant symbol.
            if number > 50:

                if number < 80:

                    s = s + 'L'
                    number = number - 50

                # Determine how many tens can be taken away from a hundred to
                # produce a number smaller than the one we have.
                elif number < 90:

                    s = s + 'XXC'
                    number = number - 80

                else:

                    s = s + 'XC'
                    number = number - 90

            # Find the number of tens remaining.
            tens = int(number / 10)
            s = s + ('M' * tens)

            # If the result is greater than five then add the relevant symbols.
            if number > 5:

                if number < 8:

                    s = s + 'V'
                    number = number - 5

                # Determine how many ones can be taken away from a ten to
                # produce a number smaller than the one we have.
                elif number < 9:

                    s = s + 'IIX'
                    number = number - 8

                else:

                    s = s + 'IX'
                    number = number - 9

            # Find the number of ones remaining.
            s = s + ('I' * number)

        if style == 'roman':

            return string.lower(s)

        else:

            return s


if __name__ == '__main__':

    syntax = (
        "[-b <begin at page>] [-e <end at page>] [--outline] "
        "[--lenient] [--show-all-pictures] <Impression file> "
        "<Output directory>"
        )
    
    syntax_obj = Syntax(syntax)
    
    # Check the arguments.
    matches = syntax_obj.get_args(sys.argv[1:])
    
    if len(matches) != 1:

        sys.stderr.write('Usage: impxml.py %s\n' % syntax)
        sys.exit(1)
    
    match = matches[0]
    
    # Look for beginning and ending pages.
    if match.has_key("begin at page"):

        try:
            begin_page = int(match["begin at page"])
        except ValueError:
            print '%s is not a valid beginning page number.' % args
            sys.exit()

    else:

        begin_page = 0

    if match.has_key("end at page"):

        try:
            end_page = int(match["end at page"])
        except ValueError:
            print '%s is not a valid ending page number.' % args
            sys.exit()

    else:

        end_page = -1

    # Read the input file and output directory details.
    infile = match["Impression file"]
    outdir = match["Output directory"]

    # Check that the output directory exists.

    # Create the output directory, if necessary.
    if not os.path.exists(outdir):

        os.mkdir(outdir)

    elif not os.path.isdir(outdir):

        print '%s already exists and is not a directory.' % outdir
        sys.exit()

    # Instantiate a writer object.
    try:

        writer = XMLWriter(infile, not match.has_key("lenient"))
        writer.write(outdir, begin_page, end_page, match.has_key("outline"),
            match.has_key("show-all-pictures"))

    except WriterError:

        e_type, e_value, e_traceback = sys.exc_info()
        output = filter(lambda x: x != None, e_value.args)
        print string.join(output, '\n')
        sys.exit()

    # Exit
    sys.exit()
