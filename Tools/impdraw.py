#! /usr/bin/env python
#
# impdraw.py - a utility for converting Computer Concepts Impression
#              documents to Acorn Drawfiles
#
# Copyright (C) 2002-2010 David Boddie <david@boddie.org.uk>
# 
# This file is part of the Impression package.
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301, USA.

"""
impdraw.py

Dump the contents of frames in an Impression document into a collection of
Drawfiles.
"""

import cStringIO, os, string, sys, types

from Impression import *
from Impression import drawtrans

import drawfile


class WriterError(Exception):

    pass


def impression_to_draw(coords, page_height):

    new_coords = []

    i = 0

    for c in coords:

        if i % 2 == 0:
            new_coords.append( int( c * (drawfile.units_per_point / 1000.0) ) )
        elif i % 2 == 1:
            new_coords.append( int( (page_height + c) * (drawfile.units_per_point / 1000.0) ) )

        i = i + 1

    return map(int, new_coords)

def clean_string(s):

    new = ''

    for i in s:

        if ord(i) >= 32:

            new = new + i

    return new



# A style to provide the default style attributes which the base style in many documents fails to provide.

class UnderBaseStyle:

    background_color = impression.RGBTColour( 255, 255, 255, 255 )


class Text:

    def __init__(self, text, y1, y2, style):

        self.text = text
        self.y1 = y1
        self.y2 = y2
        self.style = style

        font_name, font_h = style.font_name, style.font_size
        font_w = font_h * (style.aspect_ratio / 100.0)

        self.font = font_name
        self.font_width = font_w
        self.font_height = font_h

        # Create a font instance.
        try:
            self.font = fonts.Font(font_name, font_w, font_h)

        except fonts.FontError:

            # It would be useful to introduce a fallback option to cover this situation.
            print "Warning: No support for fonts."

    def width(self):

        return self.font.string_width(self.text)


class Space(Text):

    def __init__(self, style):

        Text.__init__(self, ' ', 0.0, 0.0, style)

class HSpace:

    def __init__(self, value, style):

        self.value = value
        self.style = style

    def width(self):

        return self.value

class Kern:

    def __init__(self, hkern, vkern, style):

        self.hkern = hkern
        self.vkern = vkern
        self.style = style

    def width(self):

        return self.hkern

class Line:

    def __init__(self, coords, inset_h):

        # Record the bounding box coordinates.
        self.x1, self.y1, self.x2, self.y2 = coords

        # Record the parent frame's horizontal inset margin.
        self.inset_h = inset_h

        # Store text objects in a list.
        self.text_list = []

        # Margins are initially undefined and can be specified by commands in the text.
        self.left_margin = None
        self.right_margin = None

        # Line is initially empty.
        self.empty = 1

    def add_word(self, word):

        #if isinstance(word, Text): print word.text,
        self.text_list.append(word)
        self.empty = 0

    def write_line(self, group, writer, page_height, baseline, first_line, trailing):

        # Read the text styles, establishing the alignment of the text based on the
        # rules that
        # 
        # - paragraph styles override non-paragraph styles
        # - earlier non-paragraph styles override later ones
        # - later paragraph styles override earlier ones (maybe the other way round).

        para_align = []
        non_para_align = []

        at = 0

        for text in self.text_list:

            if text.style.paragraph_apply:

                para_align.append(text.style)

            else:

                non_para_align.append(text.style)

            at = at + 1

        if para_align == []:

            if non_para_align != []:

                # If no paragraph styles were used then use the non-paragraph style alignment.
                align_style = non_para_align[0]

            else:

                print 'Information: expected text list but found', self.text_list
                return

        else:

            # Otherwise, use the paragraph alignment.
            align_style = para_align[0] # [-1]

        # Record the alignment of the text.
        alignment = align_style.justification

        # Check whether the margins are explicitly given.
        if self.left_margin is not None:

            left_margin = self.left_margin

        else:

            # Indent the first line correctly.
            if first_line == 1: # and trailing == 0:

                # This is the first line in a paragraph.
                left_margin = align_style.first_line_left_margin

            else:

                # This line is part of a paragraph.
                left_margin = align_style.left_margin

        #print align_style.first_line_left_margin, align_style.left_margin, left_margin

        # Left and right margin positions
        left_margin = min(
            max(self.x1 + left_margin, self.x1 + self.inset_h),
            self.x2 - self.inset_h
            )

        if self.right_margin is not None:

            right_margin = self.right_margin

        else:

            right_margin = align_style.right_margin

        if right_margin > 0.0:

            # Measure the right margin from the left hand side of the frame.
            right_margin = min(
                max(self.x1 + right_margin, self.x1 + self.inset_h),
                self.x2 - self.inset_h
                )

        else:
        
            # Normally, measure the margin from the right hand side of the frame.
            right_margin = min(self.x2 - self.inset_h + right_margin, self.x2 - self.inset_h)


        # Read the text.

        # Distribute the text horizontally on the line.
        x = left_margin

        # Vertically displace the items by the distance between the top of the line and the
        # baseline.
        y = self.y2 - baseline
            
        # Calculate the space used by non-whitespace in the line.
        space_used = 0.0
        number_of_spaces = 0

        for item in self.text_list:

            # Store the cursor position within the object itself.
            item.x = x

            if hasattr(item, 'y1'):

                height = item.y2 - item.y1
                item.y1 = y
                item.y2 = y + height

            if isinstance(item, Space):

                number_of_spaces = number_of_spaces + 1

            else:

                # Add the width of the item to the space used unless it is soft
                # whitespace.
                space_used = space_used + item.width()

            if isinstance(item, Kern):

                # Horizontal kerning is taken care of with the width method, but vertical
                # kerning is accounted for here.
                y = y + item.vkern

            # Move the cursor.
            x = x + item.width()


        if (alignment == 'full' and trailing == 0) or (x > right_margin):
            #(isinstance(self.text_list[-1], Space) or ):

            # Redistribute the text according to the amount of soft whitespace available
            # on the line, not including trailing soft whitespace.

            i = len(self.text_list) - 1

            while i >= 0:

                if isinstance(self.text_list[i], Space):
                    number_of_spaces = number_of_spaces - 1
                    i = i - 1
                else:
                    break

            if number_of_spaces != 0:

                each_space_width = float((right_margin - left_margin) - space_used) / number_of_spaces

                x = left_margin

                for item in self.text_list[:i+1]:

                    # Put items where they are supposed to be in this alignment scheme.
                    if x != item.x:

                        item.x = x

                    # Update the cursor, being flexible where soft whitespace is found.
                    if isinstance(item, Space):

                        x = x + each_space_width

                    else:

                        x = x + item.width()

        elif alignment == 'centre':

            # Move the text so that it is centred within the margins.
            displace_by = (right_margin - x) / 2.0

            for item in self.text_list:

                # Move all items to the right by the specified distance.
                item.x = item.x + displace_by

        elif alignment == 'right':

            # Move the text so that it is aligned with the right margin.
            displace_by = (right_margin - x)

            for item in self.text_list:

                # Move all items to the right by the specified distance.
                item.x = item.x + displace_by


        # Impression uses page coordinates in the region y <= 0 whereas
        # Draw uses y >= 0. The coordinates need to be translated upwards
        # by the page height and converted to Drawfile units.
        draw_coords = impression_to_draw(
            [ self.x1, self.y1, self.x2, self.y2 ], page_height
            )

        # Create a new group to represent the line.
        line_group = drawfile.group()

        line_group.x1 = draw_coords[0]
        line_group.y1 = draw_coords[1]
        line_group.x2 = draw_coords[2]
        line_group.y2 = draw_coords[3]
        line_group.name = 'Line group'

        # Add the items in the text list to the line group.

        for item in self.text_list:

            if isinstance(item, Text):

                text = drawfile.text()
                text.text = item.text
                coords = [ item.x, item.y1, item.x + item.width(), item.y2 ]

                text.x1, text.y1, text.x2, text.y2 = \
                    impression_to_draw(coords, page_height)

                # Instances of the Writer class contain the fonts used.
                text.style = writer.fonts.index(item.style.font_name) + 1
                text.size = map(int,
                    [ item.font_width * drawfile.units_per_point / 1000.0,
                      item.font_height * drawfile.units_per_point / 1000.0 ]
                    )

                text.baseline = [text.x1, text.y1]

                if hasattr(item.style, "background_colour"):
                
                    text.background = ( 0,
                        item.style.background_colour.red,
                        item.style.background_colour.green,
                        item.style.background_colour.blue
                        )

                text.foreground = ( 0,
                    item.style.foreground_colour.red,
                    item.style.foreground_colour.green,
                    item.style.foreground_colour.blue
                    )

                # Add the text object to the line group.
                line_group.objects.append(text)

                # Add underline and strikeout where relevant.

                # Strikeout
                if item.style.strikeout:

                    strike = drawfile.path()
                    strike_y = int((text.y1 + text.y2) / 2)
                    strike.path = \
                    [
                        ('move', (text.x1, strike_y)),
                        ('draw', (text.x2, strike_y)),
                        ('end',)
                    ]

                    colour = item.style.strikeout_colour
                    strike.outline = (0, colour.red, colour.green, colour.blue)
                    strike.fill = (255, 255, 255, 255)
                    strike.x1 = text.x1
                    strike.y1 = strike_y
                    strike.x2 = text.x2
                    strike.y2 = strike_y

                    line_group.objects.append(strike)

                # Underline
                if item.style.underline:

                    underline = drawfile.path()
                    underline_y = line_group.y1 - int(
                        (item.font_height * (item.style.underline_offset / 100.0)) * \
                        drawfile.units_per_point / 1000.0
                        )

                    underline.path = \
                    [
                        ('move', (text.x1, underline_y)),
                        ('draw', (text.x2, underline_y)),
                        ('end',)
                    ]

                    # Same colour as for strikeout.
                    colour = item.style.strikeout_colour
                    underline.outline = (0, colour.red, colour.green, colour.blue)
                    underline.fill = (255, 255, 255, 255)
                    underline.width = int(
                        (item.font_height * (item.style.underline_size / 100.0)) * \
                        drawfile.units_per_point / 1000.0
                        )
                    underline.x1 = text.x1
                    underline.y1 = underline_y
                    underline.x2 = text.x2
                    underline.y2 = underline_y

                    line_group.objects.append(underline)

        # Add horizontal rule-offs where relevant.

        if align_style.rule_off_below or align_style.rule_off_above:
        
            # Read any rule-off margins and convert them to Draw units.
            left_end = line_group.x1 + \
                int(align_style.left_rule_off_margin * drawfile.units_per_point / 1000.0)

            right_end = align_style.right_rule_off_margin

            if right_end <= 0.0:

                # Negative right margin offsets indicate measurement from the right hand side
                # of the frame.
                right_end = line_group.x2 + int(right_end * drawfile.units_per_point / 1000.0)

            else:

                # Positive right margin offsets indicate measurement from the left hand side
                # of the frame.
                right_end = line_group.x1 + int(right_end * drawfile.units_per_point / 1000.0)

        # Below the text
        if align_style.rule_off_below == 1 and trailing == 1:

            rule_off = drawfile.path()
            rule_off_y = line_group.y1 - int(
                align_style.rule_off_below_offset * drawfile.units_per_point / 1000.0
                )
            rule_off.path = \
            [
                ('move', (left_end, rule_off_y)),
                ('draw', (right_end, rule_off_y)),
                ('end',)
            ]

            colour = align_style.rule_off_colour
            rule_off.outline = (0, colour.red, colour.green, colour.blue)
            rule_off.fill = (255, 255, 255, 255)
            rule_off.width = int(
                align_style.rule_off_thickness * drawfile.units_per_point / 1000.0
                )
            rule_off.x1 = line_group.x1
            rule_off.y1 = rule_off_y
            rule_off.x2 = line_group.x2
            rule_off.y2 = rule_off_y

            line_group.objects.append(rule_off)

        # Above the text
        if align_style.rule_off_above == 1 and first_line == 1:

            rule_off = drawfile.path()
            rule_off_y = line_group.y2 + int(
                align_style.rule_off_above_offset * drawfile.units_per_point / 1000.0
                )
            rule_off.path = \
            [
                ('move', (left_end, rule_off_y)),
                ('draw', (right_end, rule_off_y)),
                ('end',)
            ]

            colour = align_style.rule_off_colour
            rule_off.outline = (0, colour.red, colour.green, colour.blue)
            rule_off.fill = (255, 255, 255, 255)
            rule_off.width = int(
                align_style.rule_off_thickness * drawfile.units_per_point / 1000.0
                )
            rule_off.x1 = line_group.x1
            rule_off.y1 = rule_off_y
            rule_off.x2 = line_group.x2
            rule_off.y2 = rule_off_y

            line_group.objects.append(rule_off)

        # Vertical rule-offs

        if align_style.tab_defns != []:

            # Read the vertical rule-off width and convert it to Draw units.
            rule_off_width = int(align_style.vertical_rule_off_width * drawfile.units_per_point / 1000.0)

            # Read the rule-off colour and convert it into a form suitable for a drawfile.path object.
            colour = align_style.rule_off_colour
            rule_off_outline = (0, colour.red, colour.green, colour.blue)

        for position, tab_type in align_style.tab_defns:

            if tab_type == 'vertical rule':

                # Create a path for this vertical rule.
                rule_off = drawfile.path()

                rule_off_x = line_group.x1 + int(position * drawfile.units_per_point / 1000.0)

                rule_off.path = \
                [
                    ('move', (rule_off_x, line_group.y2)),
                    ('draw', (rule_off_x, line_group.y1)),
                    ('end',)
                ]
    
                rule_off.outline = rule_off_outline
                rule_off.fill = (255, 255, 255, 255)
                rule_off.width = rule_off_width
                rule_off.x1 = rule_off_x
                rule_off.y1 = line_group.y1
                rule_off.x2 = rule_off_x
                rule_off.y2 = line_group.y2
    
                line_group.objects.append(rule_off)

        # Add the line group to the parent group.
        group.objects.append(line_group)



class Writer:

    # Fonts with an oblique variant available
    oblique_fonts = [
        'Corpus.Bold', 'Corpus.Medium',
        'Frutiger.Black', 'Frutiger.Bold', 'Frutiger.Light', 'Frutiger.Medium',
        'Honerton.Black', 'Homerton.Bold', 'Homerton.Light', 'Homerton.Medium', 
        ]

    # Fonts with an italic variant available
    italic_fonts = [
        'Goudy.OldStyle', 'MathGreek', 'MathPhys',
        'NewHall.Bold', 'NewHall.Medium', 'Pembroke.Bold', 'Pembroke.Medium',
        'Puritan', 'Trinity.Bold', 'Trinity.Medium' 
        ]

    # Fonts with a bold weight available
    bold_fonts = ['Anwen', 'BitFont', 'Corpus', 'Frutiger', 'Goudy.OldStyle', 'Homerton',
                    'NewHall', 'Pembroke', 'Sassoon.Primary', 'Trinity']

    weights = ['Black', 'Bold', 'Light', 'Medium']
    variants = ['Italic', 'Oblique']
    weights_and_variants = weights + variants

    def reset_styles(self):

        # Record the application numbers used as styles are applied.
        self.applied = []

        # The application numbers are used as keys in a dictionary of style instances.
        self.styles = {}

        # The current style class being used.
        self.current_style = UnderBaseStyle

        self.apply_style(0, 0)

    def apply_style(self, counter, style_no):

        style = self.document.styles[style_no]

        self.applied.append(counter)
        self.styles[counter] = style

        # Derive a new class based on the current style.
        if self.current_style is not None:

            # The new style attributes should override the old ones (left to right inheritance lookup).

            class TextStyle(style, self.current_style):

                pass
            
            self.current_style = TextStyle

        else:

            self.current_style = style

        # Determine the font name that the new style should use based on the properties
        # of the font.
        self.current_style.font_name = self.read_style_font()


    def remove_style(self, counter, style_no):

        if style_no >= len(self.document.styles):

            # Index exceeds length of document styles list so find the last style to
            # appear on the style menu.
            i = len(self.document.styles) - 1

            while i >= 0:

                if self.document.styles[i].on_menu == 1:

                    break

                i = i - 1

            style_no = max(0, i)

        style = self.document.styles[style_no]

        # Search the list of applied styles for the given style and remove it.

        i = len(self.applied) - 1

        while i >= 0:

            if self.applied[i] == counter:

                break

            i = i - 1

        self.applied = self.applied[:i] + self.applied[i+1:]

        if counter not in self.applied:

            del self.styles[counter]

    def read_style_attribute(self, name):

        # Return the style attribute by examining the applied styles in
        # reverse order until the relevant information has been found.

        i = len(self.applied) - 1

        while i >= 0:

            counter = self.applied[i]

            if hasattr(self.styles[counter], name):

                return getattr(self.styles[counter], name)

            i = i - 1

        return None

    def read_style_font(self):

        # Determine the font to use on the basis of the underlying font and any effects
        # subsequently applied.

        font_name = self.current_style.font_name
        bold = self.current_style.bold_on
        italic = self.current_style.italic_on

        # Convert the font to a list representation.
        font_list = string.split(font_name, '.')

        # Ensure that the font family and font name remain together.
        i = 0
        while i < len(font_list):

            if font_list[i] in self.weights_and_variants:

                break

            i = i + 1

        font_list = [string.join(font_list[:i], '.')] + font_list[i:]

        # Bold and italic effects can only override fonts without these effects already
        # set.
        if bold == 1:

            if (font_list[0] in self.bold_fonts) and ('Bold' not in font_list):

                # Find the current weight of the font.
                weight_index = None

                for weight in self.weights:

                    if weight in font_list:

                        weight_index = font_list.index(weight)
                        break

                if weight_index is not None:

                    font_list[ weight_index ] = 'Bold'
                    font_list[0:weight_index] = [string.join(font_list[0:weight_index], '.')]

                else:

                    font_list.insert(1, 'Bold')
                    font_list[0:2] = [string.join(font_list[0:2], '.')]

        else:

            # Join all the elements of the font description up to and including the font
            # weight.
            i = 0

            for j in font_list:

                if j in self.weights:

                    break

                i = i + 1

            font_list[0:i+1] = [string.join(font_list[0:i+1], '.')]

        if italic == 1:

            if (font_list[0] in self.italic_fonts) and ('Italic' not in font_list):

                font_list.append('Italic')

            elif (font_list[0] in self.oblique_fonts) and ('Oblique' not in font_list):

                font_list.append('Oblique')

        # Reconstruct the font name to use.
        return string.join(font_list, '.')



class DrawWriter(Writer):

    def __init__(self, infile, outdir, begin_page, end_page, outline, strict, show_all_pictures):

        # Outline flag: if set then show only an outline (no pictures).
        self.outline = outline

        # Show all pictures flag: don't discard pictures which fall outside their frame
        # boundaries.
        self.show_all_pictures = show_all_pictures

        # Open the Impression document.
        try:

            self.document = impression.Document(infile, verbose = 1, strict = strict)

        except impression.ImpressionError:

            raise WriterError, \
                "Could not read %s - it may be a version I don't understand." % infile

        # Read all master pages (temporary solution to referencing master pages
        # by name).
        #self.document.read_master_pages()

        # New paragraph flag
        self.new_para = 1

        # Use a page counter to index the Drawfiles.
        pages = len(self.document.chapter_pages)
        digits = len(str(pages))
        format = '%%0%ii' % digits

        # Examine each page.

        begin_page = min(max(0, begin_page - 1), len(self.document.chapter_pages) - 1)

        if end_page == -1: end_page = len(self.document.chapter_pages)
        
        end_page = min(max(1, end_page, begin_page + 1), len(self.document.chapter_pages))

        print
        print 'Writing pages:'
        print

        for page in range(begin_page, end_page):

            # Keep a record of which styles we have applied in this page.
            self.reset_styles()
    
            # Reset the list of fonts in use.
            self.fonts = []
    
            # Record any fonts used in the page (including the default font).
            self.record_font_use()
    
            self.write_content(outdir + os.sep + format % (page + 1), self.document.chapter_pages[page])


    def write_content(self, path, page):

        print path

        try:

            f = open(path, 'wb')

        except IOError:

            print 'Failed to open %s for writing the page.' % path
            return

        # Create a Drawfile.

        # Use lazy interpretation of sprites to avoid internal sprite handling.
        d = drawfile.drawfile(lazy = 1)

        # Maintain a list of frame backgrounds in the order in which they were created
        # which will be added to the beginning of the Drawfile's list of objects after
        # all the other objects have been found.
        backgrounds = []

        # Find the page dimensions (not including the page bleed).
        page_width = page.margins.width() - (2 * page.bleed)
        page_height = page.margins.height() - (2 * page.bleed)

        # Examine each frame.
        for frame in page.frames:

            # Draw frame outlines.

            # Determine the frame's bounding box relative to the page origin.
            frame_bbox = frame.inner_bbox.relative_to(page.origin)

            # Employ a nasty hack to avoid dealing with frames off the page (we jump to the next item in the loop).
            if frame_bbox.xmin > page_width: continue
            if frame_bbox.xmax < 0: continue
            if frame_bbox.ymin > 0: continue
            if frame_bbox.ymax < -page_height: continue

            c = impression_to_draw(
                [ frame_bbox.xmin, frame_bbox.ymin, frame_bbox.xmax, frame_bbox.ymax ],
                page_height
                )

            frame_bg = drawfile.path()
            frame_bg.path = [
                ('move', (c[0], c[1])), ('draw', (c[2], c[1])),
                ('draw', (c[2], c[3])), ('draw', (c[0], c[3])),
                ('close',), ('end',)
                ]

            # 00 rr gg bb
            frame_bg.outline = (255, 255, 255, 255)

            frame_bg.x1 = c[0]
            frame_bg.y1 = c[1]
            frame_bg.x2 = c[2]
            frame_bg.y2 = c[3]

            if hasattr(frame, 'colour'):

                if frame.colour.trans == 0:

                    frame_bg.fill = (
                        0, frame.colour.red, frame.colour.green, frame.colour.blue
                        )

                else:

                    frame_bg.fill = (255, 255, 255, 255)

            # Add the frame backgrounds to a separate list, to be added to the Drawfile
            # later.
            backgrounds.append(frame_bg)

            # Border styles

            border_objects = self.draw_borders(page, frame, frame_bbox, c)

            # Add the border objects to the Drawfile.
            d.objects = d.objects + border_objects

            if hasattr(frame, 'content_ref'):

                # If there is content associated with this frame then place it within a
                # group object.

                group = drawfile.group()
                group.x1 = c[0]
                group.y1 = c[1]
                group.x2 = c[2]
                group.y2 = c[3]
                group.name = 'Frame group'

                # Find content.
                content = self.document.content[ frame.content_ref ]
        
                if (frame.data_type & 0x07) == 3 and self.outline == 0 and \
                    content.content_type == 'Draw':

                    self.write_frame_picture(content, group, page, frame, c)

                else:

                    self.write_frame_text(
                        content, group, page, page_width, page_height, frame, frame_bbox
                        )

                # Add the group to the Drawfile's list of objects.
                d.objects.append(group)

        # Add the frame background objects to the Drawfile.
        d.objects = backgrounds + d.objects

        # Add a font table to the Drawfile's list of objects containing all the references
        # collected while reading the Impression document.
        ft = drawfile.font_table()

        n = 1
        for font in self.fonts:

            ft[n] = font
            n = n + 1

        d.objects.insert(0, ft)

        # Give the Drawfile a bounding box corresponding to the page dimensions.
        d.x1 = 0
        d.y1 = 0
        d.x2 = int( (page_width / 1000.0) * drawfile.units_per_point )
        d.y2 = int( (page_height / 1000.0) * drawfile.units_per_point )

        # Write the Drawfile.
        d.write(f)

        # Close the file.
        f.close()


    def write_frame_picture(self, content, group, page, frame, coords):

        # Obtain information about the orientation of the picture within the frame.
        xs, ys = frame.x_scale, frame.y_scale
        dx = frame.x_displacement * drawfile.units_per_point / 1000.0
        dy = frame.y_displacement * drawfile.units_per_point / 1000.0
        angle = frame.angle

        #print 'Picture'
        #print
        #print 'x scale: %.3f%%' % xs
        #print 'y scale: %.3f%%' % ys
        #print 'x displacement: %.3f mm' % (dx * 25.4 / 72000.0)
        #print 'y displacement: %.3f mm' % (dy * 25.4 / 72000.0)
        #print 'angle: %.3f degrees' % angle
        #print
        #print 'Extracting content...',

        # Take the content and transform it.
        df = cStringIO.StringIO(content.data)
        d = drawfile.drawfile(df)

        # Scale the content first.
        for object in d.objects:

            drawtrans.stretch(object, (xs / 100.0, ys / 100.0), origin = (0, 0))

        if angle != 0.0:

            # Rotate the content about the centre of the picture.
            for object in d.objects:

                if object.__class__ in self.drawfile_render:
                    drawtrans.rotate(object, angle, origin = (0, 0))

        # Translate the content relative to the bottom left of the frame then displace it.

        for object in d.objects:

            if object.__class__ in self.drawfile_render:
                drawtrans.translate( object, (coords[0] + dx, coords[1] + dy) )

        # If any of the objects fall outside the frame bounding box then don't display them at all.

        #print coords

        kept, font_mappings = self.discard_picture_objects(d.objects, coords)

        # If any font mappings were generated then modify any affected objects to use the
        # new font numbers.

        for object in kept:

            drawtrans.update_fonts(object, font_mappings)

        # Add the kept objects to the group objects.
        group.objects = group.objects + kept


    def discard_picture_objects(self, objects, coords):

        # Keep a record of any fonts used in the picture and create a mapping between font
        # numbers in the picture and font numbers in the page.
        font_mappings = {}
        kept = []

        for object in objects:

            # These objects can't be positioned so are discarded.
            if isinstance(object, drawfile.font_table):

                # The font table may contain entries which are necessary for the correct display
                # of objects in this picture, so the entries are extracted and added to the page's
                # font table for later reference.
                for number, name in object.font_table.items():

                    try:
                        new_index = self.fonts.index(name)

                    except ValueError:

                        new_index = len(self.fonts)
                        self.fonts.append(name)

                    # Create a mapping between the old font number and the new one, remembering
                    # that the new number will be one greater than the index of the font name in the
                    # list.
                    font_mappings[number] = new_index + 1

                continue

            elif isinstance(object, drawfile.options):

                continue

            elif isinstance(object, drawfile.group):

                group_kept, group_font_mappings = \
                    self.discard_picture_objects(object.objects, coords)

                # Add the new font_mappings to the list maintained here.
                for key, value in group_font_mappings.items():

                    font_mappings[key] = value

                # Add these objects to the list of kept objects.
                kept = kept + group_kept

            else:

                #print object.x1, object.y1, object.x2, object.y2

                if self.show_all_pictures == 0:

                    if object.x1 < coords[0]: continue
                    if object.y1 < coords[1]: continue
                    if object.x2 > coords[2]: continue
                    if object.y2 > coords[3]: continue

                # Add the object to the group.
                kept.append(object)

        #print '%i objects kept' % len(kept)

        return kept, font_mappings

    def write_frame_text(self, content, group, page, page_width, page_height, frame, frame_bbox):

        # Assume that the frame does not begin with a new paragraph.
        new_para = 0

        # Only the first piece uses the first line flag stored in the text.
        first_piece = 1

        for piece in content.pieces:

            if isinstance(piece, impression.TextSetup):

                self.reset_styles()
                            
                for i in piece.text:

                    # A sequence of words, beginning with a command
                    
                    if i[0] == 'apply style':

                        # Apply each style.

                        for counter, style in i[1:]:

                            self.apply_style(counter, style)

                # Modify the text style.
                self.record_font_use()

            elif isinstance(piece, impression.Text):

                #print piece
                # Construct a bounding box from the frame and line information.

                ymin = piece.lineinfo.ymin
                ymax = piece.lineinfo.ymax

                #print ymin, page.margins.ymax, self.document.master_pages[page.number].margins.ymax

                #if self.document.in_master_page(frame.content_ref):
                #
                #    master_page = self.document.master_pages[page.number]
                #
                #    #if ymin < master_page.origin[1] and not frame.local_content:
                #    #if not frame.local_content:
                #
                #    # The line position needs to be found relative to the page origin.
                #    ymin = ymin - self.document.master_pages[page.number].origin[1]
                #    ymax = ymax - self.document.master_pages[page.number].origin[1]

                #while ymin < page.margins.ymin:
                #
                #    ymin = ymin + page.margins.height()
                #    ymax = ymax + page.margins.height()

                # The line position needs to be found relative to the page origin.
                #else: # elif ymin < page.origin[1]:

                #if ymin < page.origin[1] and not frame.local_content:
                #if not frame.local_content:
                if not self.document.in_master_page(frame.content_ref):

                    if ymin < page.margins.ymax and ymax > page.margins.ymin:

                        # The line position needs to be found relative to the page origin
                        # to "normalise" its position on the page.
                        ymin = ymin - page.origin[1]
                        ymax = ymax - page.origin[1]
    
                        #if self.document.version < 28: # 0x1c
                        if self.document.old_format:
    
                            ymin = ymin + self.document.chapter_pages[0].origin[1]
                            ymax = ymax + self.document.chapter_pages[0].origin[1]

                    else:

                        # The content lies outside the chapter page margins.

                        # Displace the content until it lies within the "normalised"
                        # page margins.
    
                        normal_top = self.document.chapter_pages[0].origin[1]
                        normal_base = normal_top - page.margins.height()
    
                        while ymin < normal_base and ymax < normal_base:
    
                            # Displace the content upward by the page height plus the vertical
                            # origin "offset".
                            ymin = ymin - normal_base
                            ymax = ymax - normal_base

                else:

                    if ymin < self.document.master_pages[page.number].margins.ymax and \
                        ymax > self.document.master_pages[page.number].margins.ymin:

                        # The line position needs to be found relative to the page origin
                        # to "normalise" its position on the page.
                        ymin = ymin - self.document.master_pages[page.number].origin[1]
                        ymax = ymax - self.document.master_pages[page.number].origin[1]
    
                        #if self.document.old_format:
                        if self.document.old_format and self.document.version < 28: # 0x1c
    
                            ymin = ymin + self.document.chapter_pages[0].origin[1]
                            ymax = ymax + self.document.chapter_pages[0].origin[1]

                    else:

                        # The content lies outside the master page margins.

                        # Displace the content until it lies within the "normalised"
                        # page margins.
    
                        normal_top = self.document.master_pages[0].origin[1]
                        normal_base = normal_top - \
                            self.document.master_pages[page.number].margins.height()
    
                        while ymin < normal_base and ymax < normal_base:
    
                            # Displace the content upward by the page height plus the vertical
                            # origin "offset".
                            ymin = ymin - normal_base
                            ymax = ymax - normal_base


                # The baseline is specified as a downward displacement from the top of the
                # line bounding box.
                baseline = piece.lineinfo.baseline

                coords = [
                    frame_bbox.xmin, ymin, frame_bbox.xmax, ymax
                    ]

                # Record whether this is the first line in a new paragraph.
                if first_piece == 1:

                    first_line = piece.first_line
                    first_piece = 0

                else:

                    first_line = new_para # or piece.first_line # self.new_para

                # Also note whether this is a trailing line in a paragraph.
                trailing = piece.trailing

                # If this is a trailing line then the next line will begin a new paragraph.
                new_para = trailing

                #print
                #print first_line, trailing

                # Begin a new line of text to use.
                line = Line(coords, frame.inset_h)

                for i in piece.text:

                    if type(i) == types.StringType:

                        # Split the text up if it contains spaces.
                        words = string.split(clean_string(i), ' ')

                        for word in words[:-1]:

                            if word != '':

                                line.add_word( Text(word, ymin, ymax, self.current_style) )

                            # Add a space between each word.
                            line.add_word( Space(self.current_style) )

                        # The last word should be written without a following space.
                        if words[-1] != '':

                            line.add_word( Text(words[-1], ymin, ymax, self.current_style) )

                        if '\r' in i:

                            # A newline was encountered. This shouldn't occur mid-line but
                            # a new line object can be created if necessary as it will not
                            # be written if it is not used.
                            if line.empty == 0:

                                # Add the line to the list of objects in the group.
                                line.write_line(
                                    group, self, page_height, baseline, first_line, trailing
                                    )

                            # Following text is part of a new paragraph.
                            first_line = 1
                            new_para = 1

                            # Begin a new line, just in case more text follows on this "line".
                            line = Line(coords, frame.inset_h)

                        else:

                            # Plain text
                            new_para = 0

                    else:

                        # A sequence of words, beginning with a command
                        if i[0] == 'apply style':

                            self.reset_styles()
                            
                            # Apply each style listed only if its application number
                            # (counter) has not been recorded before.

                            for counter, style in i[2:]:

                                #if counter not in self.applied:

                                self.apply_style(counter, style)

                            # Modify the text style.
                            self.record_font_use()

                        elif i[0] == 'horizontal space':

                            line.add_word( HSpace(i[1], self.current_style) )

                        elif i[0] == 'kern':

                            # Ignore the first two values and interpret the final two as displacements
                            # in millipoints.
                            h_kern = i[3]
                            v_kern = i[4]

                            # Only vertical displacement is possible in text areas so ignore horizontal
                            # kerning.
                            line.add_word( Kern(h_kern, v_kern, self.current_style) )

                        elif i[0] == 'margins':

                            # Override the styles on the line by explicitly specifying the
                            # margins.
                            line.left_margin = i[1]
                            line.right_margin = i[2]                            

                        elif i[0] == 'page number':

                            if hasattr(page, "page"):
                            
                                word = self.write_number(page.page, i[1])
                                
                                line.add_word( Text(word, ymin, ymax, self.current_style) )

                        elif i[0] == 'embedded frame':

                            # Just add horizontal space.
                            line.add_word( HSpace(i[5], self.current_style) )

                        elif i[0] == 'special character':

                            # Add a special character to the text.
                            line.add_word( Text(i[1], ymin, ymax, self.current_style) )

                        elif i[0] == 'line width':

                            # Redefine the line margins based on this value.
                            line.left_margin = i[2]
                            line.right_margin = i[1]

                # Add the line to the list of objects in the group, if appropriate.
                if line.empty == 0:

                    line.write_line(group, self, page_height, baseline, first_line, trailing)


    def record_font_use(self):

        # Determine the font name and size of the text.
        #font_name = self.read_style_font()
        font_name = self.current_style.font_name
        font_size = self.current_style.font_size / 1000.0

        # Check whether this combination already exists.
        if font_name not in self.fonts:

            # Add the new font style to the font declarations.
            self.fonts.append(font_name)


    def write_number(self, number, style):

        if style == 'arabic':

            return '%i' % number

        else:

            # Roman numerals.
            s = ''

            # Find the number of thousands first.
            thousands = int(number / 1000)
            s = s + ('M' * thousands)

            number = number - (thousands * 1000)

            # If the result is greater than five hundred then add the relevant symbols.
            if number > 500:

                if number < 800:

                    s = s + 'D'
                    number = number - 500

                # Determine how many hundreds can be taken away from a thousand to
                # produce a number smaller than the one we have.
                elif number < 900:

                    s = s + 'CCM'
                    number = number - 800

                else:

                    s = s + 'CM'
                    number = number - 900

            # Find the number of hundreds remaining.
            hundreds = int(number / 100)
            s = s + ('C' * hundreds)

            number = number - (hundreds * 100)

            # If the result is greater than fifty then add the relevant symbol.
            if number > 50:

                if number < 80:

                    s = s + 'L'
                    number = number - 50

                # Determine how many tens can be taken away from a hundred to
                # produce a number smaller than the one we have.
                elif number < 90:

                    s = s + 'XXC'
                    number = number - 80

                else:

                    s = s + 'XC'
                    number = number - 90

            # Find the number of tens remaining.
            tens = int(number / 10)
            s = s + ('M' * tens)

            # If the result is greater than five then add the relevant symbols.
            if number > 5:

                if number < 8:

                    s = s + 'V'
                    number = number - 5

                # Determine how many ones can be taken away from a ten to
                # produce a number smaller than the one we have.
                elif number < 9:

                    s = s + 'IIX'
                    number = number - 8

                else:

                    s = s + 'IX'
                    number = number - 9

            # Find the number of ones remaining.
            s = s + ('I' * number)

        if style == 'roman':

            return string.lower(s)

        else:

            return s


    # Define the types of Draw object which can be rendered in a border.
    drawfile_render = \
    (
        drawfile.text, drawfile.path, drawfile.sprite, drawfile.group
    )

    def draw_borders(self, page, frame, frame_bbox, coords):

        # Determine which borders are present.

        corners = {'top left': {}, 'top right': {}, 'bottom left': {}, 'bottom right': {}}
        borders_used = {}

        # The actual border number is one greater than the number given but the number
        # is effectively an offset into the document's border list. However, offsets
        # 1-10 (offsets 0-9) are usually not declared and are internal to Impression.
        # Border 11 (offset 10) is usually found as the first border in documents.

        objects = []

        for border, number in frame.borders.items():

            if border == 'top' and number != 0xff:

                if borders_used.has_key(number):

                    edge, corner = borders_used[number]

                else:

                    edge, corner = self.find_border(number)

                    borders_used[number] = (edge, corner)

                if corner is not None:

                    if number not in corners['top left'].keys():

                        # Add the border number to the corner dictionary to avoid duplication.
                        corners['top left'][number] = corner

                    if number not in corners['top right'].keys():

                        # Add the border number to the corner dictionary to avoid duplication.
                        corners['top right'][number] = corner

                if edge is not None and (edge.y2 > edge.y1):

                    # Ensure that the bounding box only encompasses the points and does not
                    # include space due to line widths.
                    edge = drawtrans.stretch(edge, (1, 1), copy_object = 1)

                    # Determine the stretch factor in advance.
                    stretch = (1, float(coords[2] - coords[0]) / (edge.y2 - edge.y1))

                    # Stretch the edge piece.
                    edge = drawtrans.stretch(edge, stretch, copy_object = 1)
    
                    # Rotate the edge piece.
                    drawtrans.rotate(edge, 270)
    
                    # Translate the edge piece to align its lower side with the top side of the
                    # inner frame.
                    drawtrans.translate(
                        edge, (coords[0] - edge.x1, coords[3] - edge.y1)
                        )

                    # Add the object to the list of border objects to be added to the output.
                    objects.append(edge)

            elif border == 'left' and number != 0xff:

                if borders_used.has_key(number):

                    edge, corner = borders_used[number]

                else:

                    edge, corner = self.find_border(number)

                    borders_used[number] = (edge, corner)

                if corner is not None:

                    if number not in corners['top left'].keys():

                        # Add the border number to the corner dictionary to avoid duplication.
                        corners['top left'][number] = corner

                    if number not in corners['bottom left'].keys():

                        # Add the border number to the corner dictionary to avoid duplication.
                        corners['bottom left'][number] = corner

                if edge is not None and (edge.y2 > edge.y1):

                    # Ensure that the bounding box only encompasses the points and does not
                    # include space due to line widths.
                    edge = drawtrans.stretch(edge, (1, 1), copy_object = 1)

                    # Determine the stretch factor in advance.
                    stretch = (1, float(coords[3] - coords[1]) / (edge.y2 - edge.y1))

                    # Stretch the edge piece.
                    edge = drawtrans.stretch(edge, stretch, copy_object = 1)
    
                    # Translate the edge piece.
                    drawtrans.translate(
                        edge, (coords[0] - edge.x2, coords[1] - edge.y1)
                        )

                    # Add the object to the list of border objects to be added to the output.
                    objects.append(edge)

            elif border == 'bottom' and number != 0xff:

                if borders_used.has_key(number):

                    edge, corner = borders_used[number]

                else:

                    edge, corner = self.find_border(number)

                    borders_used[number] = (edge, corner)

                if corner is not None:

                    if number not in corners['bottom right'].keys():

                        # Add the border number to the corner dictionary to avoid duplication.
                        corners['bottom right'][number] = corner

                    if number not in corners['bottom left'].keys():

                        # Add the border number to the corner dictionary to avoid duplication.
                        corners['bottom left'][number] = corner

                if edge is not None and (edge.y2 > edge.y1):

                    # Ensure that the bounding box only encompasses the points and does not
                    # include space due to line widths.
                    edge = drawtrans.stretch(edge, (1, 1), copy_object = 1)

                    # Determine the stretch factor in advance.
                    stretch = (1, float(coords[2] - coords[0]) / (edge.y2 - edge.y1))

                    # Stretch the edge piece.
                    edge = drawtrans.stretch(edge, stretch, copy_object = 1)
    
                    # Rotate the edge piece.
                    drawtrans.rotate(edge, 90)
    
                    # Translate the edge piece.
                    drawtrans.translate(
                        edge, (coords[0] - edge.x1, coords[1] - edge.y2)
                        )

                    # Add the object to the list of border objects to be added to the output.
                    objects.append(edge)

            elif border == 'right' and number != 0xff:

                if borders_used.has_key(number):

                    edge, corner = borders_used[number]

                else:

                    edge, corner = self.find_border(number)

                    borders_used[number] = (edge, corner)

                if corner is not None:

                    if number not in corners['bottom right'].keys():

                        # Add the border number to the corner dictionary to avoid duplication.
                        corners['bottom right'][number] = corner

                    if number not in corners['top right'].keys():

                        # Add the border number to the corner dictionary to avoid duplication.
                        corners['top right'][number] = corner

                if edge is not None and (edge.y2 > edge.y1):

                    # Ensure that the bounding box only encompasses the points and does not
                    # include space due to line widths.
                    edge = drawtrans.stretch(edge, (1, 1), copy_object = 1)

                    # Determine the stretch factor in advance.
                    stretch = (1, float(coords[3] - coords[1]) / (edge.y2 - edge.y1))

                    # Stretch the edge piece.
                    edge = drawtrans.stretch(edge, stretch, copy_object = 1)
    
                    # Rotate the edge piece.
                    drawtrans.rotate(edge, 180)
    
                    # Translate the edge piece.
                    drawtrans.translate(
                        edge, (coords[2] - edge.x1, coords[1] - edge.y1)
                        )

                    # Add the object to the list of border objects to be added to the output.
                    objects.append(edge)

        # Place all the corner pieces to be rendered.

        for piece in corners['top left'].values():

            # Copy the corner piece and position it so that the bottom right corner
            # coincides with the top left corner of the inner frame.
            corner = drawtrans.translate(
                piece, ( coords[0] - piece.x2, coords[3] -piece.y1 ),
                copy_object = 1
                )

            # Add the object to the list to be rendered.
            objects.append(corner)

        for piece in corners['top right'].values():

            # Copy the corner piece and rotate it about its lower left point.
            corner = drawtrans.rotate(piece, 270, copy_object = 1)

            # Position the second corner piece so that the bottom left corner of it
            # coincides with the top right corner of the inner frame.
            drawtrans.translate(
                corner, ( coords[2] - corner.x1, coords[3] - corner.y1 )
                )

            # Add the object to the list to be rendered.
            objects.append(corner)

        for piece in corners['bottom left'].values():

            # Copy the corner piece and rotate it about its lower left point.
            corner = drawtrans.rotate(piece, 90, copy_object = 1)

            # Position the second corner piece.
            drawtrans.translate(
                corner, ( coords[0] - corner.x2, coords[1] - corner.y2 )
                )

            # Add the object to the list to be rendered.
            objects.append(corner)

        for piece in corners['bottom right'].values():

            # Copy the corner piece and rotate it about its lower left point.
            corner = drawtrans.rotate(piece, 180, copy_object = 1)
            
            # Copy the corner piece and position it.
            drawtrans.translate(
                corner, ( coords[2] - corner.x1, coords[1] - corner.y2 )
                )

            # Add the object to the list to be rendered.
            objects.append(corner)


        # Recolour the objects, if necessary.
        if hasattr(frame, 'border_colour') and (frame.border_colour.trans == 0):

            colour = (
                0,
                frame.border_colour.red,
                frame.border_colour.green,
                frame.border_colour.blue
                )

            for object in objects:

                drawtrans.recolour(object, colour)


        if objects != []:

            # Return a group object to be rendered.
            group = drawfile.group()
            group.objects = objects
            group.name = 'Frame borders'

            group.x1, group.y1, group.x2, group.y2 = drawtrans.find_bbox(group.objects)

            objects = [group]

        # Return the objects list, whether empty or containing a group object.
        return objects        

    def find_border(self, number):

        if number <= 9:

            # Internal Impression border: use a substitute.
            edge, corner = self.create_default_border_pieces(number)

        else:

            # Try to find the border in the file.
            try:
                border = self.document.borders[number - 10]

            except ImpressionError:

                border = None

            if border is not None:

                # Create/extract the pieces to use for the edge and corners.
                edge, corner = self.create_border_pieces(border)

        # Return the pieces found.
        return edge, corner

    def create_border_pieces(self, border):

        # Create a Drawfile from the border data.
        f = cStringIO.StringIO(border.data)
        d = drawfile.drawfile(f)

        # Extract the first two rendered objects in the Drawfile.
        objects = []
        for obj in d.objects:

            if obj.__class__ in self.drawfile_render:

                # Add this to the list of objects to use.
                objects.append(obj)

                # Return the pieces to use for the edges and corners of the border.
                if len(objects) == 2:
                    return objects

        return None, None

    def create_default_border_pieces(self, number):

        # Create a default border piece depending on the border number.
        edge = drawfile.path()
        edge.outline = (0, 0, 0, 0)
        edge.fill = (255, 255, 255, 255)
        edge.path = [('move', (0, 0)), ('draw', (0, 100)), ('end',)]
        edge.x1, edge.y1 = 0, 0
        edge.x2, edge.y2 = 0, 100

        #corner = drawfile.path()
        #corner.outline = (255, 255, 255, 255)
        #corner.path = [('move', (0, 0)), ('end',)]
        corner = None

        return edge, corner



def find_switch(l, switch, args = 0):

    j = -1

    for i in range(0, len(l)):
        if l[i] == switch:
            if args > 0:
                if i < len(l)-1:
                    # Switch was before the end
                    if args == 1:
                        return l[i], l[i+1]
                    else:
                        return l[i], l[i+1:i+1+args]
                else:
                    return l[i], None
            else:
                return l[i], None

        elif l[i][:len(switch)] == switch:
            # Switch is the first part of an item in the list then return
            # the item split into the switch and the argument. Should only
            # occur for single argument switches. 
            return l[i][:len(switch)], l[i][len(switch):]

    return None, None


# Functions from arguments.py - (C) David Boddie (Thu 01st February 2001)

def remove_switch(l, switch, args = 0):

    argv = []

    i = 0
    while i < len(l):

        if l[i] == switch:
            i = i + args + 1
        elif l[i][:len(switch)] == switch:
            i = i + args
        else:
            argv.append(l[i])
            i = i + 1

    return argv

if __name__ == '__main__':

    # Check the arguments.
    if len(sys.argv) < 3:

        print 'Usage: impdraw.py [-b <begin at page>] [-e <end at page>] [--outline] ' + \
            '[--lenient] [--show-all-pictures] <Impression file> <Output directory>'
        sys.exit()

    argv = sys.argv[1:]

    # Look for beginning and ending pages.
    begin, args = find_switch(argv, '-b', 1)

    if begin is not None:

        try:
            begin_page = int(args)
        except ValueError:
            print '%s is not a valid beginning page number.' % args
            sys.exit()

        argv = remove_switch(argv, '-b', 1)

    else:

        begin_page = 0

    end, args = find_switch(argv, '-e', 1)

    if end is not None:

        try:
            end_page = int(args)
        except ValueError:
            print '%s is not a valid ending page number.' % args
            sys.exit()

        argv = remove_switch(argv, '-e', 1)

    else:

        end_page = -1

    outline, args = find_switch(argv, '--outline', 0)

    if outline is not None:

        argv = remove_switch(argv, '--outline', 0)

    lenient, args = find_switch(argv, '--lenient', 0)

    if lenient is not None:

        argv = remove_switch(argv, '--lenient', 0)

    show_all_pictures, args = find_switch(argv, '--show-all-pictures', 0)

    if show_all_pictures is not None:

        argv = remove_switch(argv, '--show-all-pictures', 0)

    # Read the input file and output directory details.
    infile = argv[0]
    outdir = argv[1]

    # Check that the output directory exists.

    # Create the output directory, if necessary.
    if not os.path.exists(outdir):

        os.mkdir(outdir)

    elif not os.path.isdir(outdir):

        print '%s already exists and is not a directory.' % outdir
        sys.exit()

    # Instantiate a writer object.
    try:

        writer = DrawWriter(
            infile, outdir, begin_page, end_page, outline != None, lenient == None,
            show_all_pictures != None
            )

    except WriterError:

        e_type, e_value, e_traceback = sys.exc_info()
        output = filter(lambda x: x != None, e_value.args)
        print string.join(output, '\n')
        sys.exit()

    # Exit
    sys.exit()
