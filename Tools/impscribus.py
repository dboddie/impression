#! /usr/bin/env python
#
# impscribus.py - a utility for converting Computer Concepts Impression
#                 documents to Scribus XML files
#
# Copyright (C) 2005-2010 David Boddie <david@boddie.org.uk>
# 
# This file is part of the Impression package.
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301, USA.

"""
impscribus.py

Write the contents of an Impression document to an Scribus file.
"""

import os, string, sys, types
from xml.dom import minidom

from Impression import *
from cmdsyntax import Syntax


class WriterError(Exception):

    pass


def clean_string(s):

    new = ''

    for i in s:

        if ord(i) >= 32:

            new = new + i

    return new



# A style to provide the default style attributes which the base style in many documents fails to provide.

class UnderBaseStyle:

    background_color = (255, 255, 255, 255)


class Text:

    def __init__(self, text, style):

        self.text = text
        self.style = style

        font_name, font_h = style.font_name, style.font_size
        font_w = font_h * (style.aspect_ratio / 100.0)

        self.font = font_name
        self.font_width = font_w
        self.font_height = font_h

        # Create a font instance.
        try:
            self.font = fonts.Font(font_name, font_w, font_h)

        except fonts.FontError:

            # It would be useful to introduce a fallback option to cover this
            # situation.
            print "Warning: No support for fonts."

    def width(self):

        return self.font.string_width(self.text)
    
    def toxml(self, document, parent, colours = None):
    
        text_element = document.createElement("ITEXT")
        align = self.style.justify_list.index(self.style.justification)
        text_element.setAttribute("CAB", str(align))
        text_element.setAttribute("CH", self.text)
        text_element.setAttribute("CEXTRA", "0")
        text_element.setAttribute("CFONT", self.style.font_name)
        
        if hasattr(self.style, "foreground_colour") and colours is not None:
        
            colour = (
                self.style.foreground_colour.red,
                self.style.foreground_colour.green,
                self.style.foreground_colour.blue
                )
            if not colours.has_key(colour):
                colours[colour] = "Color%s" % ("%02x%02x%02x" % colour)
            
            text_element.setAttribute("CCOLOR", colours[colour])
        
        else:
        
            text_element.setAttribute("CCOLOR", "Black")
        
        text_element.setAttribute("CSHADE", "100")
        text_element.setAttribute("CSIZE", str(self.font_width/1000.0))
        
        style = 0
        if hasattr(self.style, "superscript") and self.style.superscript:
            style = style | 1
        if hasattr(self.style, "subscript") and self.style.subscript:
            style = style | 2
        if hasattr(self.style, "underline") and self.style.underline:
            style = style | 8
        if hasattr(self.style, "strikeout") and self.style.strikeout:
            style = style | 16
        text_element.setAttribute("CSTYLE", str(style))
        
        parent.appendChild(text_element)


class Space(Text):

    def __init__(self, style):

        Text.__init__(self, ' ', style)

class HSpace:

    def __init__(self, value, style):

        self.value = value
        self.style = style

    def width(self):

        return self.value
    
    def toxml(self, document, parent):
    
        pass

class Kern:

    def __init__(self, hkern, vkern, style):

        self.hkern = hkern
        self.vkern = vkern
        self.style = style

    def width(self):

        return self.hkern
    
    def toxml(self, document, parent):
    
        pass


class Writer:

    def reset_styles(self):

        # Record the application numbers used as styles are applied.
        self.applied = []

        # The application numbers are used as keys in a dictionary of style
        # instances.
        self.styles = {}

        # Maintain a list of used styles
        self.used_styles = []
        
        # The current style class being used.
        self.current_style = UnderBaseStyle

        self.apply_style(0, 0)

    def apply_style(self, counter, style_no):

        style = self.document.styles[style_no]

        self.applied.append(counter)
        self.styles[counter] = style

        # Derive a new class based on the current style.
        if self.current_style is not None:

            # The new style attributes should override the old ones (left to right inheritance lookup).

            class TextStyle(style, self.current_style):

                pass
            
            self.current_style = TextStyle

        else:

            self.current_style = style


    def remove_style(self, counter, style_no):

        if style_no >= len(self.document.styles):

            # Index exceeds length of document styles list so find the last style to
            # appear on the style menu.
            i = len(self.document.styles) - 1

            while i >= 0:

                if self.document.styles[i].on_menu == 1:

                    break

                i = i - 1

            style_no = max(0, i)

        style = self.document.styles[style_no]

        # Search the list of applied styles for the given style and remove it.

        i = len(self.applied) - 1

        while i >= 0:

            if self.applied[i] == counter:

                break

            i = i - 1

        self.applied = self.applied[:i] + self.applied[i+1:]

        if counter not in self.applied:

            del self.styles[counter]

    def read_style_attribute(self, name):

        # Return the style attribute by examining the applied styles in
        # reverse order until the relevant information has been found.

        i = len(self.applied) - 1

        while i >= 0:

            counter = self.applied[i]

            if hasattr(self.styles[counter], name):

                return getattr(self.styles[counter], name)

            i = i - 1

        return None



class ScribusWriter(Writer):

    def __init__(self, infile, strict):

        # Open the Impression document.
        try:

            self.document = impression.Document(
                infile, verbose = 1, strict = strict
                )

        except impression.ImpressionError:

            raise WriterError, \
                "Could not read %s - it may be a version I don't understand." % infile
    
    def write(self, outfile, begin_page, end_page, outline, show_all_pictures):
    
        # Outline flag: if set then show only an outline (no pictures).
        self.outline = outline

        # Show all pictures flag: don't discard pictures which fall outside their frame
        # boundaries.
        self.show_all_pictures = show_all_pictures

        # Read all master pages (temporary solution to referencing master pages
        # by name).
        #self.document.read_master_pages()

        # New paragraph flag
        self.new_para = 1

        # Use a page counter to index the files.
        pages = len(self.document.chapter_pages)
        digits = len(str(pages))
        format = '%%0%ii' % digits

        # Examine each page.

        begin_page = min(max(0, begin_page - 1), len(self.document.chapter_pages) - 1)

        if end_page == -1: end_page = len(self.document.chapter_pages)
        
        end_page = min(max(1, end_page, begin_page + 1), len(self.document.chapter_pages))

        print
        print 'Writing file: %s' % outfile
        print

        try:

            f = open(outfile, 'wb')

        except IOError:

            print 'Failed to open %s for writing the document.' % outfile
            return

        self.colours = {}
        self.standard_colours = []
        
        self.scribus_document = minidom.Document()
        
        scribus_element = self.scribus_document.createElement("SCRIBUSUTF8")
        scribus_element.setAttribute("Version", "1.2.1")
        self.scribus_document.appendChild(scribus_element)
        
        document = self.scribus_document.createElement("DOCUMENT")
        self.write_document_attributes(document)
        scribus_element.appendChild(document)
        
        first_colour_element = self.create_standard_colours(document)
        
        layers = self.scribus_document.createElement("LAYERS")
        layers.setAttribute("DRUCKEN", "1")
        layers.setAttribute("LEVEL", "0")
        layers.setAttribute("NAME", "Background")
        layers.setAttribute("NUMMER", "0")
        layers.setAttribute("SICHTBAR", "1")
        document.appendChild(layers)
        
        # Keep a record of which styles we have applied.
        self.reset_styles()
        
        # Reset the list of fonts in use.
        self.fonts = []
        
        for page in self.document.master_pages:
        
            # Record any fonts used in the page (including the default font).
            self.record_font_use()
            
            self.write_page(document, page, master = 1)
        
        for page in range(begin_page, end_page):

            print page,
            
            # Reset the list of fonts in use.
            self.fonts = []
    
            # Record any fonts used in the page (including the default font).
            self.record_font_use()
    
            self.write_page(document, self.document.chapter_pages[page])
        
        print
        #styles = self.write_styles(document)
        self.insert_colours(document, first_colour_element)
        
        # Write the XML file.
        f.write(self.scribus_document.toprettyxml(indent = " "))
        
        # Close the file.
        f.close()
    
    def create_standard_colours(self, document):
    
        black = self.scribus_document.createElement("COLOR")
        black.setAttribute("NAME", "Black")
        black.setAttribute("RGB", "#000000")
        black.setAttribute("CMYK", "#000000ff")
        document.appendChild(black)
        
        self.standard_colours.append((0, 0, 0))
        self.colours[(0, 0, 0)] = "Black"
        return black
    
    def write_styles(self, document):
    
        for style in self.document.styles.keys():
        
            style_element = self.scribus_document.createElement("STYLE")
            style_element.setAttribute("ALIGN", "0")
            style_element.setAttribute("BASE", "0")
            style_element.setAttribute("DROP", "0")
            style_element.setAttribute("DROPLIN", "2")
            style_element.setAttribute("EFFECT", "0")
            style_element.setAttribute("FCOLOR", "Black")
            style_element.setAttribute("FIRST", "0")
            style_element.setAttribute("FONT", "Arial Bold")
            style_element.setAttribute("FONTSIZE", "12")
            style_element.setAttribute("FSHADE", "100")
            style_element.setAttribute("INDENT", "0")
            style_element.setAttribute("LINESP", "14.4")
            style_element.setAttribute("NACH", "0")
            style_element.setAttribute("NAME", "MyStyle")
            style_element.setAttribute("NUMTAB", "0")
            style_element.setAttribute("SCOLOR", "Black")
            style_element.setAttribute("SSHADE", "100")
            style_element.setAttribute("TABS", "")
            style_element.setAttribute("VOR", "0")
            document.appendChild(style_element)
    
    def insert_colours(self, document, first):
    
        for rgb, name in self.colours.items():
        
            if rgb not in self.standard_colours:
            
                color_element = self.scribus_document.createElement("COLOR")
                color_element.setAttribute("NAME", name)
                color_element.setAttribute("RGB", "#%02x%02x%02x" % rgb)
                color_element.setAttribute("CMYK",
                    "#%02x%02x%02x00" % (255 - rgb[0], 255 - rgb[1], 255 - rgb[2])
                    )
                first = document.insertBefore(color_element, first)
    
    def write_page(self, document, page, master = 0):

        # Find the page dimensions (not including the page bleed).
        self.page_width = page.margins.width() - (2 * page.bleed)
        self.page_height = page.margins.height() - (2 * page.bleed)

        page_element = self.scribus_document.createElement("PAGE")
        if master:
            page_element.setAttribute("MNAM", "")
            # Name/number of this master page:
            page_element.setAttribute("NAM", str(page.number))
            page_element.setAttribute("NUM", "0")
        else:
            # The name/number of the master page used by this page:
            page_element.setAttribute("MNAM", str(page.number))
            page_element.setAttribute("NAM", "")
            page_element.setAttribute("NUM", str(page.page - 1))
        
        page_element.setAttribute("HorizontalGuides", "")
        page_element.setAttribute("NumHGuides", "0")
        page_element.setAttribute("NumVGuides", "0")
        page_element.setAttribute("VerticalGuides", "")
        
        page_object_element = self.scribus_document.createElement("PAGEOBJECT")
        
        # Create the page attributes using the page dimensions (not
        # including the page bleed).
        self.write_frame_attributes(
            page_object_element, page, 0, 0, self.page_width, self.page_height
            )
        page_element.appendChild(page_object_element)
        
        # Examine each frame.
        for frame in page.frames:

            # Write frame information to the document.
            frame_element = self.scribus_document.createElement("PAGEOBJECT");

            # Determine the frame's bounding box relative to the page origin.
            frame_bbox = frame.inner_bbox.relative_to(page.origin)
            
            # 00 rr gg bb
            # Frame outline colour
            #if hasattr(frame, 'colour'):
            #
            #    if frame.colour.trans == 0:
            #    
            #        frame_element.setAttribute(
            #            "colour", "#%02x%02x%02x" % (
            #                frame.colour.red, frame.colour.green, frame.colour.blue
            #                )
            #            )

            # Border styles

            # Add the border objects to the frame.
            #for edge, number in frame.borders.items():
            #
            #    if number != 0xff:
            #    
            #        border = self.scribus_document.createElement("border")
            #        border.setAttribute("edge", str(edge))
            #        border.setAttribute("number", str(number))
            #        frame_element.appendChild(border)
            
            if hasattr(frame, 'content_ref'):

                # Process the contents of this frame.

                # Find content.
                content = self.document.content[ frame.content_ref ]
                
                if (frame.data_type & 0x07) == 3 and self.outline == 0 and \
                    content.content_type == 'Draw':

                    #self.write_frame_picture(
                    #    frame_element, content, page, frame
                    #    )
                    pass

                else:

                    self.write_frame_text(
                        frame_element, content, page, frame, frame_bbox
                        )

            self.write_frame_attributes(
                frame_element, frame,
                frame_bbox.xmin, -frame_bbox.ymax,
                frame_bbox.xmax - frame_bbox.xmin,
                frame_bbox.ymax - frame_bbox.ymin
                )
            
            # Add the frame to the page itself, not the object that represents
            # it.
            page_element.appendChild(frame_element)
        
        # Append the page to the document.
        document.appendChild(page_element)


    def write_frame_picture(self, frame_element, content, page,
                            frame):

        # Obtain information about the orientation of the picture within
        # the frame.

        #print 'Picture'
        #print
        #print 'x scale: %.3f%%' % xs
        #print 'y scale: %.3f%%' % ys
        #print 'x displacement: %.3f mm' % (dx * 25.4 / 72000.0)
        #print 'y displacement: %.3f mm' % (dy * 25.4 / 72000.0)
        #print 'angle: %.3f degrees' % angle
        #print
        #print 'Extracting content...',

        # Take the content and transform it.
        picture = self.scribus_document.createElement("picture")
        picture.setAttribute("type", "image/x-drawfile")
        picture.setAttribute("xscale", str(frame.x_scale))
        picture.setAttribute("yscale", str(frame.y_scale))
        picture.setAttribute("dx", str(frame.x_displacement/1000.0))
        picture.setAttribute("dy", str(frame.y_displacement/1000.0))
        picture.setAttribute("angle", str(frame.angle))
        content = self.scribus_document.createTextNode(content.data)
        picture.appendChild(content)
        
        frame_element.appendChild(picture)
    
    def copy_attribute(self, cl, attr, element, attribute):
    
        if hasattr(cl, attr):
        
            element.setAttribute(attribute, str(getattr(cl, attr)))
    
    def create_style_element(self, frame_element):
    
        style_element = self.scribus_document.createElement("style")
        self.copy_attribute(self.current_style, "name",
                           style_element, "name")
        self.copy_attribute(self.current_style, "font_name",
                            style_element, "font")
        self.copy_attribute(self.current_style, "bold_on",
                            style_element, "bold")
        self.copy_attribute(self.current_style, "italic_on",
                            style_element, "italic")
        self.copy_attribute(self.current_style, "font_size",
                            style_element, "size")
        self.copy_attribute(self.current_style, "aspect_ratio",
                            style_element, "aspect")
        self.copy_attribute(self.current_style, "superscript",
                            style_element, "superscript")
        self.copy_attribute(self.current_style, "subscript",
                            style_element, "subscript")
        self.copy_attribute(self.current_style, "script_aspect",
                            style_element, "scriptaspect")
        self.copy_attribute(self.current_style, "subscript_offset",
                            style_element, "subscriptoffset")
        self.copy_attribute(self.current_style, "superscript_offset",
                            style_element, "superscriptoffset")
        self.copy_attribute(self.current_style, "foreground_colour",
                            style_element, "foreground")
        self.copy_attribute(self.current_style, "background_colour",
                            style_element, "background")

        if hasattr(self.current_style, "underline"):

            underline = self.scribus_document.createElement("underline")
            self.copy_attribute(self.current_style, "underline_offset",
                                underline, "offset")
            self.copy_attribute(self.current_style, "underline_size",
                                underline, "size")
            style_element.appendChild(underline)

        if hasattr(self.current_style, "strikeout"):

            strikeout = self.scribus_document.createElement("strikeout")
            self.copy_attribute(self.current_style, "strikeout_colour",
                                strikeout, "colour")
            style_element.appendChild(strikeout)

        self.copy_attribute(self.current_style, "tracking",
                            style_element, "tracking")
        self.copy_attribute(self.current_style, "hyphenation",
                            style_element, "hyphenation")
        self.copy_attribute(self.current_style, "leadering",
                            style_element, "leadering")
        self.copy_attribute(self.current_style, "decimal_tab",
                            style_element, "decimal")

        self.copy_attribute(self.current_style, "above_para",
                            style_element, "abovepara")
        self.copy_attribute(self.current_style, "below_para",
                            style_element, "belowpara")
        self.copy_attribute(self.current_style, "justification",
                            style_element, "justification")
        self.copy_attribute(self.current_style, "line_spacing",
                            style_element, "linespacing")
        self.copy_attribute(self.current_style, "line_spacing_units",
                            style_element, "linespacingunits")
        self.copy_attribute(self.current_style, "page_grid_lock",
                            style_element, "pagegridlock")
        self.copy_attribute(self.current_style, "keep_together_length",
                            style_element, "keeptogetherlength")
        self.copy_attribute(self.current_style, "keep_together_single_para",
                            style_element, "keeptogethersinglepara")
        self.copy_attribute(self.current_style, "keep_together_multiple_para",
                            style_element, "keeptogethermultiplepara")
        self.copy_attribute(self.current_style, "keep_with_next_para",
                            style_element, "keepwithnextpara")
        self.copy_attribute(self.current_style, "auto_indent",
                            style_element, "autoindent")
        self.copy_attribute(self.current_style, "first_line_left_margin",
                            style_element, "firstlineleftmargin")
        self.copy_attribute(self.current_style, "left_margin",
                            style_element, "leftmargin")
        self.copy_attribute(self.current_style, "right_margin",
                            style_element, "rightmargin")
        self.copy_attribute(self.current_style, "left_rule_off_margin",
                            style_element, "leftruleoffmargin")
        self.copy_attribute(self.current_style, "right_rule_off_margin",
                            style_element, "rightruleoffmargin")

        if hasattr(self.current_style, "tab_defns"):

            tabs_element = self.scribus_document.createElement("tabs")
            for tab_position, tab_type in self.current_style.tab_defns:

                tab_element = self.scribus_document.createElement("tab")
                tab_element.setAttribute("type", str(tab_type))
                tab_element.setAttribute("position", str(tab_position/1000.0))
                tabs_element.appendChild(tab_element)

            style_element.appendChild(tabs_element)

        self.copy_attribute(self.current_style, "rule_off_thickness",
                            style_element, "ruleoffthickness")
        self.copy_attribute(self.current_style, "rule_off_above",
                            style_element, "ruleoffabove")
        self.copy_attribute(self.current_style, "rule_off_below",
                            style_element, "ruleoffbelow")
        self.copy_attribute(self.current_style, "rule_off_colour",
                            style_element, "ruleoffcolour")
        self.copy_attribute(self.current_style, "vertical_rule_off_width",
                            style_element, "verticalruleoffwidth")

        frame_element.appendChild(style_element)
    
    def write_frame_text(self, frame_element, content, page,
                         frame, frame_bbox):
    
        # Assume that the frame does not begin with a new paragraph.
        new_para = 0
        
        # Only the first piece uses the first line flag stored in the text.
        first_piece = 1
        
        for piece in content.pieces:
        
            if isinstance(piece, impression.TextSetup):
            
                self.reset_styles()
                
                #print piece, content.addr
                for i in piece.text:
                
                    # A sequence of words, beginning with a command
                    
                    if i[0] == 'apply style':
                    
                        # Apply each style.
                        
                        for counter, style in i[1:]:
                        
                            self.apply_style(counter, style)
            
            elif isinstance(piece, impression.Text):
            
                # Record whether this is the first line in a new paragraph.
                if first_piece == 1:
                
                    first_line = piece.first_line
                    first_piece = 0
                
                else:
                
                    first_line = new_para # or piece.first_line # self.new_para
                
                # Also note whether this is a trailing line in a paragraph.
                trailing = piece.trailing
                
                # If this is a trailing line then the next line will begin a new paragraph.
                new_para = trailing
                
                for i in piece.text:
                
                    if type(i) == types.StringType:
                    
                        # Split the text up if it contains spaces.
                        words = string.split(clean_string(i), ' ')
                        
                        for word in words[:-1]:
                        
                            if word != '':
                            
                                # Create a text element and add it to the line group.
                                Text(word, self.current_style).toxml(
                                    self.scribus_document, frame_element,
                                    self.colours
                                    )
                            
                            # Add a space between each word.
                            Space(self.current_style).toxml(
                                self.scribus_document, frame_element
                                )
                        
                        # The last word should be written without a following space.
                        if words[-1] != '':
                        
                            Text(words[-1], self.current_style).toxml(
                                self.scribus_document, frame_element,
                                self.colours
                                )
                        
                        if '\r' in i:
                        
                            # A newline was encountered. This shouldn't occur mid-line but
                            # a new line can be started.
                            #Text("\x05", self.current_style).toxml(
                            #    self.scribus_document, frame_element,
                            #    self.colours
                            #    )
                            
                            # Following text is part of a new paragraph.
                            first_line = 1
                            new_para = 1
                        
                        else:
                        
                            # Plain text
                            new_para = 0
                    
                    else:
                    
                        # A sequence of words, beginning with a command
                        if i[0] == 'apply style':
                        
                            self.reset_styles()
                            
                            # Apply each style listed only if its application number
                            # (counter) has not been recorded before.
                            
                            for counter, style in i[2:]:
                            
                                #if counter not in self.applied:
                                
                                self.apply_style(counter, style)
                        
                        elif i[0] == 'horizontal space':
                        
                            HSpace(i[1], self.current_style).toxml(
                                self.scribus_document, frame_element
                                )
                        
                        elif i[0] == 'kern':
                        
                            # Ignore the first two values and interpret the final two as displacements
                            # in millipoints.
                            h_kern = i[3]
                            v_kern = i[4]
                            
                            # Only vertical displacement is possible in text areas so ignore horizontal
                            # kerning.
                            Kern(h_kern, v_kern, self.current_style).toxml(
                                self.scribus_document, frame_element
                                )
                        
                        elif i[0] == 'page number':
                        
                            if hasattr(page, "page"):
                            
                                word = self.write_number(page.page, i[1])
                                
                                Text(word, self.current_style).toxml(
                                    self.scribus_document, frame_element,
                                    self.colours
                                    )
                        
                        elif i[0] == 'embedded frame':
                        
                            # Just add horizontal space.
                            HSpace(i[5], self.current_style).toxml(
                                self.scribus_document, frame_element
                                )
                        
                        elif i[0] == 'special character':
                        
                            # Add a special character to the text.
                            Text(i[1], self.current_style).toxml(
                                self.scribus_document, frame_element,
                                self.colours
                                )
                        
                        elif i[0] == 'line width':
                        
                            # Redefine the line margins based on this value.
                            left_margin = i[2]
                            right_margin = i[1]
                            
                            if not frame_element.hasAttribute("PWIDTH"):
                                frame_element.setAttribute("PWIDTH",
                                    right_margin - left_margin
                                    )
                    
                    # end of piece test
                
                # end of piece loop
            # end of piece test
        # end of pieces content.pieces loop
    
    def write_document_attributes(self, document):
    
        document.setAttribute("ABSTSPALTEN", "11")
        document.setAttribute("ANZPAGES", "1")
        document.setAttribute("AUTHOR", "")
        document.setAttribute("AUTOSPALTEN", "1")
        document.setAttribute("BORDERBOTTOM", "00")
        document.setAttribute("BORDERLEFT", "0")
        document.setAttribute("BORDERRIGHT", "0")
        document.setAttribute("BORDERTOP", "0")
        document.setAttribute("COMMENTS", "")
        document.setAttribute("DFONT", "Arial Bold")
        document.setAttribute("DSIZE", "12")
        document.setAttribute("KEYWORDS", "")
        document.setAttribute("PAGEHEIGHT", "842")
        document.setAttribute("PAGEWITH", "595")
        document.setAttribute("TITLE", "")
        document.setAttribute("UNITS", "0")
        document.setAttribute("VHOCH", "33")
        document.setAttribute("VHOCHSC", "100")
        document.setAttribute("VKAPIT", "75")
        document.setAttribute("VTIEF", "33")
        document.setAttribute("VTIEFSC", "100")
    
    def write_frame_attributes(self, element, obj, x, y, width, height):
    
        x = x / 1000.0
        y = y / 1000.0
        width = width/1000.0
        height = height/1000.0

        # Give the page a bounding box corresponding to the page dimensions.
        element.setAttribute("HEIGHT", str(height))
        element.setAttribute("WIDTH", str(width))
        element.setAttribute("XPOS", str(x))
        element.setAttribute("YPOS", str(y))
        
        element.setAttribute("ALIGN", "0")
        element.setAttribute("AUTOTEXT", "1")
        element.setAttribute("BACKITEM", "-1")
        element.setAttribute("BACKPAGE", "-1")
        element.setAttribute("BBOXH", "0")
        element.setAttribute("BBOXX", "0")
        element.setAttribute("BEXTRA", "0")
        element.setAttribute("BOOKMARK", "0")
        element.setAttribute("BottomLine", "0")
        element.setAttribute("BottomLINK", "0")
        element.setAttribute("CLIPEDIT", "0")
        element.setAttribute("COLGAP", "0")
        element.setAttribute("COLUMNS", "1")
        element.setAttribute("DASHS", "")
        element.setAttribute("EXTRA", "0")
        element.setAttribute("EXTRAV", "0")
        element.setAttribute("FLIPPEDH", "0")
        element.setAttribute("FLIPPEDV", "0")
        element.setAttribute("FRTYPE", "0")
        element.setAttribute("GROUPS", "")
        if self.fonts != []:
            element.setAttribute("IFONT", self.fonts[0])
        else:
            element.setAttribute("IFONT", "")
        
        if self.current_style.line_spacing_units == "%":
            line_spacing = (
                self.current_style.font_size * (
                    self.current_style.line_spacing / 100.0
                    )
                )/1000.0
        else:
            line_spacing = self.current_style.line_spacing / 1000.0
        element.setAttribute("LINESP", str(line_spacing))
        element.setAttribute("LOCALSCX", "1")
        element.setAttribute("LOCALSCY", "1")
        element.setAttribute("LOCALX", "0")
        element.setAttribute("LOCALY", "0")
        element.setAttribute("NEXTITEM", "-1")
        element.setAttribute("NEXTPAGE", "-1")
        element.setAttribute("NUMDASH", "0")
        element.setAttribute("NUMGROUP", "0")
        element.setAttribute("NUMPO", "16") # POCOOR
        element.setAttribute("NUMTAB", "0")
        element.setAttribute("PCOLOR", "None")
        element.setAttribute("PCOLOR2", "None")
        element.setAttribute("PFILE", "")
        element.setAttribute("PICART", "1")
        element.setAttribute("PILINEART", "1")
        
        p1 = "%i %i" % (0, 0)
        p2 = "%i %i" % (width, 0)
        p3 = "%i %i" % (width, height)
        p4 = "%i %i" % (0, height)
        
        element.setAttribute("POCOOR", (
            "%s %s " % (p1, p1) + \
            "%s %s %s %s " % (p2, p2, p2, p2) + \
            "%s %s %s %s " % (p3, p3, p3, p3) + \
            "%s %s %s %s " % (p4, p4, p4, p4) + \
            "%s %s" % (p1, p1)
            ))
        element.setAttribute("PRINTABLE", "1")
        element.setAttribute("PTYPE", "4") # text
        element.setAttribute("PWIDTH", "1")
        element.setAttribute("REXTRA", "0")
        element.setAttribute("REVERS", "0")
        element.setAttribute("ROT", "0")
        element.setAttribute("SHADE", "100")
        element.setAttribute("SHADE2", "100")
        element.setAttribute("TABS", "")
        element.setAttribute("TEXTRA", "0")
        if hasattr(obj, "repel_text") and obj.repel_text:
            element.setAttribute("TEXTFLOW", "1")
        else:
            element.setAttribute("TEXTFLOW", "0")
    
    def record_font_use(self):

        # Determine the font name and size of the text.
        #font_name = self.read_style_font()
        font_name = self.current_style.font_name
        font_size = self.current_style.font_size / 1000.0

        # Check whether this combination already exists.
        if font_name not in self.fonts:

            # Add the new font style to the font declarations.
            self.fonts.append(font_name)


    def write_number(self, number, style):

        if style == 'arabic':

            return '%i' % number

        else:

            # Roman numerals.
            s = ''

            # Find the number of thousands first.
            thousands = int(number / 1000)
            s = s + ('M' * thousands)

            number = number - (thousands * 1000)

            # If the result is greater than five hundred then add the relevant symbols.
            if number > 500:

                if number < 800:

                    s = s + 'D'
                    number = number - 500

                # Determine how many hundreds can be taken away from a thousand to
                # produce a number smaller than the one we have.
                elif number < 900:

                    s = s + 'CCM'
                    number = number - 800

                else:

                    s = s + 'CM'
                    number = number - 900

            # Find the number of hundreds remaining.
            hundreds = int(number / 100)
            s = s + ('C' * hundreds)

            number = number - (hundreds * 100)

            # If the result is greater than fifty then add the relevant symbol.
            if number > 50:

                if number < 80:

                    s = s + 'L'
                    number = number - 50

                # Determine how many tens can be taken away from a hundred to
                # produce a number smaller than the one we have.
                elif number < 90:

                    s = s + 'XXC'
                    number = number - 80

                else:

                    s = s + 'XC'
                    number = number - 90

            # Find the number of tens remaining.
            tens = int(number / 10)
            s = s + ('M' * tens)

            # If the result is greater than five then add the relevant symbols.
            if number > 5:

                if number < 8:

                    s = s + 'V'
                    number = number - 5

                # Determine how many ones can be taken away from a ten to
                # produce a number smaller than the one we have.
                elif number < 9:

                    s = s + 'IIX'
                    number = number - 8

                else:

                    s = s + 'IX'
                    number = number - 9

            # Find the number of ones remaining.
            s = s + ('I' * number)

        if style == 'roman':

            return string.lower(s)

        else:

            return s


if __name__ == '__main__':

    syntax = (
        "[-b <begin at page>] [-e <end at page>] [--outline] "
        "[--lenient] [--show-all-pictures] <Impression file> "
        "<Scribus file>"
        )
    
    syntax_obj = Syntax(syntax)
    
    # Check the arguments.
    matches = syntax_obj.get_args(sys.argv[1:])
    
    if len(matches) != 1:

        sys.stderr.write('Usage: impscribus.py %s\n' % syntax)
        sys.exit(1)
    
    match = matches[0]
    
    # Look for beginning and ending pages.
    if match.has_key("begin at page"):

        try:
            begin_page = int(match["begin at page"])
        except ValueError:
            print '%s is not a valid beginning page number.' % args
            sys.exit()

    else:

        begin_page = 0

    if match.has_key("end at page"):

        try:
            end_page = int(match["end at page"])
        except ValueError:
            print '%s is not a valid ending page number.' % args
            sys.exit()

    else:

        end_page = -1

    # Read the input file and output directory details.
    infile = match["Impression file"]
    outfile = match["Scribus file"]

    # Check that the output directory exists.

    # Create the output directory, if necessary.
    if os.path.exists(outfile):

        print '%s already exists.' % outfile
        sys.exit(1)

    # Instantiate a writer object.
    try:

        writer = ScribusWriter(infile, not match.has_key("lenient"))
        writer.write(outfile, begin_page, end_page, match.has_key("outline"),
            match.has_key("show-all-pictures"))

    except WriterError:

        e_type, e_value, e_traceback = sys.exc_info()
        output = filter(lambda x: x != None, e_value.args)
        print string.join(output, '\n')
        sys.exit(1)

    # Reopen the output file and remove the first line to avoid problems with
    # Scribus.
    lines = open(outfile).readlines()
    open(outfile, "w").writelines(lines[1:])

    # Exit
    sys.exit()
